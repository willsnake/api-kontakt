<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>404 Page Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href="https://www.apikontakt.com">Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a href="https://www.apikontakt.com/main/login">Login</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href="https://www.apikontakt.com"><img src="img/Apikontakt_iza.png" /></a>
    </div>

    <div class="row" style="text-align:center;">
        <div class="span12">
            <h1><?php echo $heading; ?></h1>
            <?php echo $message; ?>
        </div>
    </div>
</div>

<br />
<br />
<br />

<hr>

<footer style="text-align:center;">
    <p><a class="link-footer" href="https://www.apikontakt.com/about">About us </a><a class="link-footer" href="https://www.apikontakt.com/kontakt"> Kontakt us</a> <a class="link-footer" href="https://www.apikontakt.com/browser">Browser Requirements</a> <a class="link-footer" href="https://www.apikontakt.com/terms">Terms and Conditions</a> <a class="link-footer" href="https://www.apikontakt.com/privacy_policy">Privacy Policy</a> <a class="link-footer" href="https://www.apikontakt.com/cookie_policy">Cookie Policy</a> apikontakt &copy; 2013</p>
</footer>

<script src="js/jquery.min.js"></script>

</body>
</html>