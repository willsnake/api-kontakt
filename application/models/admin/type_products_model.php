<?php
class Type_products_model extends grocery_CRUD_Model  {

    function get_list() {
        if($this->table_name === null) {
            return false;
        }

        $select = "COUNT(productos.product_name) AS conteo, productos.type";

        /* if(!empty($this->relation)) {
            foreach($this->relation as $relation) {
                list($field_name , $related_table , $related_field_title) = $relation;
                $unique_join_name = $this->_unique_join_name($field_name);
                $unique_field_name = $this->_unique_field_name($field_name);

                if(strstr($related_field_title,'{')) {
                    $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
                }
                else {
                    $select .= ", $unique_join_name.$related_field_title as $unique_field_name";
                }

                if($this->field_exists($related_field_title)) {
                    $select .= ", {$this->table_name}.$related_field_title as '{$this->table_name}.$related_field_title'";
                }
            }
        }*/

        $this->db->select($select, false);
        $this->db->group_by("productos.type");

        /*$this->db->join('datosusuario','datosusuario.id_datos_usuario = '. $this->table_name . '.id_supplier', 'right');
        $this->db->join('usuario','usuario.id_datos_usuario = datosusuario.id_datos_usuario WHERE productos.id_supplier IS NULL AND usuario.id_tipo_usuario = 2');*/

        $results = $this->db->get($this->table_name)->result();

        return $results;
    }

}