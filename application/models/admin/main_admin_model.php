<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_admin_model extends CI_Model {

    public function loguear() {
        $user = trim($this->input->post('user_txt'));

        $sql = "SELECT user_name FROM Usuario WHERE user_name = ?";

        $result = $this->db->query($sql, array($user));
        $row = $result->row();

        if($row != null && $row->user_name != null) {
            $password = md5($this->input->post('pass_txt'));

            $sql = "SELECT password FROM Usuario WHERE password = ? AND user_name = ?";
            $result = $this->db->query($sql, array($password, $user));
            $row = $result->row();

            if($row != null && $row->password != null) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return FALSE;
        }

    }

    public function getUsuario() {
        $usuario = $this->input->post('user_txt');
        $sql = "SELECT id_tipo_usuario as tipo, id_datos_usuario as usuario FROM Usuario WHERE user_name = ?";
        $result = $this->db->query($sql, array($usuario, $usuario));
        $row = $result->row();
        return $row;
    }

    public function admin_add_temp_buyer($key) {
        $data = array(
            'prefix' => "Mr.",
            'first_name' => "Daniel",
            'last_name' => "Rodriguez",
            'country' => "Mexico",
            'city' => "Mexico",
            'street_address' => "Emilio Carranza 162",
            'zip_postal' => "09440",
            'company_name' => "Bhacking",
            'company_type' => "",
            'country_code' => "52",
            'area_code' => "55",
            'phone' => "55326908",
            'web_page' => "www.bhacking.com",
            'job_position' => "Developer",
            'email' => "willsnake87@gmail.com",
            'user_name' => "willsnake",
            'password' => md5("prueba"),
            'id_tipo_usuario' => 3,
            'reference_code' => "APIK001",
            'key' => $key
        );

        $query = $this->db->insert('TempDatosUsuario',$data);

        if($query) {
            return true;
        }
        else {
            return false;
        }

    }

    public function bulkEmail($id) {
        $sql = "SELECT * FROM TempDatosUsuario WHERE id_temp_datos_usuario = ?";
        $result = $this->db->query($sql, array($id));
        $row = $result->row();
        return $row;
    }

}