<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

	public function loguear() {
		$user = trim($this->input->post('user'));
		
		if (!strpos($user,'@') == true) {
			$sql = "SELECT user_name FROM Usuario WHERE user_name = ?";

			$result = $this->db->query($sql, array($user));
			$row = $result->row();
			
			if($row != null && $row->user_name != null) {
				$password = md5($this->input->post('password'));
		
				$sql = "SELECT password FROM Usuario WHERE password = ? AND user_name = ?";
				$result = $this->db->query($sql, array($password, $user));
				$row = $result->row();
				
				if($row != null && $row->password != null) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return FALSE;
			}
		}
		else {
			$sql = "SELECT id_datos_usuario FROM DatosUsuario WHERE email = ?";
			$result = $this->db->query($sql, array($user));
			$row = $result->row();

			if($row != null && $row->id_datos_usuario != null) {
				$password = md5($this->input->post('password'));
		
				$sql = "SELECT password FROM Usuario WHERE password = ? AND id_datos_usuario = ?";
				$result = $this->db->query($sql, array($password, $row->id_datos_usuario));
				$row = $result->row();
				
				if($row != null && $row->password != null) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return FALSE;
			}
		}

	}

	public function tipoUsuario() {		
		$usuario = $this->input->post('user');
		$sql = "SELECT u.id_tipo_usuario as tipo FROM Usuario u JOIN DatosUsuario du ON u.id_datos_usuario = du.id_datos_usuario WHERE u.user_name = ? OR du.email = ?";
		$result = $this->db->query($sql, array($usuario, $usuario));
		$row = $result->row();
		return $row->tipo;
	}
	
	public function getUsuario() {	
		$usuario = $this->input->post('user');
		$sql = "SELECT u.id_tipo_usuario as tipo, u.id_datos_usuario as usuario FROM Usuario u JOIN DatosUsuario du ON u.id_datos_usuario = du.id_datos_usuario WHERE u.user_name = ? OR du.email = ?";
		$result = $this->db->query($sql, array($usuario, $usuario));
		$row = $result->row();
		return $row;
	}

}

?>