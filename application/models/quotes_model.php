<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quotes_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }
	
	public function add_supplier_quote() {		
		$data = array(
					'product_name' => $this->input->post('name'),
					'pharmacopeia' => $this->input->post('pharmacopeia'),
					'quantity' => $this->input->post('quantity'),
					'unit' => $this->input->post('unit'),
					'incoterm' => $this->input->post('incoterm'),
					'port' => $this->input->post('port'),
					'due_date' => $this->input->post('limit'),
                    'notes_buyer' => $this->input->post('notes'),
					'status' => 0,
					'id_buyer' => $this->session->userdata('usuario'),
					'id_supplier' => $this->input->post('usuario'),
				);
		
		$query = $this->db->insert('QuotesBuyer',$data);

        $insert_id = $this->db->insert_id();
        $newdata = array(
            'id_quotes_supplier' => $insert_id,
            'product_name' => $this->input->post('name'),
            'pharmacopeia' => $this->input->post('pharmacopeia'),
            'quantity' => $this->input->post('quantity'),
            'unit' => $this->input->post('unit'),
            'incoterm' => $this->input->post('incoterm'),
            'port' => $this->input->post('port'),
            'due_date' => $this->input->post('limit'),
            'notes_buyer' => $this->input->post('notes'),
            'status' => 0,
            'id_buyer' => $this->session->userdata('usuario'),
            'id_supplier' => $this->input->post('usuario'),
        );
        $query = $this->db->insert('QuotesSupplier',$newdata);
		
		if($query) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public function add_quote_buyer($usuario) {
		$data = array(
					'product_name' => $this->input->post('name'),
					'pharmacopeia' => $this->input->post('pharmacopeia'),
					'quantity' => $this->input->post('quantity'),
					'unit' => $this->input->post('unit'),
					'incoterm' => $this->input->post('incoterm'),
					'port' => $this->input->post('port'),
					'due_date' => $this->input->post('limit'),
                    'notes_buyer' => $this->input->post('notes'),
					'status' => 0,
					'id_buyer' => $this->session->userdata('usuario'),
					'id_supplier' => $usuario,
				);

        $query = $this->db->insert('QuotesBuyer',$data);
        $insert_id = $this->db->insert_id();
        $newdata = array(
            'id_quotes_supplier' => $insert_id,
            'product_name' => $this->input->post('name'),
            'pharmacopeia' => $this->input->post('pharmacopeia'),
            'quantity' => $this->input->post('quantity'),
            'unit' => $this->input->post('unit'),
            'incoterm' => $this->input->post('incoterm'),
            'port' => $this->input->post('port'),
            'due_date' => $this->input->post('limit'),
            'notes_buyer' => $this->input->post('notes'),
            'status' => 0,
            'id_buyer' => $this->session->userdata('usuario'),
            'id_supplier' => $usuario,
        );
        $query = $this->db->insert('QuotesSupplier',$newdata);
		
		if($query) {
			return true;
		}
		else {
			return false;
		}		
	}

	public function loguear() {
		$this->db->where('user',$this->input->post('user'));
		$this->db->where('password',md5($this->input->post('password')));
		$query = $this->db->get('Usuarios');
		if ($query->num_rows() == 1) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

	public function tipoUsuario() {
		$usuario = trim($this->input->post('user'));
		$sql = "SELECT tipo_usuario as tipo FROM Usuarios WHERE user = {$usuario}";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	public function sendQuote($id) {
		$sql = "SELECT * FROM QuotesBuyer WHERE id_quotes_buyer = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
		return $row;
	}
	
	public function sendQuote_Form() {
		$cookies = $this->session->all_userdata();
	
		$id = $this->input->post('id_quote');
		$price = $this->input->post('price');
		$currency = $this->input->post('currency');
		$payment = $this->input->post('payment');
		$availability = $this->input->post('availability');
		$notes = $this->input->post('notes');
		
		$data = array(
				'status' => 1,
				'price' => $price,
				'currency' => $currency,
				'payment' => $payment,
				'availability' => $availability,
				'notes_supplier' => $notes,
            );

        $this->db->where('id_quotes_supplier', $id);
        $query1 = $this->db->update('QuotesSupplier', $data);

        $this->db->where('id_quotes_buyer', $id);
        $query2 = $this->db->update('QuotesBuyer', $data);

		
		if($query1 && $query2) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function infoQuote($id) {
		$sql = "SELECT * FROM QuotesBuyer WHERE id_quotes_buyer = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
		return $row;
	}
	
	public function selectSuppliers() {
		$name = $this->input->post('name');
		$this->db->select('usuario');
		$this->db->where('name', $name); 
        $query = $this->db->get('Products');
		print_r($query->result());
        return $query->result();
	}

	public function number_of_rows() {
		$name = "%".trim($this->input->post('product_name_txt'))."%";
		$cas1 = "%".trim($this->input->post('cas1'))."%";
		$cas2 = "%".trim($this->input->post('cas2'))."%";
		$cas3 = "%".trim($this->input->post('cas3'))."%";
		
		$sql = "SELECT * FROM Productos WHERE product_name LIKE ? AND cas1 LIKE ? AND cas2 LIKE ? AND cas3 LIKE ?";
		$result = $this->db->query($sql, array($name, $cas1, $cas2, $cas3));
		$row = $result->num_rows();
		return $row;
	}
	
	public function get_Supplier($user) {
		$sql = "SELECT p.id_supplier, p.product_name, du.company_name FROM Productos p JOIN DatosUsuario du ON p.id_supplier = du.id_datos_usuario WHERE p.id_productos = ? AND p.id_supplier = du.id_datos_usuario";
		$result = $this->db->query($sql, array($user));
		$row = $result->row();
		return $row;
	}
	
	public function get_Email($user) {
		$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
		$result = $this->db->query($sql, array($user));
		$row = $result->row();

        $resultado = array(
            'email' => $row->email,
            'company_name' => $row->company_name,
            'first_name' => $row->first_name,
            'last_name' => $row->last_name
        );
		return $resultado;
	}
	
	public function get_Email_Supplier($id) {
        $sql = "SELECT id_buyer FROM QuotesBuyer WHERE id_quotes_buyer = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
        $buyer = $row->id_buyer;

		$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
		$result = $this->db->query($sql, array($buyer));
		$row = $result->row();

        $resultado = array(
            'email' => $row->email,
            'company_name' => $row->company_name,
            'first_name' => $row->first_name,
            'last_name' => $row->last_name
        );

        return $resultado;
	}
	
	public function supplierName() {
		$name = trim($this->input->post('supplier_name'));
		
		$sql = "SELECT companyname FROM Usuarios WHERE companyname LIKE '%?%'";
		$result = $this->db->query($sql, array($name));
		$row = $result->row();
		return $row->companyname;
	}
	
	public function getProductName($id) {
		$sql = "SELECT product_name FROM Productos WHERE id_productos = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
		return $row;
	}
	
	public function getSupplierName($id) {
		$sql = "SELECT usuario FROM Products WHERE id_products = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
		return $row;
	}

	public function get_email_suppliers_quote($id) {
		$sql = "SELECT id_supplier FROM Productos WHERE id_productos = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
        $usuario = $row->id_supplier;

        $sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
		$result = $this->db->query($sql, array($usuario));
		$row = $result->row();

        $resultado = array(
            'id_datos_usuario' => $usuario,
            'email' => $row->email,
            'first_name' => $row->first_name,
            'last_name' => $row->last_name
        );
        return $resultado;

	}
	
	public function selectProducts($name){
        $name = "%".$name."%";
        $sql = "SELECT du.id_datos_usuario as id, du.company_name FROM DatosUsuario du JOIN Usuario u ON du.id_datos_usuario = u.id_datos_usuario WHERE du.company_name LIKE ? AND u.id_tipo_usuario = 2 GROUP BY du.company_name";
        $result = $this->db->query($sql, array($name));
        $row = $result->result();
        return $row;
	}

}

?>