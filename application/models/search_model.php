<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model {

	public function GetAutocomplete($options = array()) {
        $options['keyword'] = "%".$options['keyword']."%";
        $sql = "SELECT p.product_name FROM Productos p JOIN Usuario u ON p.id_supplier = u.id_datos_usuario WHERE p.product_name LIKE ? AND u.id_tipo_usuario = 2 GROUP BY p.product_name";
        $result = $this->db->query($sql, array($options['keyword']));
        return $result->result();
    }
	
	public function GetAutocompleteSupplier($options = array()) {
        $options['keyword'] = "%".$options['keyword']."%";
        $sql = "SELECT du.company_name FROM DatosUsuario du JOIN Usuario u ON du.id_datos_usuario = u.id_datos_usuario WHERE du.company_name LIKE ? AND u.id_tipo_usuario = 2 GROUP BY du.company_name";
        $result = $this->db->query($sql, array($options['keyword']));
        return $result->result();
	}
}

?>