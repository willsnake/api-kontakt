<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelousuarios extends CI_Model {

	public function __construct() {
        parent::__construct();
    }
	
	public function infoUsuario($id) {
		$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
        $result = $this->db->query($sql, array($id));
		$row = $result->row();
		return $row;
	}

	public function add_temp_users($key) {
		$data = array(
                    'prefix' => $this->input->post('prefix_txt'),
                    'first_name' => $this->input->post('fname_txt'),
                    'last_name' => $this->input->post('lname_txt'),
                    'country' => $this->input->post('country_txt'),
                    'city' => $this->input->post('city_txt'),
                    'street_address' => $this->input->post('street_txt'),
                    'zip_postal' => $this->input->post('zip_txt'),
					'company_name' => $this->input->post('companyname_txt'),
					'company_type' => $this->input->post('companytype_txt'),
					'country_code' => $this->input->post('countrycode_txt'),
					'area_code' => $this->input->post('areacode_txt'),
					'phone' => $this->input->post('phone_txt'),
					'web_page' => $this->input->post('page_txt'),
					'job_position' => $this->input->post('position_txt'),
					'email' => $this->input->post('email_txt'),
					'reference_code' => $this->input->post('code_txt'),
					'user_name' => $this->input->post('user_txt'),
					'password' => md5($this->input->post('password_txt')),
					'id_tipo_usuario' => 2,
					'key' => $key
				);

        $query = $this->db->insert('TempDatosUsuario',$data);
		
		if($query) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public function add_temp_buyer($key) {
        $data = array(
            'prefix' => $this->input->post('prefix_txt'),
            'first_name' => $this->input->post('fname_txt'),
            'last_name' => $this->input->post('lname_txt'),
            'country' => $this->input->post('country_txt'),
            'city' => $this->input->post('city_txt'),
            'street_address' => $this->input->post('street_txt'),
            'zip_postal' => $this->input->post('zip_txt'),
            'company_name' => $this->input->post('companyname_txt'),
            'company_type' => $this->input->post('companytype_txt'),
            'country_code' => $this->input->post('countrycode_txt'),
            'area_code' => $this->input->post('areacode_txt'),
            'phone' => $this->input->post('phone_txt'),
            'web_page' => $this->input->post('page_txt'),
            'job_position' => $this->input->post('position_txt'),
            'email' => $this->input->post('email_txt'),
            'reference_code' => $this->input->post('code_txt'),
            'user_name' => $this->input->post('user_txt'),
            'password' => md5($this->input->post('password_txt')),
            'id_tipo_usuario' => 3,
            'reference_code' => $this->input->post('code_txt'),
            'key' => $key
        );

        $query = $this->db->insert('TempDatosUsuario',$data);
		
		if($query) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public function is_key_valid($key) {
		$this->db->where('key',$key);
		$query = $this->db->get('TempDatosUsuario');
		
		if($query->num_rows() == 1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function add_user($key) {
		$this->db->where('key',$key);
		$temp_user = $this->db->get('TempDatosUsuario');
		
		if($temp_user) {
			$row = $temp_user->row();

			$data = array(
                'prefix' => $row->prefix,
                'first_name' => $row->first_name,
                'last_name' => $row->last_name,
                'country' => $row->country,
                'city' => $row->city,
                'street_address' => $row->street_address,
                'zip_postal' => $row->zip_postal,
                'company_name' => $row->company_name,
                'company_type' => $row->company_type,
                'country_code' => $row->country_code,
                'area_code' => $row->area_code,
                'phone' => $row->phone,
                'web_page' => $row->web_page,
                'job_position' => $row->job_position,
                'email' => $row->email,
                'reference_code' => $row->reference_code,
                'date_subscripton' => date('Y-m-d'),
            );

            $did_add_data = $this->db->insert('DatosUsuario',$data);
            $insert_id = $this->db->insert_id();
            $usuario = array(
                'user_name' => $row->user_name,
                'password' => $row->password,
                'id_datos_usuario' => $insert_id,
                'id_tipo_usuario' => $row->id_tipo_usuario
            );
            $did_add_user = $this->db->insert('Usuario',$usuario);
            $id_usuario = $this->db->insert_id();
		}
		
		if($did_add_user && $did_add_data) {
			$this->db->where('key',$key);
			$this->db->delete('TempDatosUsuario');
            $ingreso = array(
                'id_usuario' => $id_usuario,
                'id_tipo_usuario' => $usuario['id_tipo_usuario'],
            );

            if($usuario['id_tipo_usuario'] == 3) {
                $usuario = array(
                    'id_datos_usuario' => $insert_id,
                );
                $this->db->where('key',$key);
                $did_add_user = $this->db->update('BillingInfo',$usuario);


                $fecha = date("Y-m-d");

                $datosPagoUsuario = array(
                    'fecha_pago' => $fecha,
                    'pagado' => 1,
                    'renovar_pago' => date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day")),
                    'id_datos_usuario' => $insert_id
                );

                $this->db->insert('pago',$datosPagoUsuario);
            }

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'support@apikontakt.com',
                'smtp_pass' => 'ApiKtkt8',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $this->email->from('support@apikontakt.com','API Kontakt');
            $this->email->to($row->email);
            $this->email->subject('Welcome to API Kontakt');

            $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
            $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
            $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';
            $message .= '<p>Dear <strong>'.$row->first_name.' '.$row->last_name.'</strong>,';
            $message .= '</p><p>Congratulations! Your subscription has been confirmed.</p><p>Thank you for signing up!</p><p>You are now part of the World’s Largest Pharma Network.</p><p>Login to your account and start enjoying all the <a href="'.base_url().'">API Kontakt</a> benefits.</p><p>Best regards</p><br /><p>API Kontakt Team</p></div></div></body></html>';

            $this->email->message($message);
            $this->email->send();
			return $ingreso;
		}
		else {
			return false;
		}
	}
	
	public function email_exists($email) {
        $email = trim($email);
        $sql = "SELECT email FROM DatosUsuario WHERE email = ?";
        $result = $this->db->query($sql, array($email));
        $row = $result->row();
        return $row;
	}
	
	public function add_key_reset_password($email,$key) {
        $sql = "SELECT id_datos_usuario FROM DatosUsuario WHERE email = ?";
        $result = $this->db->query($sql, array($email));
        $row = $result->row();

        $data = array(
            'key_password' => $key,
        );

        $this->db->insert('Key',$data);
        $insert_id = $this->db->insert_id();
        $sql = "UPDATE Usuario SET id_key = ? WHERE id_datos_usuario = ?";
        $result = $this->db->query($sql, array($insert_id, $row->id_datos_usuario));

		if($result) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function verify_reset_password($email, $key) {
        $email = trim($email);

        $sql = "SELECT id_datos_usuario FROM DatosUsuario WHERE email = ?";
        $result = $this->db->query($sql, array($email));
        $row = $result->row();

		$sql = "SELECT u.id_key  FROM Usuario u JOIN `key` k ON u.id_key = k.id_key WHERE id_datos_usuario = ? AND k.key_password = ?";
        $result = $this->db->query($sql, array($row->id_datos_usuario, $key));
		$row = $result->row();

		if($result->num_rows() === 1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function updatePassword() {
		$email = trim($this->input->post('email'));
		$password = md5($this->input->post('password'));

        $sql = "SELECT id_datos_usuario FROM DatosUsuario WHERE email = ?";
        $result = $this->db->query($sql, array($email));
        $row = $result->row();

        $sql2 = "SELECT id_key FROM Usuario WHERE id_datos_usuario = ?";
        $result2 = $this->db->query($sql2, array($row->id_datos_usuario));
        $row2 = $result2->row();

        $sql = "UPDATE Usuario SET Usuario.`password` = ? WHERE id_datos_usuario = ?";
        $change_password = $this->db->query($sql, array($password, $row->id_datos_usuario));
        $sql = "UPDATE Usuario SET id_key = 0 WHERE id_datos_usuario = ?";
        $delete_key_usuario = $this->db->query($sql, array($row->id_datos_usuario));
        $sql = "DELETE FROM `Key` WHERE id_key = ?";
        $delete_key_key = $this->db->query($sql, array($row2->id_key));
		
		if($change_password && $delete_key_usuario && $delete_key_key) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function updateSupplier() {
		$cookies = $this->session->all_userdata();
        $user = $cookies['usuario'];
        $data = array(
            'prefix' => $this->input->post('prefix_txt'),
            'first_name' => $this->input->post('fname_txt'),
            'last_name' => $this->input->post('lname_txt'),
            'country' => $this->input->post('country_txt'),
            'city' => $this->input->post('city_txt'),
            'street_address' => $this->input->post('street_txt'),
            'zip_postal' => $this->input->post('zip_txt'),
            'company_name' => $this->input->post('companyname_txt'),
            'company_type' => $this->input->post('companytype_txt'),
            'country_code' => $this->input->post('countrycode_txt'),
            'area_code' => $this->input->post('areacode_txt'),
            'phone' => $this->input->post('phone_txt'),
            'web_page' => $this->input->post('page_txt'),
            'job_position' => $this->input->post('position_txt'),
            'email' => $this->input->post('email_txt'),
        );

        $sql = "UPDATE datosusuario SET datosusuario.prefix = ?, datosusuario.first_name = ?, datosusuario.last_name = ?, datosusuario.country = ?, datosusuario.city = ?, datosusuario.street_address = ?, datosusuario.zip_postal = ?, datosusuario.company_name = ?, datosusuario.company_type = ?, datosusuario.country_code = ?, datosusuario.area_code = ?, datosusuario.phone = ?, datosusuario.web_page = ?, datosusuario.job_position = ?, datosusuario.email = ? WHERE datosusuario.id_datos_usuario = ?";
        $result = $this->db->query($sql, array($this->input->post('prefix_txt'), $this->input->post('fname_txt'), $this->input->post('lname_txt'), $this->input->post('country_txt'), $this->input->post('city_txt'), $this->input->post('street_txt'), $this->input->post('zip_txt'), $this->input->post('companyname_txt'), $this->input->post('companytype_txt'), $this->input->post('countrycode_txt'), $this->input->post('areacode_txt'), $this->input->post('phone_txt'), $this->input->post('page_txt'), $this->input->post('position_txt'), $this->input->post('email_txt'), $user));

        if($result == 1) {
            $sql = "UPDATE usuario SET usuario.user_name = ?, usuario.`password` = ? WHERE usuario.id_datos_usuario = ?";
            $result = $this->db->query($sql, array($this->input->post('user_txt'), md5($this->input->post('password_txt')), $user));
            return true;
        }
        else {
            return false;
        }
	}
	
	public function updateBuyer() {
        $cookies = $this->session->all_userdata();
        $user = $cookies['usuario'];
        $data = array(
            'prefix' => $this->input->post('prefix_txt'),
            'first_name' => $this->input->post('fname_txt'),
            'last_name' => $this->input->post('lname_txt'),
            'country' => $this->input->post('country_txt'),
            'city' => $this->input->post('city_txt'),
            'street_address' => $this->input->post('street_txt'),
            'zip_postal' => $this->input->post('zip_txt'),
            'company_name' => $this->input->post('companyname_txt'),
            'country_code' => $this->input->post('countrycode_txt'),
            'area_code' => $this->input->post('areacode_txt'),
            'phone' => $this->input->post('phone_txt'),
            'web_page' => $this->input->post('page_txt'),
            'job_position' => $this->input->post('position_txt'),
            'email' => $this->input->post('email_txt'),
        );

        $sql = "UPDATE datosusuario SET datosusuario.prefix = ?, datosusuario.first_name = ?, datosusuario.last_name = ?, datosusuario.country = ?, datosusuario.city = ?, datosusuario.street_address = ?, datosusuario.zip_postal = ?, datosusuario.company_name = ?, datosusuario.country_code = ?, datosusuario.area_code = ?, datosusuario.phone = ?, datosusuario.web_page = ?, datosusuario.job_position = ?, datosusuario.email = ? WHERE datosusuario.id_datos_usuario = ?";
        $result = $this->db->query($sql, array($this->input->post('prefix_txt'), $this->input->post('fname_txt'), $this->input->post('lname_txt'), $this->input->post('country_txt'), $this->input->post('city_txt'), $this->input->post('street_txt'), $this->input->post('zip_txt'), $this->input->post('companyname_txt'), $this->input->post('countrycode_txt'), $this->input->post('areacode_txt'), $this->input->post('phone_txt'), $this->input->post('page_txt'), $this->input->post('position_txt'), $this->input->post('email_txt'), $user));

        if($result == 1) {
            $sql = "UPDATE usuario SET usuario.user_name = ?, usuario.`password` = ? WHERE usuario.id_datos_usuario = ?";
            $result = $this->db->query($sql, array($this->input->post('user_txt'), md5($this->input->post('password_txt')), $user));
            return true;
        }
        else {
            return false;
        }
	}
	
	public function get_CAS($id) {
		$sql = "SELECT cas1, cas2, cas3 FROM Productos WHERE id_productos = ?";
		$result = $this->db->query($sql, array($id));
		$row = $result->row();
		return $row;
	}

    public function delete_user() {
        $cookies = $this->session->all_userdata();
        $user = $cookies['usuario'];
        if ($this->session->userdata('tipo_usuario') == 2) {
            $this->db->delete('productos', array('id_supplier' => $user));
            $this->db->delete('quotesbuyer', array('id_supplier' => $user));
            $this->db->delete('quotessupplier', array('id_supplier' => $user));
        }
        else {
            $this->db->delete('quotesbuyer', array('id_buyer' => $user));
            $this->db->delete('quotessupplier', array('id_buyer' => $user));
        }
        $this->db->delete('datosusuario', array('id_datos_usuario' => $user));
        $this->db->delete('usuario', array('id_datos_usuario' => $user));
    }

    public function add_billing_info($key) {


        $this->form_validation->set_rules('address_txt','ZIP/Postal Code','trim|required');
        $this->form_validation->set_rules('country_txt','Country Code','trim|required');
        $this->form_validation->set_rules('city_txt','Area Code','trim|required');
        $this->form_validation->set_rules('state_txt','Phone Number','trim|required');
        $this->form_validation->set_rules('zip_txt','Web Page','trim|required');
        $this->form_validation->set_rules('telephone_txt','Title','trim|required');
        $this->form_validation->set_rules('tax_txt','First Name','trim|required');

        $data = array(
            'first_name' => $this->input->post('fname_txt'),
            'last_name' => $this->input->post('lname_txt'),
            'company_name' => $this->input->post('company_txt'),
            'email' => $this->input->post('email_txt'),
            'address' => $this->input->post('address_txt'),
            'country' => $this->input->post('country_txt'),
            'city' => $this->input->post('city_txt'),
            'state' => $this->input->post('state_txt'),
            'zip' => $this->input->post('zip_txt'),
            'telephone' => $this->input->post('telephone_txt'),
            'tax' => $this->input->post('tax_txt'),
            'key' => $key
        );

        $query = $this->db->insert('BillingInfo',$data);

        if($query) {
            return true;
        }
        else {
            return false;
        }
    }

    public function send_mail_confirmation($key) {
        $sql = "SELECT * FROM tempdatosusuario WHERE tempdatosusuario.`key` = ?";
        $result = $this->db->query($sql, array($key));
        $row = $result->row();
        return $row;
    }

    public function setPayment($key) {
        $sql = "SELECT * FROM tempdatosusuario WHERE tempdatosusuario.`key` = ?";
        $result = $this->db->query($sql, array($key));
        $row = $result->row();
        return $row;
    }

    public function deleteTempUser($key) {
        $this->db->where('key', $key);
        $this->db->delete('tempdatosusuario');
    }
}
?>