<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buyer extends CI_Controller {

	public function __construct() {
		parent::__construct();
        if (!$this->session->userdata('logueado')) {
            redirect('main/restringido');
        }
        elseif($this->session->userdata('tipo_usuario') == 2 && $this->session->userdata('logueado') == 1){
            redirect('supplier');
        }
	}

	public function index() {
        $cookies = $this->session->all_userdata();
        if ($cookies['logueado'] != 1 && $cookies['tipo_usuario'] != 3) {
            redirect('main/restringido');
        }
        else {
            $this->datos();
        }
	}
	
	public function output($output = null) {
		$this->load->view('find_supplier_view.php',$output);
	}
	
	public function outputProduct($output = null) {
		$this->load->view('find_product_view.php',$output);
	}
	
	public function outputQuote($output = null) {
		$this->load->view('buyer_quote_manager_view.php',$output);
	}
	
	public function outputCheckQuote($output = null) {		
		$this->load->view('info_quote_view.php',$output);
	}

	public function datos() {
		$this->load->view('buyer_view');
	}
	
	public function finder() {
		$this->load->view('finder_view');
	}
	
	public function printSelection($data) {
		$this->load->model('quotes_model');
		$id_array = array();
		$id_array = explode("|", $data);
		
		if($id_array[0] == 'on') {
			$suppliers = $this->quotes_model->getProductName($id_array[1]);
		}
		else {
			$suppliers = $this->quotes_model->getProductName($id_array[0]);
		}
		
		$datos['data'] = $data;
		$datos['name'] = $suppliers->product_name;
		$this->load->view('quoter_view',$datos);
	}
	
	public function selectSupplier() {
		$datos = array(
					'data' => ''
					);
		$this->load->model('quotes_model');
        $name = $this->input->post('supplier_name_txt');
		$suppliers = $this->quotes_model->selectProducts($name);
		foreach($suppliers as $row) {
			$datos['data'] .= "<li class='suppliers_list'><a href='".site_url('buyer/findSupplier/'.$row->id.'/'.url_title($row->company_name,'_'))."'>$row->company_name</a></li>";
		}
		$this->load->view('select_supplier_view',$datos);
	}

	public function findSupplier($key,$supplier) {
		if (strpos($supplier,'_') !== false) {
			$supplier = str_replace('_',' ',$supplier);
		}

        $sql = "SELECT du.id_datos_usuario as id FROM DatosUsuario du JOIN Usuario u ON du.id_datos_usuario = u.id_datos_usuario WHERE du.id_datos_usuario = ? LIMIT 1";
        $result = $this->db->query($sql, array($key));
        $row = $result->result();

		$crud = new grocery_CRUD();
		
		$crud->where('id_supplier', $row[0]->id);
		
		$crud->set_table('productos')
			->set_subject('Product')
			->columns('id_supplier','product_name','specification','pharmacopeia','cas1','gmp','dmf','cep_cos','edqm','written_confirmation','type')
			->display_as('id_supplier','Supplier')
			->display_as('product_name','Product Name')
			->display_as('specification','Specification')
			->display_as('pharmacopeia','Pharmacopeia')
			->display_as('cas1','CAS Number')
			->display_as('gmp','GMP')
			->display_as('dmf','DMF')
			->display_as('cep_cos','CEP / COS')
			->display_as('edqm','EDQM')
			->display_as('written_confirmation','Written Confirmation')
			->display_as('type','Type');

        $crud->callback_column($this->unique_field_name('id_supplier'),array($this,'datos_usuario'));
        $crud->set_relation('id_supplier','datosusuario','company_name');
		
		$crud->callback_column('cas1',array($this,'Concatenar_CAS'));

		$crud->field_type('cas2','invisible');
		$crud->field_type('cas3','invisible');
								
		$crud->add_action('Quote', base_url('img/boton_request_quote32.png'), '','',array($this,'ask_quote'));

        $crud->callback_column('written_confirmation',array($this,'palomita'));
        $crud->callback_column('cep_cos',array($this,'palomita'));
        $crud->callback_column('edqm',array($this,'palomita'));
        $crud->callback_column('dmf',array($this,'palomita'));
		
		$crud->unset_operations();
 
		$output = $crud->render();

		$this->output($output);
	}
	
	public function findProduct() {
		$crud = new grocery_CRUD();
		
		$crud->like('product_name', $this->input->post('product_name_txt'));
		$crud->like('cas1', $this->input->post('cas1'));
		$crud->like('cas2', $this->input->post('cas2'));
		$crud->like('cas3', $this->input->post('cas3'));
		
		$crud->set_theme('check-flexigrid');
			
		$crud->set_table('productos')
			->set_subject('Product')
			->columns('id_supplier','product_name','specification','pharmacopeia','cas1','gmp','dmf','cep_cos','edqm','written_confirmation','type')
			->display_as('id_supplier','Supplier')
			->display_as('product_name','Product Name')
			->display_as('specification','Specification')
			->display_as('pharmacopeia','Pharmacopeia')
			->display_as('cas1','CAS Number')
			->display_as('gmp','GMP')
			->display_as('dmf','DMF')
			->display_as('cep_cos','CEP / COS')
			->display_as('edqm','EDQM')
			->display_as('written_confirmation','Written Confirmation')
			->display_as('type','Type');

        $crud->callback_column($this->unique_field_name('id_supplier'),array($this,'datos_usuario'));
        $crud->set_relation('id_supplier','datosusuario','company_name');
		
		$crud->callback_column('cas1',array($this,'Concatenar_CAS'));

		$crud->field_type('cas2','invisible');
		$crud->field_type('cas3','invisible');

        $crud->callback_column('written_confirmation',array($this,'palomita'));
        $crud->callback_column('cep_cos',array($this,'palomita'));
        $crud->callback_column('edqm',array($this,'palomita'));
        $crud->callback_column('dmf',array($this,'palomita'));

		$crud->unset_operations();
 
		$output = $crud->render();
		
		$this->load->model('quotes_model');
		
		$value = $this->quotes_model->number_of_rows();
		
		$output->rows = $value;

		$this->outputProduct($output);
		
	}

    public function unique_field_name($field_name) {
        return 's'.substr(md5($field_name),0,8);
    }

    public function palomita($value, $row) {
        if($value == "Yes"){
            return "<img src='".base_url('img/Check_verde.png')."' width='24'>";
        }
        else {
            return "-";
        }
    }
	
	public function quotes() {
		$crud = new grocery_CRUD();
		$cookies = $this->session->all_userdata();
		
		$crud->where('id_buyer',$cookies['usuario']);
			
		$crud->set_table('quotesbuyer')
			->set_subject('Product')
			->columns('product_name','pharmacopeia','quantity','unit','incoterm','port','due_date','status','id_supplier')
			->display_as('name','Product Name')
			->display_as('unit','Units')
			->display_as('pharmacopeia','Pharmacopeia')
			->display_as('buyer','Buyer')
			->display_as('country','Country')
			->display_as('due_date','Due Date')
			->display_as('status','Quote Status')
			->display_as('id_supplier','Sent to');

        $crud->callback_column('id_supplier',array($this,'datos_usuario'));

        $crud->callback_column('status',array($this,'desplegar_status'));

        $crud->callback_before_delete(array($this,'revisar_status'));

        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_read();
 
		$output = $crud->render();

		$this->outputQuote($output);
	}

    public function revisar_status($primary_key) {
        $this->db->where('id_quotes_buyer',$primary_key);
        $row = $this->db->get('quotesbuyer')->row();

        if($row->status == 0){
            $this->db->delete('quotessupplier', array('id_quotes_supplier' => $primary_key));
        }
        return true;
    }
	
	public function desplegar_status($value, $row) {
		if($value != 0){
			return "<a href='".base_url('buyer/checkQuotes/'.$row->id_quotes_buyer)."'>Received</a>";
		}
		else {
			return "Pending";
		}
	}
	
	public function checkQuotes($id) {
		$crud = new grocery_CRUD();
		
		$crud->where('id_quotes_buyer',$id);
			
		$crud->set_table('quotesbuyer')
			->set_subject('Product')
			->columns('id_supplier','price','currency','payment','availability','notes_supplier')
			->display_as('id_supplier','Supplier')
			->display_as('currency','Currency')
			->display_as('payment','Payment Terms')
			->display_as('notes_supplier','Notes from supplier')
			->display_as('availability','Availability');

        $crud->callback_column('id_supplier',array($this,'datos_usuario'));

        $crud->unset_operations();
 
		$output = $crud->render();
		
		$this->load->model('quotes_model');
		$value = $this->quotes_model->infoQuote($id);
		
		$output->name = $value->product_name;
		$output->unit = $value->unit;
		$output->quantity = $value->quantity;
		$output->notes_buyer = $value->notes_buyer;
		$output->pharmacopeia = $value->pharmacopeia;
		$output->incoterm = $value->incoterm;
		$output->port = $value->port;

		$this->outputCheckQuote($output);
	}
	
	public function Concatenar_CAS($value, $row) {
		return "<p>".$row->cas1." - ".$row->cas2." - ".$row->cas3."</p>";
	}
	
	public function ask_quote($primary_key , $row) {
		return base_url('buyer/supplierQuote/'.url_title($primary_key,'_').'/'.url_title($row->product_name,'_'));
	}
	
	public function supplierQuote($user,$name) {
		if (strpos($user,'_') !== false) {
			$user = str_replace('_',' ',$user);
		}
		if (strpos($name,'_') !== false) {
			$name = str_replace('_',' ',$name);
		}

		$this->load->model('quotes_model');
		$usuario = $this->quotes_model->get_Supplier($user);
		$datos = array(
					'supplier' => $usuario->id_supplier,
					'p_name' => $usuario->product_name,
                    'c_name' => $usuario->company_name
				);
		$this->load->view('supplier_quote_view',$datos);
	}

	public function send_supplier_quote() {
		$this->load->model('quotes_model');

		$this->form_validation->set_rules('name','Product Name','trim|required');
		$this->form_validation->set_rules('pharmacopeia','Pharmacopeia','trim|required');
		$this->form_validation->set_rules('quantity','Quantity','trim|required');
		$this->form_validation->set_rules('unit','Unit','trim|required');
		$this->form_validation->set_rules('incoterm','Incoterm','trim|required');
		$this->form_validation->set_rules('port','Port','trim|required');
		$this->form_validation->set_rules('limit','Limit to Quote','trim|required');
		$this->form_validation->set_rules('notes','Notes','trim');

		if ($this->form_validation->run()) {
			if($this->quotes_model->add_supplier_quote()){
				$usuario = $this->quotes_model->get_Email($this->input->post('usuario'));
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'support@apikontakt.com',
                    'smtp_pass' => 'ApiKtkt8',
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8'
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

				$product = $this->input->post('name');

				$this->email->from('support@apikontakt.com','API Kontakt');
				$this->email->to($usuario['email']);
				$this->email->subject($product.' Quote request');

                $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
                $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
                $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

                $message .= '<p>Dear <strong>'.$usuario['first_name'].' '.$usuario['last_name'].'</strong>,';
                $message .= '<p>You have received a new quote request via the API Kontakt QuoteManager.</p>';
                $message .= "Please login to your <a href='".base_url()."'>API Kontakt</a> account and start quoting!</p>";
                $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
                $this->email->message($message);

				if($this->email->send()) {
					$this->load->view('quote_sent_success_view');
				}
				else {
					echo "<br />Email failed";
				}
			}
			else{
				echo "Could not insert quote";
			}
		}
		else {
            $datos = array(
                'pharmacopeia' => $this->input->post('pharmacopeia'),
                'quantity' => $this->input->post('quantity'),
                'unit' => $this->input->post('unit'),
                'incoterm' => $this->input->post('incoterm'),
                'port' => $this->input->post('port'),
                'due_date' => $this->input->post('limit'),
                'notes_buyer' => $this->input->post('notes'),
                'supplier' => $this->input->post('usuario'),
                'p_name' => $this->input->post('name'),
                'c_name' => $this->input->post('c_name_txt')
            );
			$this->load->view('supplier_quote_view_error',$datos);
		}
	}

	public function quoter() {
		$this->load->view('quoter_view');
	}

	public function sendQuote() {
		$this->load->model('quotes_model');

		$this->form_validation->set_rules('name','Product Name','trim|required');
		$this->form_validation->set_rules('pharmacopeia','Pharmacopeia','trim|required');
		$this->form_validation->set_rules('quantity','Quantity','trim|required');
		$this->form_validation->set_rules('unit','Unit','trim|required');
		$this->form_validation->set_rules('incoterm','Incoterm','trim|required');
		$this->form_validation->set_rules('port','Port','trim|required');
		$this->form_validation->set_rules('limit','Limit to Quote','trim|required');
		$this->form_validation->set_rules('notes','Notes','trim');

		if ($this->form_validation->run()) {
			$check = FALSE;
			$id_array = array();
			$id_array = explode("|", $this->input->post('id_quotes'));
			foreach($id_array as $item){
				if($item != 'on' && $item != '') {
					$data = $this->quotes_model->get_email_suppliers_quote($item);
					if($this->quotes_model->add_quote_buyer($data['id_datos_usuario'])){

                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://smtp.googlemail.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'support@apikontakt.com',
                            'smtp_pass' => 'ApiKtkt8',
                            'mailtype'  => 'html',
                            'charset'   => 'utf-8'
                        );

                        $this->load->library('email', $config);
                        $this->email->set_newline("\r\n");
					
						$this->email->from('support@apikontakt.com','API Kontakt');
						$this->email->to($data['email']);
						$this->email->subject($this->input->post('name').' Quote request');

                        $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
                        $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
                        $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

                        $message .= '<p>Dear <strong>'.$data['first_name'].' '.$data['last_name'].'</strong>,';
                        $message .= '<p>You have received a new quote request via the API Kontakt QuoteManager.</p>';
                        $message .= "Please login to your <a href='".base_url()."'>API Kontakt</a> account and start quoting!</p>";
                        $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
                        $this->email->message($message);


						if($this->email->send()) {
							$check = TRUE;
						}
						else {
							$check = FALSE;
						}
					}
					else{
						echo "Something went wrong";
					}
				}
			}
			if($check) {
				$this->load->view('quote_sent_success_view');
			}
		}
		else {
            $datos = array(
                'pharmacopeia' => $this->input->post('pharmacopeia'),
                'quantity' => $this->input->post('quantity'),
                'unit' => $this->input->post('unit'),
                'incoterm' => $this->input->post('incoterm'),
                'port' => $this->input->post('port'),
                'due_date' => $this->input->post('limit'),
                'notes_buyer' => $this->input->post('notes'),
            );

            $this->load->model('quotes_model');
            $id_array = array();
            $id_array = explode("%7C", $this->input->post('id_quotes'));

            if($id_array[0] == 'on') {
                $suppliers = $this->quotes_model->getProductName($id_array[1]);
            }
            else {
                $suppliers = $this->quotes_model->getProductName($id_array[0]);
            }

            $datos['data'] = $this->input->post('id_quotes');
            $datos['name'] = $this->input->post('product_name');
            $this->load->view('quoter_view_error',$datos);

		}
	}
	
	public function readyQuote() {
		$this->load->model('quotes_model');
		$id_array = array();
		$id_array = explode("%7C", $this->input->post('id_quote'));
		
		if($this->quotes_model->add_quote()){
			foreach($id_array as $item){
				if($item != 'on' && $item != '') {
					$email = $this->quotes_model->get_Email($row);
					$this->load->library('email',$attributes = array('mailtype' => 'html'));
				
					$this->email->from('support@apikontakt.com','Admin');
					$this->email->to($email->email);
					$this->email->subject('Quote request');

                    $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
                    $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
                    $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

                    $message .= '<p>Dear <strong>'.$email['first_name'].' '.$email['last_name'].'</strong>,';
                    $message .= '<p>You have received a new quote request via the API Kontakt QuoteManager.</p>';
                    $message .= "Please login to your <a href='".base_url()."'>API Kontakt</a> account and start quoting!</p>";
                    $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
                    $this->email->message($message);
					
					if($this->email->send()) {
						$check = TRUE;
					}
					else {
						$check = FALSE;
					}
				}
			}
			if($check) {
				$this->load->view('quote_sent_success_view');
			}
		}
		else{
			echo "Something went wrong";
		}
	}
	
	/*
	 * Esta funcion despliega la informacion del buyer que fue seleccionado por el buyer
	 * */
    public function info($id, $user) {
        if (strpos($user,'_') !== false) {
            $user = str_replace('_',' ',$user);
        }
        $this->load->model('modelousuarios');
        $result = $this->modelousuarios->infoUsuario($id);
        $datos = array(
            'company_name' => $result->company_name,
            'company_type' => $result->company_type,
            'country' => $result->country,
            'country_code' => $result->country_code,
            'area_code' => $result->area_code,
            'phone' => $result->phone,
            'web_page' => $result->web_page,
            'prefix' => $result->prefix,
            'first_name' => $result->first_name,
            'last_name' => $result->last_name,
            'job_position' => $result->job_position,
            'email' => $result->email
        );
		$this->load->view('supplier_info_view',$datos);
	}
	
	public function suggestions() {		
		$this->load->model('search_model');
		$term = $this->input->post('term',TRUE);
		
		if (strlen($term) < 2) break;
		
		$rows = $this->search_model->GetAutocomplete(array('keyword' => $term));
		
		$json_array = array();
		foreach ($rows as $row)
			array_push($json_array, $row->product_name);
			
		echo json_encode($json_array);
    }
	
	public function suggestionsSupplier() {
		$this->load->model('search_model');
		$term = $this->input->post('palabra',TRUE);
		
		if (strlen($term) < 2) break;
		
		$rows = $this->search_model->GetAutocompleteSupplier(array('keyword' => $term));
		
		$json_array = array();
		foreach ($rows as $row)
			array_push($json_array, $row->company_name);
			
		echo json_encode($json_array);
	}
	
	public function datos_usuario($value, $row) {
        $sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
        $result = $this->db->query($sql, array($row->id_supplier));
        $fila = $result->row();
		return "<a href='".site_url('buyer/info/'.$row->id_supplier."/".url_title($fila->company_name,'_'))."'>$fila->company_name</a>";
	}
	
	public function profile() {
		$this->load->view('buyer_profile_view');
	}
	
	public function update_info() {
		$this->load->view('update_profile_buyer_view');
	}
	
	public function validate_update() {
        $this->form_validation->set_rules('companyname_txt','Company Name','trim|required');
        $this->form_validation->set_rules('country_txt','Country','trim|required');
        $this->form_validation->set_rules('city_txt','City','trim|required');
        $this->form_validation->set_rules('street_txt','Street Address','trim|required');
        $this->form_validation->set_rules('zip_txt','ZIP/Postal Code','trim|required');
        $this->form_validation->set_rules('countrycode_txt','Country Code','trim|required');
        $this->form_validation->set_rules('areacode_txt','Area Code','trim|required');
        $this->form_validation->set_rules('phone_txt','Phone Number','trim|required');
        $this->form_validation->set_rules('page_txt','Web Page','trim|required');
        $this->form_validation->set_rules('prefix_txt','Title','trim|required');
        $this->form_validation->set_rules('fname_txt','First Name','trim|required');
        $this->form_validation->set_rules('lname_txt','Last Name','trim|required');
        $this->form_validation->set_rules('position_txt','Job Position','trim|required');
        $this->form_validation->set_rules('email_txt','Email','trim|required|valid_email');
        $this->form_validation->set_rules('cemail_txt','Confirm Email','trim|required|valid_email|matches[email_txt]');
        $this->form_validation->set_rules('user_txt','User Name','trim|required');
        $this->form_validation->set_rules('password_txt','Password','trim|required');
        $this->form_validation->set_rules('cpassword_txt','Confirm Password','trim|required|matches[password_txt]');
		
		if ($this->form_validation->run()) {
			
			$this->load->model('modelousuarios');
			
			if($this->modelousuarios->updateBuyer()){
				$this->load->view('buyer_update_success_view');
			}
			else {
				echo "<br />Couldn't update data";
			}
			
		}
		else {
            $this->load->view('update_profile_buyer_view');
		}
	}

}