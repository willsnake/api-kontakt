<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct() {
		parent::__construct();
        if (!$this->session->userdata('logueado')) {
            redirect('main/restringido');
        }
        elseif($this->session->userdata('tipo_usuario') == 3 && $this->session->userdata('logueado') == 1){
            redirect('buyer');
        }

	}

	public function index() {
		$cookies = $this->session->all_userdata();
		if ($cookies['logueado'] != 1 && $cookies['tipo_usuario'] != 2) {
			redirect('main/restringido');
		}
		else {
			$this->datos();
		}
	}

	/*Esta es la funcion que despliega los productos de los suppliers*/
    public function outputProduct($output = null) {
		$this->load->view('my_products_view.php',$output);
	}

    /*Esta es la funcion que despliega las cotizaciones que le llegan a los suppliers*/
    public function outputQuote($output = null) {
		$this->load->view('quotes_view.php',$output);
	}

	public function datos() {
		$this->load->view('supplier_view');
	}

	/*
	 * Esta funcion se encarga de desplegar una tabla donde los suppliers pueden agregar, editar o borrar sus productos
	 * */
    public function my_products() {
		$crud = new grocery_CRUD();
		$cookies = $this->session->all_userdata();

		$crud->where('id_supplier',$cookies['usuario']);

        $crud->set_table("productos")
			->set_subject('Product')
			->columns('product_name','specification','pharmacopeia','cas1','gmp','dmf','cep_cos','edqm','written_confirmation','type')
			->display_as('product_name','Product Name')
			->display_as('specification','Specification')
			->display_as('pharmacopeia','Pharmacopeia')
			->display_as('cas1','CAS Number')
			->display_as('gmp','GMP')
			->display_as('written_confirmation','Written Confirmation')
			->display_as('cep_cos','CEP')
			->display_as('edqm','EDQM')
			->display_as('dmf','DMF')
			->display_as('type','Type');

		$crud->required_fields('product_name','pharmacopeia','gmp','written_confirmation','cep_cos','edqm','dmf','type');

		$crud->fields('product_name','specification','pharmacopeia','cas1','cas2','cas3','gmp','dmf','cep_cos','edqm','written_confirmation','type','id_supplier');

		$crud->field_type('cas2','invisible');
		$crud->field_type('cas3','invisible');
        $crud->field_type('id_supplier','invisible');

		$crud->field_type('pharmacopeia','multiselect',
							array("USP" => "USP", "BP" => "BP","EP" => "EP","CP" => "CP","JP" => "JP","IP" => "IP","In House" => "In House","Other" => "Other"));
		$crud->field_type('written_confirmation','dropdown',
            array("Yes" => "Yes", "No" => "No"));
		$crud->field_type('cep_cos','dropdown',
            array("Yes" => "Yes", "No" => "No"));
		$crud->field_type('edqm','dropdown',
            array("Yes" => "Yes", "No" => "No"));
		$crud->field_type('dmf','dropdown',
            array("Yes" => "Yes", "No" => "No"));

		$crud->field_type('gmp','multiselect',
								array(
									"No" => "No",
									"WHO GMP" => "WHO GMP",
									"US FDA" => "US FDA",
									"COFEPRIS" => "COFEPRIS",
									"ANVISA" => "ANVISA",
									"TGA" => "TGA",
									"Afghanistan"	=>	"Afghanistan",
									"Albania"	=>	"Albania",
									"Algeria"	=>	"Algeria",
									"American Samoa"	=>	"American Samoa",
									"Andorra"	=>	"Andorra",
									"Angola"	=>	"Angola",
									"Anguilla"	=>	"Anguilla",
									"Antarctica"	=>	"Antarctica",
									"Antigua and Barbuda"	=>	"Antigua and Barbuda",
									"Argentina"	=>	"Argentina",
									"Armenia"	=>	"Armenia",
									"Aruba"	=>	"Aruba",
									"Australia"	=>	"Australia",
									"Austria"	=>	"Austria",
									"Azerbaijan"	=>	"Azerbaijan",
									"Bahamas"	=>	"Bahamas",
									"Bahrain"	=>	"Bahrain",
									"Bangladesh"	=>	"Bangladesh",
									"Barbados"	=>	"Barbados",
									"Belarus"	=>	"Belarus",
									"Belgium"	=>	"Belgium",
									"Belize"	=>	"Belize",
									"Benin"	=>	"Benin",
									"Bermuda"	=>	"Bermuda",
									"Bhutan"	=>	"Bhutan",
									"Bolivia"	=>	"Bolivia",
									"Bosnia and Herzegovina"	=>	"Bosnia and Herzegovina",
									"Botswana"	=>	"Botswana",
									"Brazil"	=>	"Brazil",
									"Brunei Darussalam"	=>	"Brunei Darussalam",
									"Bulgaria"	=>	"Bulgaria",
									"Burkina Faso"	=>	"Burkina Faso",
									"Burundi"	=>	"Burundi",
									"Cambodia"	=>	"Cambodia",
									"Cameroon"	=>	"Cameroon",
									"Canada"	=>	"Canada",
									"Cape Verde"	=>	"Cape Verde",
									"Cayman Islands"	=>	"Cayman Islands",
									"Central African Republic"	=>	"Central African Republic",
									"Chad"	=>	"Chad",
									"Chile"	=>	"Chile",
									"China"	=>	"China",
									"Christmas Island"	=>	"Christmas Island",
									"Cocos (Keeling) Islands"	=>	"Cocos (Keeling) Islands",
									"Colombia"	=>	"Colombia",
									"Comoros"	=>	"Comoros",
									"Democratic Republic of the Congo (Kinshasa)"	=>	"Democratic Republic of the Congo (Kinshasa)",
									"Congo, Republic of (Brazzaville)"	=>	"Congo, Republic of (Brazzaville)",
									"Cook Islands"	=>	"Cook Islands",
									"Costa Rica"	=>	"Costa Rica",
									"Ivory Coast (Cote d'Ivoire)"	=>	"Ivory Coast (Cote d'Ivoire)",
									"Croatia"	=>	"Croatia",
									"Cuba"	=>	"Cuba",
									"Cyprus"	=>	"Cyprus",
									"Czech Republic"	=>	"Czech Republic",
									"Denmark"	=>	"Denmark",
									"Djibouti"	=>	"Djibouti",
									"Dominica"	=>	"Dominica",
									"Dominican Republic"	=>	"Dominican Republic",
									"East Timor Timor-Leste"	=>	"East Timor Timor-Leste",
									"Ecuador"	=>	"Ecuador",
									"Egypt"	=>	"Egypt",
									"El Salvador"	=>	"El Salvador",
									"Equatorial Guinea"	=>	"Equatorial Guinea",
									"Eritrea"	=>	"Eritrea",
									"Estonia"	=>	"Estonia",
									"Ethiopia"	=>	"Ethiopia",
									"Falkland Islands"	=>	"Falkland Islands",
									"Faroe Islands"	=>	"Faroe Islands",
									"Fiji"	=>	"Fiji",
									"Finland"	=>	"Finland",
									"France"	=>	"France",
									"French Guiana"	=>	"French Guiana",
									"French Polynesia"	=>	"French Polynesia",
									"French Southern Territories"	=>	"French Southern Territories",
									"Gabon"	=>	"Gabon",
									"Gambia"	=>	"Gambia",
									"Georgia"	=>	"Georgia",
									"Germany"	=>	"Germany",
									"Ghana"	=>	"Ghana",
									"Gibraltar"	=>	"Gibraltar",
									"Great Britain"	=>	"Great Britain",
									"Greece"	=>	"Greece",
									"Greenland"	=>	"Greenland",
									"Grenada"	=>	"Grenada",
									"Guadeloupe"	=>	"Guadeloupe",
									"Guam"	=>	"Guam",
									"Guatemala"	=>	"Guatemala",
									"Guinea"	=>	"Guinea",
									"Guinea-Bissau"	=>	"Guinea-Bissau",
									"Guyana"	=>	"Guyana",
									"Haiti"	=>	"Haiti",
									"Holy See"	=>	"Holy See",
									"Honduras"	=>	"Honduras",
									"Hong Kong"	=>	"Hong Kong",
									"Hungary"	=>	"Hungary",
									"Iceland"	=>	"Iceland",
									"India"	=>	"India",
									"Indonesia"	=>	"Indonesia",
									"Iran (Islamic Republic of)"	=>	"Iran (Islamic Republic of)",
									"Iraq"	=>	"Iraq",
									"Ireland"	=>	"Ireland",
									"Israel"	=>	"Israel",
									"Italy"	=>	"Italy",
									"Jamaica"	=>	"Jamaica",
									"Japan"	=>	"Japan",
									"Jordan"	=>	"Jordan",
									"Kazakhstan"	=>	"Kazakhstan",
									"Kenya"	=>	"Kenya",
									"Kiribati"	=>	"Kiribati",
									"Korea, Democratic People's Rep. (North Korea)"	=>	"Korea, Democratic People's Rep. (North Korea)",
									"Korea, Republic of (South Korea)"	=>	"Korea, Republic of (South Korea)",
									"Kosovo"	=>	"Kosovo",
									"Kuwait"	=>	"Kuwait",
									"Kyrgyzstan"	=>	"Kyrgyzstan",
									"Lao, People's Democratic Republic"	=>	"Lao, People's Democratic Republic",
									"Latvia"	=>	"Latvia",
									"Lebanon"	=>	"Lebanon",
									"Lesotho"	=>	"Lesotho",
									"Liberia"	=>	"Liberia",
									"Libya"	=>	"Libya",
									"Liechtenstein"	=>	"Liechtenstein",
									"Lithuania"	=>	"Lithuania",
									"Luxembourg"	=>	"Luxembourg",
									"Macao"	=>	"Macao",
									"Macedonia, Rep. Of"	=>	"Macedonia, Rep. Of",
									"Madagascar"	=>	"Madagascar",
									"Malawi"	=>	"Malawi",
									"Malaysia"	=>	"Malaysia",
									"Maldives"	=>	"Maldives",
									"Mali"	=>	"Mali",
									"Malta"	=>	"Malta",
									"Marshall Islands"	=>	"Marshall Islands",
									"Martinique"	=>	"Martinique",
									"Mauritania"	=>	"Mauritania",
									"Mauritius"	=>	"Mauritius",
									"Mayotte"	=>	"Mayotte",
									"Mexico"	=>	"Mexico",
									"Micronesia, Federal States of"	=>	"Micronesia, Federal States of",
									"Moldova, Republic of"	=>	"Moldova, Republic of",
									"Monaco"	=>	"Monaco",
									"Mongolia"	=>	"Mongolia",
									"Montenegro"	=>	"Montenegro",
									"Montserrat"	=>	"Montserrat",
									"Morocco"	=>	"Morocco",
									"Mozambique"	=>	"Mozambique",
									"Myanmar, Burma"	=>	"Myanmar, Burma",
									"Namibia"	=>	"Namibia",
									"Nauru"	=>	"Nauru",
									"Nepal"	=>	"Nepal",
									"Netherlands"	=>	"Netherlands",
									"Netherlands Antilles"	=>	"Netherlands Antilles",
									"New Caledonia"	=>	"New Caledonia",
									"New Zealand"	=>	"New Zealand",
									"Nicaragua"	=>	"Nicaragua",
									"Niger"	=>	"Niger",
									"Nigeria"	=>	"Nigeria",
									"Niue"	=>	"Niue",
									"Northern Mariana Islands"	=>	"Northern Mariana Islands",
									"Norway"	=>	"Norway",
									"Oman"	=>	"Oman",
									"Pakistan"	=>	"Pakistan",
									"Palau"	=>	"Palau",
									"Palestinian territories"	=>	"Palestinian territories",
									"Panama"	=>	"Panama",
									"Papua New Guinea"	=>	"Papua New Guinea",
									"Paraguay"	=>	"Paraguay",
									"Peru"	=>	"Peru",
									"Philippines"	=>	"Philippines",
									"Pitcairn Island"	=>	"Pitcairn Island",
									"Poland"	=>	"Poland",
									"Portugal"	=>	"Portugal",
									"Puerto Rico"	=>	"Puerto Rico",
									"Qatar"	=>	"Qatar",
									"Reunion Island"	=>	"Reunion Island",
									"Romania"	=>	"Romania",
									"Russian Federation"	=>	"Russian Federation",
									"Rwanda"	=>	"Rwanda",
									"Saint Kitts and Nevis"	=>	"Saint Kitts and Nevis",
									"Saint Lucia"	=>	"Saint Lucia",
									"Saint Vincent and the Grenadines"	=>	"Saint Vincent and the Grenadines",
									"Samoa"	=>	"Samoa",
									"San Marino"	=>	"San Marino",
									"Sao Tome and Príncipe"	=>	"Sao Tome and Príncipe",
									"Saudi Arabia"	=>	"Saudi Arabia",
									"Senegal"	=>	"Senegal",
									"Serbia"	=>	"Serbia",
									"Seychelles"	=>	"Seychelles",
									"Sierra Leone"	=>	"Sierra Leone",
									"Singapore"	=>	"Singapore",
									"Slovakia (Slovak Republic)"	=>	"Slovakia (Slovak Republic)",
									"Slovenia"	=>	"Slovenia",
									"Solomon Islands"	=>	"Solomon Islands",
									"Somalia"	=>	"Somalia",
									"South Africa"	=>	"South Africa",
									"South Sudan"	=>	"South Sudan",
									"Spain"	=>	"Spain",
									"Sri Lanka"	=>	"Sri Lanka",
									"Sudan"	=>	"Sudan",
									"Suriname"	=>	"Suriname",
									"Swaziland"	=>	"Swaziland",
									"Sweden"	=>	"Sweden",
									"Switzerland"	=>	"Switzerland",
									"Syria, Syrian Arab Republic"	=>	"Syria, Syrian Arab Republic",
									"Taiwan (Republic of China)"	=>	"Taiwan (Republic of China)",
									"Tajikistan"	=>	"Tajikistan",
									"Tanzania"	=>	"Tanzania",
									"Thailand"	=>	"Thailand",
									"Tibet"	=>	"Tibet",
									"Timor-Leste (East Timor)"	=>	"Timor-Leste (East Timor)",
									"Togo"	=>	"Togo",
									"Tokelau"	=>	"Tokelau",
									"Tonga"	=>	"Tonga",
									"Trinidad and Tobago"	=>	"Trinidad and Tobago",
									"Tunisia"	=>	"Tunisia",
									"Turkey"	=>	"Turkey",
									"Turkmenistan"	=>	"Turkmenistan",
									"Turks and Caicos Islands"	=>	"Turks and Caicos Islands",
									"Tuvalu"	=>	"Tuvalu",
									"Uganda"	=>	"Uganda",
									"Ukraine"	=>	"Ukraine",
									"United Arab Emirates"	=>	"United Arab Emirates",
									"United Kingdom"	=>	"United Kingdom",
									"United States"	=>	"United States",
									"Uruguay"	=>	"Uruguay",
									"Uzbekistan"	=>	"Uzbekistan",
									"Vanuatu"	=>	"Vanuatu",
									"Vatican City State (Holy See)"	=>	"Vatican City State (Holy See)",
									"Venezuela"	=>	"Venezuela",
									"Vietnam"	=>	"Vietnam",
									"Virgin Islands (British)"	=>	"Virgin Islands (British)",
									"Virgin Islands (U.S.)"	=>	"Virgin Islands (U.S.)",
									"Wallis and Futuna Islands"	=>	"Wallis and Futuna Islands",
									"Western Sahara"	=>	"Western Sahara",
									"Yemen"	=>	"Yemen",
									"Zambia"	=>	"Zambia",
									"Zimbabwe"	=>	"Zimbabwe"
								));

		$crud->callback_column('cas1',array($this,'Concatenar_CAS'));

		$crud->field_type('type','dropdown',
                                array( "API"  => "API", "Intermediate" => "Intermediate", "Excipient" => "Excipient"));

        $crud->callback_add_field('cas1',array($this,'Agregar_Cas_Number'));
		$crud->callback_edit_field('cas1',array($this,'Editar_Cas_Number'));

        $crud->callback_add_field('pharmacopeia',array($this,'Agregar_Explicacion_Pharma'));

        $crud->callback_add_field('gmp',array($this,'Agregar_Explicacion_GMP'));

        $crud->callback_column('written_confirmation',array($this,'palomita'));
        $crud->callback_column('cep_cos',array($this,'palomita'));
        $crud->callback_column('edqm',array($this,'palomita'));
        $crud->callback_column('dmf',array($this,'palomita'));

		$crud->unset_export();
		$crud->unset_print();
		$crud->unset_read();

		$crud->callback_before_insert(array($this,'Preparar_Datos'));
		$crud->callback_after_insert(array($this, 'Resetear_Variables'));

		$output = $crud->render();

		$this->outputProduct($output);
	}

    public function Agregar_Explicacion_Pharma() {
        return '<select id="field-pharmacopeia" name="pharmacopeia[]" multiple="multiple" size="8" class="chosen-multiple-select" data-placeholder="Select one or more" style="width:510px;" ><option value="USP"  >USP</option><option value="BP"  >BP</option><option value="EP"  >EP</option><option value="CP"  >CP</option><option value="JP"  >JP</option><option value="IP"  >IP</option><option value="In House"  >In House</option><option value="Other"  >Other</option></select>';
    }

    public function Agregar_Explicacion_GMP() {
        return "<select id='field-gmp' name='gmp[]' multiple='multiple' size='8' class='chosen-multiple-select' data-placeholder='Select one or more' style='width:510px;' ><option value='No'  >No</option><option value='WHO GMP'  >WHO GMP</option><option value='US FDA'  >US FDA</option><option value='COFEPRIS'  >COFEPRIS</option><option value='ANVISA'  >ANVISA</option><option value='TGA'  >TGA</option><option value='Afghanistan'  >Afghanistan</option><option value='Albania'  >Albania</option><option value='Algeria'  >Algeria</option><option value='American Samoa'  >American Samoa</option><option value='Andorra'  >Andorra</option><option value='Angola'  >Angola</option><option value='Anguilla'  >Anguilla</option><option value='Antarctica'  >Antarctica</option><option value='Antigua and Barbuda'  >Antigua and Barbuda</option><option value='Argentina'  >Argentina</option><option value='Armenia'  >Armenia</option><option value='Aruba'  >Aruba</option><option value='Australia'  >Australia</option><option value='Austria'  >Austria</option><option value='Azerbaijan'  >Azerbaijan</option><option value='Bahamas'  >Bahamas</option><option value='Bahrain'  >Bahrain</option><option value='Bangladesh'  >Bangladesh</option><option value='Barbados'  >Barbados</option><option value='Belarus'  >Belarus</option><option value='Belgium'  >Belgium</option><option value='Belize'  >Belize</option><option value='Benin'  >Benin</option><option value='Bermuda'  >Bermuda</option><option value='Bhutan'  >Bhutan</option><option value='Bolivia'  >Bolivia</option><option value='Bosnia and Herzegovina'  >Bosnia and Herzegovina</option><option value='Botswana'  >Botswana</option><option value='Brazil'  >Brazil</option><option value='Brunei Darussalam'  >Brunei Darussalam</option><option value='Bulgaria'  >Bulgaria</option><option value='Burkina Faso'  >Burkina Faso</option><option value='Burundi'  >Burundi</option><option value='Cambodia'  >Cambodia</option><option value='Cameroon'  >Cameroon</option><option value='Canada'  >Canada</option><option value='Cape Verde'  >Cape Verde</option><option value='Cayman Islands'  >Cayman Islands</option><option value='Central African Republic'  >Central African Republic</option><option value='Chad'  >Chad</option><option value='Chile'  >Chile</option><option value='China'  >China</option><option value='Christmas Island'  >Christmas Island</option><option value='Cocos (Keeling) Islands'  >Cocos (Keeling) Islands</option><option value='Colombia'  >Colombia</option><option value='Comoros'  >Comoros</option><option value='Democratic Republic of the Congo (Kinshasa)'  >Democratic Republic of the Congo (Kinshasa)</option><option value='Congo, Republic of (Brazzaville)'  >Congo, Republic of (Brazzaville)</option><option value='Cook Islands'  >Cook Islands</option><option value='Costa Rica'  >Costa Rica</option><option value='Ivory Coast (Cote d'Ivoire)'  >Ivory Coast (Cote d'Ivoire)</option><option value='Croatia'  >Croatia</option><option value='Cuba'  >Cuba</option><option value='Cyprus'  >Cyprus</option><option value='Czech Republic'  >Czech Republic</option><option value='Denmark'  >Denmark</option><option value='Djibouti'  >Djibouti</option><option value='Dominica'  >Dominica</option><option value='Dominican Republic'  >Dominican Republic</option><option value='East Timor Timor-Leste'  >East Timor Timor-Leste</option><option value='Ecuador'  >Ecuador</option><option value='Egypt'  >Egypt</option><option value='El Salvador'  >El Salvador</option><option value='Equatorial Guinea'  >Equatorial Guinea</option><option value='Eritrea'  >Eritrea</option><option value='Estonia'  >Estonia</option><option value='Ethiopia'  >Ethiopia</option><option value='Falkland Islands'  >Falkland Islands</option><option value='Faroe Islands'  >Faroe Islands</option><option value='Fiji'  >Fiji</option><option value='Finland'  >Finland</option><option value='France'  >France</option><option value='French Guiana'  >French Guiana</option><option value='French Polynesia'  >French Polynesia</option><option value='French Southern Territories'  >French Southern Territories</option><option value='Gabon'  >Gabon</option><option value='Gambia'  >Gambia</option><option value='Georgia'  >Georgia</option><option value='Germany'  >Germany</option><option value='Ghana'  >Ghana</option><option value='Gibraltar'  >Gibraltar</option><option value='Great Britain'  >Great Britain</option><option value='Greece'  >Greece</option><option value='Greenland'  >Greenland</option><option value='Grenada'  >Grenada</option><option value='Guadeloupe'  >Guadeloupe</option><option value='Guam'  >Guam</option><option value='Guatemala'  >Guatemala</option><option value='Guinea'  >Guinea</option><option value='Guinea-Bissau'  >Guinea-Bissau</option><option value='Guyana'  >Guyana</option><option value='Haiti'  >Haiti</option><option value='Holy See'  >Holy See</option><option value='Honduras'  >Honduras</option><option value='Hong Kong'  >Hong Kong</option><option value='Hungary'  >Hungary</option><option value='Iceland'  >Iceland</option><option value='India'  >India</option><option value='Indonesia'  >Indonesia</option><option value='Iran (Islamic Republic of)'  >Iran (Islamic Republic of)</option><option value='Iraq'  >Iraq</option><option value='Ireland'  >Ireland</option><option value='Israel'  >Israel</option><option value='Italy'  >Italy</option><option value='Jamaica'  >Jamaica</option><option value='Japan'  >Japan</option><option value='Jordan'  >Jordan</option><option value='Kazakhstan'  >Kazakhstan</option><option value='Kenya'  >Kenya</option><option value='Kiribati'  >Kiribati</option><option value='Korea, Democratic People's Rep. (North Korea)'  >Korea, Democratic People's Rep. (North Korea)</option><option value='Korea, Republic of (South Korea)'  >Korea, Republic of (South Korea)</option><option value='Kosovo'  >Kosovo</option><option value='Kuwait'  >Kuwait</option><option value='Kyrgyzstan'  >Kyrgyzstan</option><option value='Lao, People's Democratic Republic'  >Lao, People's Democratic Republic</option><option value='Latvia'  >Latvia</option><option value='Lebanon'  >Lebanon</option><option value='Lesotho'  >Lesotho</option><option value='Liberia'  >Liberia</option><option value='Libya'  >Libya</option><option value='Liechtenstein'  >Liechtenstein</option><option value='Lithuania'  >Lithuania</option><option value='Luxembourg'  >Luxembourg</option><option value='Macao'  >Macao</option><option value='Macedonia, Rep. Of'  >Macedonia, Rep. Of</option><option value='Madagascar'  >Madagascar</option><option value='Malawi'  >Malawi</option><option value='Malaysia'  >Malaysia</option><option value='Maldives'  >Maldives</option><option value='Mali'  >Mali</option><option value='Malta'  >Malta</option><option value='Marshall Islands'  >Marshall Islands</option><option value='Martinique'  >Martinique</option><option value='Mauritania'  >Mauritania</option><option value='Mauritius'  >Mauritius</option><option value='Mayotte'  >Mayotte</option><option value='Mexico'  >Mexico</option><option value='Micronesia, Federal States of'  >Micronesia, Federal States of</option><option value='Moldova, Republic of'  >Moldova, Republic of</option><option value='Monaco'  >Monaco</option><option value='Mongolia'  >Mongolia</option><option value='Montenegro'  >Montenegro</option><option value='Montserrat'  >Montserrat</option><option value='Morocco'  >Morocco</option><option value='Mozambique'  >Mozambique</option><option value='Myanmar, Burma'  >Myanmar, Burma</option><option value='Namibia'  >Namibia</option><option value='Nauru'  >Nauru</option><option value='Nepal'  >Nepal</option><option value='Netherlands'  >Netherlands</option><option value='Netherlands Antilles'  >Netherlands Antilles</option><option value='New Caledonia'  >New Caledonia</option><option value='New Zealand'  >New Zealand</option><option value='Nicaragua'  >Nicaragua</option><option value='Niger'  >Niger</option><option value='Nigeria'  >Nigeria</option><option value='Niue'  >Niue</option><option value='Northern Mariana Islands'  >Northern Mariana Islands</option><option value='Norway'  >Norway</option><option value='Oman'  >Oman</option><option value='Pakistan'  >Pakistan</option><option value='Palau'  >Palau</option><option value='Palestinian territories'  >Palestinian territories</option><option value='Panama'  >Panama</option><option value='Papua New Guinea'  >Papua New Guinea</option><option value='Paraguay'  >Paraguay</option><option value='Peru'  >Peru</option><option value='Philippines'  >Philippines</option><option value='Pitcairn Island'  >Pitcairn Island</option><option value='Poland'  >Poland</option><option value='Portugal'  >Portugal</option><option value='Puerto Rico'  >Puerto Rico</option><option value='Qatar'  >Qatar</option><option value='Reunion Island'  >Reunion Island</option><option value='Romania'  >Romania</option><option value='Russian Federation'  >Russian Federation</option><option value='Rwanda'  >Rwanda</option><option value='Saint Kitts and Nevis'  >Saint Kitts and Nevis</option><option value='Saint Lucia'  >Saint Lucia</option><option value='Saint Vincent and the Grenadines'  >Saint Vincent and the Grenadines</option><option value='Samoa'  >Samoa</option><option value='San Marino'  >San Marino</option><option value='Sao Tome and Príncipe'  >Sao Tome and Príncipe</option><option value='Saudi Arabia'  >Saudi Arabia</option><option value='Senegal'  >Senegal</option><option value='Serbia'  >Serbia</option><option value='Seychelles'  >Seychelles</option><option value='Sierra Leone'  >Sierra Leone</option><option value='Singapore'  >Singapore</option><option value='Slovakia (Slovak Republic)'  >Slovakia (Slovak Republic)</option><option value='Slovenia'  >Slovenia</option><option value='Solomon Islands'  >Solomon Islands</option><option value='Somalia'  >Somalia</option><option value='South Africa'  >South Africa</option><option value='South Sudan'  >South Sudan</option><option value='Spain'  >Spain</option><option value='Sri Lanka'  >Sri Lanka</option><option value='Sudan'  >Sudan</option><option value='Suriname'  >Suriname</option><option value='Swaziland'  >Swaziland</option><option value='Sweden'  >Sweden</option><option value='Switzerland'  >Switzerland</option><option value='Syria, Syrian Arab Republic'  >Syria, Syrian Arab Republic</option><option value='Taiwan (Republic of China)'  >Taiwan (Republic of China)</option><option value='Tajikistan'  >Tajikistan</option><option value='Tanzania'  >Tanzania</option><option value='Thailand'  >Thailand</option><option value='Tibet'  >Tibet</option><option value='Timor-Leste (East Timor)'  >Timor-Leste (East Timor)</option><option value='Togo'  >Togo</option><option value='Tokelau'  >Tokelau</option><option value='Tonga'  >Tonga</option><option value='Trinidad and Tobago'  >Trinidad and Tobago</option><option value='Tunisia'  >Tunisia</option><option value='Turkey'  >Turkey</option><option value='Turkmenistan'  >Turkmenistan</option><option value='Turks and Caicos Islands'  >Turks and Caicos Islands</option><option value='Tuvalu'  >Tuvalu</option><option value='Uganda'  >Uganda</option><option value='Ukraine'  >Ukraine</option><option value='United Arab Emirates'  >United Arab Emirates</option><option value='United Kingdom'  >United Kingdom</option><option value='United States'  >United States</option><option value='Uruguay'  >Uruguay</option><option value='Uzbekistan'  >Uzbekistan</option><option value='Vanuatu'  >Vanuatu</option><option value='Vatican City State (Holy See)'  >Vatican City State (Holy See)</option><option value='Venezuela'  >Venezuela</option><option value='Vietnam'  >Vietnam</option><option value='Virgin Islands (British)'  >Virgin Islands (British)</option><option value='Virgin Islands (U.S.)'  >Virgin Islands (U.S.)</option><option value='Wallis and Futuna Islands'  >Wallis and Futuna Islands</option><option value='Western Sahara'  >Western Sahara</option><option value='Yemen'  >Yemen</option><option value='Zambia'  >Zambia</option><option value='Zimbabwe'  >Zimbabwe</option></select>";
//        return '<input type="text" value="" name="pharmacopeia" placeholder="You can select multiple GMPs">';
    }

    public function Agregar_Explicacion_Specification() {
        return '<input type="text" value="" name="specification" placeholder="Example: 90%, etc">';
    }

    public function palomita($value, $row) {
        if($value == "Yes"){
            return "<img src='".base_url('img/Check_azul.png')."' width='24'>";
        }
        else {
            return "-";
        }
    }

	public function Concatenar_CAS($value, $row) {
		return "<p>".$row->cas1." - ".$row->cas2." - ".$row->cas3."</p>";
	}

	public function Agregar_Cas_Number() {
		 return '<input type="text" maxlength="7" name="cas1" style="width:22%"> - <input type="text" maxlength="2" name="cas2" style="width:22%"> - <input type="text" maxlength="1" name="cas3" style="width:22%">';
	}

	public function Editar_Cas_Number($value, $primary_key) {
		$this->load->model('modelousuarios');
		$result = $this->modelousuarios->get_CAS($primary_key);
		return '<input type="text" maxlength="7" name="cas1" style="width:22%" value="'.$result->cas1.'"> - <input type="text" maxlength="2" name="cas2" style="width:22%" value="'.$result->cas2.'"> - <input type="text" maxlength="1" name="cas3" style="width:22%" value="'.$result->cas3.'">';
	}

	public function Preparar_Datos($post_array) {
		$cookies = $this->session->all_userdata();
		$post_array['product_name'] = ucwords(strtolower($this->input->post('product_name')));
		$post_array['cas1'] = $this->input->post('cas1');
		$post_array['cas2'] = $this->input->post('cas2');
		$post_array['cas3'] = $this->input->post('cas3');
        $post_array['id_supplier'] = $cookies['usuario'];
		return $post_array;
	}

	public function Resetear_Variables($post_array,$primary_key) {
		unset($_POST);
		return true;
	}

	/*
	 * Esta funcion se encarga de manejar las cotizaciones que le llegan al supplier
	 * */
    public function quotes() {
		$crud = new grocery_CRUD();
		$cookies = $this->session->all_userdata();

		$crud->where('id_supplier',$cookies['usuario']);

		$crud->set_table('quotessupplier')
			->set_subject('Quotes')
			->columns('product_name','pharmacopeia','quantity','unit','incoterm','port','due_date','id_buyer','status','notes_buyer')
			->display_as('product_name','Product Name')
			->display_as('unit','Units')
			->display_as('pharmacopeia','Pharmacopeia')
			->display_as('id_buyer','Buyer')
			->display_as('country','Country')
			->display_as('due_date','Due Date')
			->display_as('notes_buyer','Notes from buyer')
			->display_as('status','Status');

        $crud->callback_column($this->unique_field_name('id_buyer'),array($this,'datos_usuario'));
        $crud->set_relation('id_buyer','datosusuario','company_name');

		$crud->callback_column('status',array($this,'desplegar_status'));

        $crud->callback_before_delete(array($this,'revisar_status'));

        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_read();

		$output = $crud->render();

		$this->outputQuote($output);
	}

    public function revisar_status($primary_key) {
        $this->db->where('id_quotes_supplier',$primary_key);
        $row = $this->db->get('quotessupplier')->row();

        if($row->status == 0){
            $this->db->delete('quotesbuyer', array('id_quotes_buyer' => $primary_key));
        }
        return true;
    }

    public function unique_field_name($field_name) {
        return 's'.substr(md5($field_name),0,8);
    }

	public function datos_usuario($value, $row) {
		return "<a href='".site_url('supplier/info/'.$row->id_buyer."/".url_title($value,'_'))."'>$value</a>";
	}

	public function desplegar_status($value, $row) {
		if($value == 1){
			return "<a href='".base_url('supplier/checkQuote/'.$row->id_quotes_supplier)."'>Quote Sent<a/>";
		}
		else {
			return "<a href='".base_url('supplier/sendQuote/'.$row->id_quotes_supplier)."'>Answer Quote</a>";
		}
	}

	public function info($id, $user) {
		if (strpos($user,'_') !== false) {
			$user = str_replace('_',' ',$user);
		}
		$this->load->model('modelousuarios');
		$result = $this->modelousuarios->infoUsuario($id);
			$datos = array(
				'company_name' => $result->company_name,
				'company_type' => $result->company_type,
				'country' => $result->country,
				'country_code' => $result->country_code,
				'area_code' => $result->area_code,
				'phone' => $result->phone,
				'web_page' => $result->web_page,
				'prefix' => $result->prefix,
				'first_name' => $result->first_name,
				'last_name' => $result->last_name,
				'job_position' => $result->job_position,
				'email' => $result->email
				);
		$this->load->view('buyer_info_view',$datos);
	}

	/*
	 * Esta funcion se encarga de desplegar la cotizacion que fue seleccionada por el supplier, y abre un formulario donde el supplier
	 * puede responder la cotizacion.
	 * */
    public function sendQuote($id) {
		$this->load->model('quotes_model');
		$result = $this->quotes_model->sendQuote($id);
		$result->id = $id;
		$this->load->view('send_quote_view',$result);
	}

	/*
	 * Esta funcion se encarga de validar los datos enviados por el supplier y desplegar una pantalla de confirmacion
	 * */
    public function sendQuote_form() {
        $id = $this->input->post('id_quote');
        $this->load->model('quotes_model');
        $result = $this->quotes_model->sendQuote($id);
        $result->id = $id;

		$this->form_validation->set_rules('price','Price','trim|required|xss_clean');
		$this->form_validation->set_rules('currency','Currency','trim|required|xss_clean');
		$this->form_validation->set_rules('payment','Payment Terms','trim|required|xss_clean');
		$this->form_validation->set_rules('availability','Availability','trim|required|xss_clean');
		$this->form_validation->set_rules('notes','Notes','trim|xss_clean');

		if ($this->form_validation->run()) {
            $this->load->view('quote_information_view',$result);
		}
		else {
			$this->load->model('quotes_model');
            $result = $this->quotes_model->sendQuote($this->input->post('id_quote'));
            $datos = array(
                'id_quote_request' => $this->input->post('id_quote'),
                'price' => $this->input->post('price'),
                'currency' => $this->input->post('currency'),
                'payment' => $this->input->post('payment'),
                'availability' => $this->input->post('availability'),
                'notes' => $this->input->post('notes'),
                'name' => $result->product_name,
                'quantity' => $result->quantity,
                'pharmacopeia' => $result->pharmacopeia,
                'incoterm' => $result->incoterm,
                'port' => $result->port,
                'unit' => $result->unit,
                'notes_buyer' => $result->notes_buyer
            );
			$this->load->view('send_quote_error_view',$datos);
		}
	}

    public  function sendQuoteChecked(){
        $this->load->model('quotes_model');

        if($this->quotes_model->sendQuote_Form()){
            $email = $this->quotes_model->get_Email_Supplier($this->input->post('id_quote'));
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'support@apikontakt.com',
                'smtp_pass' => 'ApiKtkt8',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $product = $this->input->post('name');

            $this->email->from('support@apikontakt.com','API Kontakt');
            $this->email->to($email['email']);
            $this->email->subject($product.' Quote Request');

            $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
            $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
            $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

            $message .= '<p>Dear <strong>'.$email['first_name'].' '.$email['last_name'].'</strong>,';
            $message .= '<p>You have received a new quote via the API Kontakt QuoteManager.</p>';
            $message .= "<p> Please acces to <a href='".base_url()."'>www.apikontakt.com</a> to see the detailed information.</p>";
            $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
            $this->email->message($message);

            if($this->email->send()) {
                $this->load->view('quote_sent_supplier_view');
            }
            else {
                echo "<br />Email failed";
            }
        }
        else {
            echo "Problem sending quote";
        }
    }

	/*
	 * Esta funcion se encarga de desplegar la informacion de la cotizacion que fue enviada por el supplier
	 * */
    public function checkQuote($id) {
        $data = array(
            'id' => $id
        );
		$this->load->view('check_quote_view',$data);
	}

	public function profile() {
		$this->load->view('supplier_profile_view');
	}

	public function update_info() {
		$this->load->view('update_profile_supplier_view');
	}

	public function validate_update() {
        $this->form_validation->set_rules('companyname_txt','Company Name','trim|required');
        $this->form_validation->set_rules('companytype_txt','Company Type','trim|required');
        $this->form_validation->set_rules('country_txt','Country','trim|required');
        $this->form_validation->set_rules('city_txt','City','trim|required');
        $this->form_validation->set_rules('street_txt','Street Address','trim|required');
        $this->form_validation->set_rules('zip_txt','ZIP/Postal Code','trim|required');
        $this->form_validation->set_rules('countrycode_txt','Country Code','trim|required');
        $this->form_validation->set_rules('areacode_txt','Area Code','trim|required');
        $this->form_validation->set_rules('phone_txt','Phone Number','trim|required');
        $this->form_validation->set_rules('page_txt','Web Page','trim|required');
        $this->form_validation->set_rules('prefix_txt','Title','trim|required');
        $this->form_validation->set_rules('fname_txt','First Name','trim|required');
        $this->form_validation->set_rules('lname_txt','Last Name','trim|required');
        $this->form_validation->set_rules('position_txt','Job Position','trim|required');
        $this->form_validation->set_rules('email_txt','Email','trim|required|valid_email');
        $this->form_validation->set_rules('cemail_txt','Confirm Email','trim|required|valid_email|matches[email_txt]');
        $this->form_validation->set_rules('user_txt','User Name','trim|required');
        $this->form_validation->set_rules('password_txt','Password','trim|required');
        $this->form_validation->set_rules('cpassword_txt','Confirm Password','trim|required|matches[password_txt]');

		if ($this->form_validation->run()) {

			$this->load->model('modelousuarios');

			if($this->modelousuarios->updateSupplier()){
				$this->load->view('supplier_update_success_view');
			}
			else {
				echo "<br />Couldn't update data";
			}

		}
		else {
			$this->load->view('update_profile_supplier_view');
		}
	}
	
}