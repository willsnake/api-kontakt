<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('main_view');
	}
	
	public function email() {
		$this->load->view('email_success_view');
	}

	public function login() {
		$this->load->view('login_view');
	}
	
	public function register() {
		$this->load->view('register_view');
	}
	
	public function register_buyer() {
        $word = array_merge(range('A', 'Z'), range('0', '9'));
        shuffle($word);
        $word= substr(implode($word), 0, 8);

        $vals = array(
            'word'	 => $word,
            'time'	=>  900000000,
            'img_path' => './captcha/',
            'img_url' => base_url().'captcha/',
            'font_path'	 => './fonts/calibri.ttf',
            'img_width'	 => '150',
            'img_height' => '50',
            'expiration' => '300',
        );

        $captcha = create_captcha($vals);

        $datos = array(
            'captcha_time'	=> $captcha['time'],
            'ip_address'	=> $this->input->ip_address(),
            'word'	 => $captcha['word']
        );

        $query = $this->db->insert_string('captcha', $datos);
        $this->db->query($query);

        $data['image'] = $captcha['image'];

		$this->load->view('register_buyer_view', $data);
	}
	
	public function register_supplier() {
        $word = array_merge(range('A', 'Z'), range('0', '9'));
        shuffle($word);
        $word= substr(implode($word), 0, 8);

        $vals = array(
            'word'	 => $word,
            'time'	=>  900000000,
            'img_path' => './captcha/',
            'img_url' => base_url().'captcha/',
            'font_path'	 => './fonts/calibri.ttf',
            'img_width'	 => '150',
            'img_height' => '50',
            'expiration' => '300',
        );

        $captcha = create_captcha($vals);

        $datos = array(
            'captcha_time'	=> $captcha['time'],
            'ip_address'	=> $this->input->ip_address(),
            'word'	 => $captcha['word']
        );

        $query = $this->db->insert_string('captcha', $datos);
        $this->db->query($query);

        $data['image'] = $captcha['image'];

		$this->load->view('register_suplier_view', $data);
	}

	public function validar_login() {

		$this->form_validation->set_rules('user','User','trim|required|xss_clean|callback_verificarCuenta');
		$this->form_validation->set_rules('password','Password','trim|required|xss_clean');

		if ($this->form_validation->run()) {
			$usuario = $this->main_model->getUsuario();
			$datos = array(
				'tipo_usuario' => $usuario->tipo,
				'usuario' => $usuario->usuario,
				'logueado' => 1
				);
			$this->session->set_userdata($datos);
			$cookies = $this->session->all_userdata();
			if ($cookies['tipo_usuario'] == 2) {
				redirect('main/dashboardSup');
			}
			elseif($cookies['tipo_usuario'] == 3) {
				redirect('main/dashboardBuy');
			}
		}
		else {
			$this->load->view('login_view');
		}
	}
	
	public function validar_registro_supplier() {
		$this->form_validation->set_rules('companyname_txt','Company Name','trim|required');
        $this->form_validation->set_rules('companytype_txt','Company Type','trim|required');
		$this->form_validation->set_rules('country_txt','Country','trim|required');
        $this->form_validation->set_rules('city_txt','City','trim|required');
        $this->form_validation->set_rules('street_txt','Street Address','trim|required');
        $this->form_validation->set_rules('zip_txt','ZIP/Postal Code','trim|required');
		$this->form_validation->set_rules('countrycode_txt','Country Code','trim|required');
		$this->form_validation->set_rules('areacode_txt','Area Code','trim|required');
		$this->form_validation->set_rules('phone_txt','Phone Number','trim|required');
		$this->form_validation->set_rules('page_txt','Web Page','trim|required');
		$this->form_validation->set_rules('prefix_txt','Title','trim|required');
		$this->form_validation->set_rules('fname_txt','First Name','trim|required');
		$this->form_validation->set_rules('lname_txt','Last Name','trim|required');
		$this->form_validation->set_rules('position_txt','Job Position','trim|required');
		$this->form_validation->set_rules('email_txt','Email','trim|required|valid_email|is_unique[DatosUsuario.email]');
		$this->form_validation->set_rules('cemail_txt','Confirm Email','trim|required|valid_email|matches[email_txt]');
		$this->form_validation->set_rules('user_txt','User Name','trim|required|callback_username_check');
		$this->form_validation->set_rules('password_txt','Password','trim|required');
		$this->form_validation->set_rules('cpassword_txt','Confirm Password','trim|required|matches[password_txt]');
		$this->form_validation->set_rules('captcha_txt','Captcha','trim|required|callback_captcha_check');

		$this->form_validation->set_message('is_unique', 'This email address already exists');
		
		if ($this->form_validation->run()) {

			$key = md5(uniqid());
			
			$this->load->model('modelousuarios');
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'support@apikontakt.com',
                'smtp_pass' => 'ApiKtkt8',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
			
			$this->email->from('support@apikontakt.com','API Kontakt');
			$this->email->to($this->input->post('email_txt'));
			$this->email->subject('Confirm your account');
            $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
            $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
            $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

            $message .= '<p>Dear <strong>'.$this->input->post('fname_txt').' '.$this->input->post('lname_txt').'</strong>,';
            $message .= "<p>Please <a href='".base_url('main/register_user/'.$key)."'>Click here</a> to confirm your account.</p>";
            $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
            $this->email->message($message);
			
			if($this->modelousuarios->add_temp_users($key)){
				if($this->email->send()) {
					$this->load->view('email_success_view');
				}
				else {
					echo "<br />Email failed";
				}
			}
			else {
				echo "<br />Problem adding user to temp database";
			}
			
		}
		else {
            $word = array_merge(range('A', 'Z'), range('0', '9'));
            shuffle($word);
            $word= substr(implode($word), 0, 8);

            $vals = array(
                'word'	 => $word,
                'time'	=>  900000000,
                'img_path' => './captcha/',
                'img_url' => base_url().'captcha/',
                'font_path'	 => './fonts/calibri.ttf',
                'img_width'	 => '150',
                'img_height' => '50',
                'expiration' => '300',
            );

            $captcha = create_captcha($vals);

            $datos = array(
                'captcha_time'	=> $captcha['time'],
                'ip_address'	=> $this->input->ip_address(),
                'word'	 => $captcha['word']
            );

            $query = $this->db->insert_string('captcha', $datos);
            $this->db->query($query);

			$data = array(
						'image' => $captcha['image']
					);

			$this->load->view('register_suplier_view', $data);
		}
	}
	
	public function validar_registro_buyer() {
        $this->form_validation->set_rules('companyname_txt','Company Name','trim|required');
        $this->form_validation->set_rules('country_txt','Country','trim|required');
        $this->form_validation->set_rules('city_txt','City','trim|required');
        $this->form_validation->set_rules('street_txt','Street Address','trim|required');
        $this->form_validation->set_rules('zip_txt','ZIP/Postal Code','trim|required');
        $this->form_validation->set_rules('countrycode_txt','Country Code','trim|required');
        $this->form_validation->set_rules('areacode_txt','Area Code','trim|required');
        $this->form_validation->set_rules('phone_txt','Phone Number','trim|required');
        $this->form_validation->set_rules('page_txt','Web Page','trim|required');
        $this->form_validation->set_rules('prefix_txt','Title','trim|required');
        $this->form_validation->set_rules('fname_txt','First Name','trim|required');
        $this->form_validation->set_rules('lname_txt','Last Name','trim|required');
        $this->form_validation->set_rules('position_txt','Job Position','trim|required');
        $this->form_validation->set_rules('email_txt','Email','trim|required|valid_email|is_unique[DatosUsuario.email]');
        $this->form_validation->set_rules('cemail_txt','Confirm Email','trim|required|valid_email|matches[email_txt]');
        $this->form_validation->set_rules('user_txt','User Name','trim|required|callback_username_check');
        $this->form_validation->set_rules('password_txt','Password','trim|required');
        $this->form_validation->set_rules('cpassword_txt','Confirm Password','trim|required|matches[password_txt]');
        $this->form_validation->set_rules('code_txt','Reference Code','trim');
        $this->form_validation->set_rules('captcha_txt','Captcha','trim|required|callback_captcha_check');

		$this->form_validation->set_message('is_unique', 'This email address already exists');

		if ($this->form_validation->run()) {
            $this->load->model('modelousuarios');
            $key = md5(uniqid());

            $data = array(
                'key' => $key,
            );

            if($this->modelousuarios->add_temp_buyer($key)){
                $this->payment($data);
            }
            else {
                echo "<br />Problem adding user to temp database";
            }
		}
		else {
            $word = array_merge(range('A', 'Z'), range('0', '9'));
            shuffle($word);
            $word= substr(implode($word), 0, 8);

			$vals = array(
                'word'	 => $word,
                'time'	=>  900000000,
                'img_path' => './captcha/',
                'img_url' => base_url().'captcha/',
                'font_path'	 => './fonts/calibri.ttf',
                'img_width'	 => '150',
                'img_height' => '50',
                'expiration' => '300',
            );

            $captcha = create_captcha($vals);

            $datos = array(
                'captcha_time'	=> $captcha['time'],
                'ip_address'	=> $this->input->ip_address(),
                'word'	 => $captcha['word'],
            );

            $query = $this->db->insert_string('captcha', $datos);
            $this->db->query($query);

            $data = array(
                'image' => $captcha['image']
            );
			$this->load->view('register_buyer_view', $data);
		}
	}

    public function captcha_check($str) {

        $expiration = time()-900000000;
        $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

        $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
        $binds = array($this->input->post('captcha_txt'), $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0) {
            $this->form_validation->set_message('captcha_check', "The captcha wasn't entered correctly");
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    public function username_check($str) {
        $sql = "SELECT user_name FROM Usuario WHERE user_name = ?";
        $query = $this->db->query($sql, array($str));
        if ($query->num_rows() != 0) {
            $this->form_validation->set_message('username_check', 'The username already exists');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
	
	public function register_user($key) {
		$this->load->model('modelousuarios');
		if($this->modelousuarios->is_key_valid($key)){
			if($newuser = $this->modelousuarios->add_user($key)) {
				$datos = array(
					'tipo_usuario' => $newuser['id_tipo_usuario'],
					'usuario' => $newuser['id_usuario'],
					'logueado' => 1 
					);
				$this->session->set_userdata($datos);
				$cookies = $this->session->all_userdata();
				if ($cookies['tipo_usuario'] == 2) {
					redirect('main/dashboardSup');
				}
				elseif($cookies['tipo_usuario'] == 3) {
					redirect('main/dashboardBuy');
				}
			}
			else {
				echo "Failed to add user";
			}
		}
		else {
            $this->load->view('error_key_view');
		}
	}

	public function dashboardSup() {
		$cookies = $this->session->all_userdata();
		if ($cookies['logueado'] == 1) {
			redirect('supplier');
		}
		else {
			redirect('main/restringido');
		}
	}

	public function dashboardBuy() {
		$cookies = $this->session->all_userdata();
		if ($cookies['logueado'] == 1) {
			redirect('buyer');
		}
		else {
			redirect('main/restringido');
		}
	}

	public function restringido() {
		$this->load->view('restringido_view');
	}

	public function verificarCuenta() {
		if ($this->main_model->loguear()) {
			return TRUE;
		}
		else {

			$this->form_validation->set_message('verificarCuenta','<div id="report-error" class="report-div error" style="display: block;"><p>Wrong username or password.</p></div>');
			return FALSE;
		}
	}
	
	public function forgotPassword() {
		$this->load->view('forgot_pass_view');
	}
	
	public function sendPassword() {
		
		if (isset($_POST['emailpass']) && !empty($_POST['emailpass'])) {
			$this->load->model('modelousuarios');
			
			$email = trim($this->input->post('emailpass'));
			$result = $this->modelousuarios->email_exists($email);
			
			if($result) {
						
				$key = md5(uniqid());

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'support@apikontakt.com',
                    'smtp_pass' => 'ApiKtkt8',
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8'
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('support@apikontakt.com','API Kontakt');
                $this->email->to($email);
                $this->email->subject('Reset your password');

                $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
                $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
                $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

                $message .= '<p>A request to reset your password has been sent to us.</p>';
                $message .= "<p><a href='{unwrap}".base_url('main/resetPassword/'.$this->input->post('emailpass')."/".$key)."{/unwrap}'>Click here</a> to reset your password.</p>";
                $message .= '<p>If you have received this email by mistake, please delete it.</p>';
                $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
                $this->email->message($message);
				
				if($this->modelousuarios->add_key_reset_password($email,$key)){
					if($this->email->send()) {
						$this->load->view('email_resetPass_success_view');
					}
					else {
						echo "<br />Email failed";
					}
				}
				else {
					echo "<br />Error inserting key";
				}
			}
			else {
                $this->load->view('email_not_exist_view');
			}
			
		}
		else {
			redirect(base_url());
		}
	}
	
	public function resetPassword($email, $key) {
		if (strpos($email,'%40') == true) {
			$email = str_replace("%40", "@", $email);
		}
		
		if(isset($email, $key)) {
			$this->load->model('modelousuarios');
			
			$email = trim($email);
			$email_hash = sha1($email.$key);
			
			$verificado = $this->modelousuarios->verify_reset_password($email,$key);
			
			if($verificado) {
				$this->load->view('reset_pass_form_view', array('email'=>$email,'key'=>$key,'email_hash'=>$email_hash));
			}
			else  {
                echo("<br />");
                echo "Error verifying that email or key";
			}
		}
		else {
			echo "Error";
		}
	}
	
	public function updatePassword() {
		if(!isset($_POST['email'],$_POST['email_hash']) || $_POST['email_hash'] != sha1($_POST['email'].$_POST['key'])) {
			die('Error updating your password');
		}
		else {
			$this->form_validation->set_rules('email_hash','Email Hash','trim|required|xss_clean');
			$this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('password','Password','trim|required|xss_clean');
			$this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|xss_clean');
			
			if($this->form_validation->run() == FALSE) {
				$this->load->view('reset_pass_form_view');
			}
			else {
				$this->load->model('modelousuarios');
				$result = $this->modelousuarios->updatePassword();
				
				if($result) {
					redirect("main/login");
					// echo "Password updated";
				}
				else {
					echo "Error updating password";
				}
			}
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

    public function payment($data) {
        $this->load->view('payment_view', $data);
    }

    public function payment_paypal() {
        $key = $this->input->post("key");
        $this->load->library('merchant');
        $this->merchant->load('paypal_express');
        $settings = $this->merchant->default_settings();
        $settings = array(
            'username' => 'invoice_api1.apikontakt.com',
            'password' => '4JK5NQHUYKKK4FGJ',
            'signature' => 'AiPC9BjkCyDFQXbSkoZcgqH3hpacA4v7Zfg4CIT8Dxyp2ub8dctOBR5U',
        );

        $this->merchant->initialize($settings);

        $params =array(
            'amount' => 1998.00,
            'currency' => 'USD',
            'return_url' => base_url('main/payment_received/'.$key),
            'cancel_url' => base_url('main/cancel_payment/'.$key),
            'description' => 'API Kontakt Annual Membership',
        );

        $response = $this->merchant->purchase($params);
    }

    public function cancel_payment($key) {
        $this->load->model('modelousuarios');
        $this->modelousuarios->deleteTempUser($key);
        $this->load->view('cancel_payment_view');
    }

    public function payment_received($key) {
        $this->load->model('modelousuarios');

        $this->load->library('merchant');
        $this->merchant->load('paypal_express');

        $settings = array(
            'username' => 'invoice_api1.apikontakt.com',
            'password' => '4JK5NQHUYKKK4FGJ',
            'signature' => 'AiPC9BjkCyDFQXbSkoZcgqH3hpacA4v7Zfg4CIT8Dxyp2ub8dctOBR5U',
        );

        $this->merchant->initialize($settings);

        $params =array(
            'amount' => 1998.00,
            'currency' => 'USD',
            'cancel_url' => base_url('main/cancel_payment'),
            'description' => 'API Kontakt Annual Membership',
        );

        $response = $this->merchant->purchase_return($params);

        if ($response->success()) {

            $user = $this->modelousuarios->send_mail_confirmation($key);

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'support@apikontakt.com',
                'smtp_pass' => 'ApiKtkt8',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $this->email->from('support@apikontakt.com','API Kontakt');
            $this->email->to($user->email);
            $this->email->subject('Confirm your account');

            $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
            $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
            $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

            $message .= '<p>Dear <strong>'.$user->first_name.' '.$user->last_name.'</strong>,';
            $message .= "<p>Please <a href='".base_url('main/register_user/'.$key)."'>Click here</a> to confirm your account.</p>";
            $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
            $this->email->message($message);

            if($this->email->send()) {
                $this->load->view('email_success_view');
            }
            else {
                echo "<br />Email failed";
            }
        }
        else {
            $this->modelousuarios->deleteTempUser($key);
            $message = $response->message();
            $datos = array(
                'mensaje' => $message,
            );
            $this->load->view('unsuccessful_transaction_view', $datos);
        }
    }

    public function result() {
        require('./libs/http_client.php');
        $this->load->model('modelousuarios');

        $this->form_validation->set_rules('fname_txt','First Name','trim|required');
        $this->form_validation->set_rules('lname_txt','Last Name','trim|required');
        $this->form_validation->set_rules('company_txt','Company Name','trim|required');
        $this->form_validation->set_rules('email_txt','Email','trim|required');
        $this->form_validation->set_rules('address_txt','Address','trim|required');
        $this->form_validation->set_rules('country_txt','Country','trim|required');
        $this->form_validation->set_rules('city_txt','City','trim|required');
        $this->form_validation->set_rules('state_txt','State','trim|required');
        $this->form_validation->set_rules('zip_txt','ZIP','trim|required');
        $this->form_validation->set_rules('telephone_txt','Telephone','trim|required');
        $this->form_validation->set_rules('tax_txt','Tax / VAT Number','trim|required');
        $this->form_validation->set_rules('card_number_txt','Card Number','trim|required');
        $this->form_validation->set_rules('month_txt','Month','trim|required');
        $this->form_validation->set_rules('year_txt','Year','trim|required');
        $this->form_validation->set_rules('cvv_txt','CVV','trim|required');


        if ($this->form_validation->run()) {

            $from   = 'USD';
            $to     = 'MXN';
            $url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $from . $to .'=X';
            $handle = @fopen($url, 'r');

            if ($handle) {
                $result = fgets($handle, 4096);
                fclose($handle);
            }
            $allData = explode(',',$result);
            $dollarValue = $allData[1];

//            $total = number_format($dollarValue * 1998,2,'.', '');
            $total = number_format($dollarValue,2,'.', '');

            $key = $this->input->post("key");
            $card = trim($this->input->post("card_number_txt"));
            $month = trim($this->input->post("month_txt"));
            $year = trim($this->input->post("year_txt"));
            $expires = $month."/".$year;
            $cvv = trim($this->input->post("cvv_txt"));

            $http = new httpClient();
            $http->Connect("eps.banorte.com", 443) or die("Connect problem");

            $params = array(
                'Name'=>'7580364',
                'Password'=>'api364p',
                'ClientId'=>'75730',
                'Mode'=>'P',
                'TransType'=>'Auth',
                'Number'=> $card,
                'Expires'=>$expires,
                'Cvv2Indicator'=>1,
                'Cvv2Val'=>$cvv,
                'Total'=> $total
            );

            $status = $http->Post("/recibo", $params);
            $errorcode = $http->getHeader("CcErrCode");

            if ($status == 200 && $errorcode == 50) {
                $this->modelousuarios->deleteTempUser($key);
                $datos = array(
                    'mensaje' => "Your credit card was declined, please contact your bank or card issuer to find out the specific reason.",
                );
                $this->load->view('unsuccessful_transaction_view', $datos);
            }
            elseif($status != 200) {
                $this->modelousuarios->deleteTempUser($key);
                $datos = array(
                    'mensaje' => "The transaction was unsuccessful, please try again.",
                );
                $this->load->view('unsuccessful_transaction_view', $datos);
            }
            elseif($status == 200 && $errorcode == 1) {
                $this->modelousuarios->add_billing_info($key);
                $user = $this->modelousuarios->send_mail_confirmation($key);

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'support@apikontakt.com',
                    'smtp_pass' => 'ApiKtkt8',
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8'
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('support@apikontakt.com','API Kontakt');
                $this->email->to($user->email);
                $this->email->subject('Confirm your account');

                $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
                $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
                $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

                $message .= '<p>Dear <strong>'.$user->first_name.' '.$user->last_name.'</strong>,';
                $message .= "<p>Please <a href='".base_url('main/register_user/'.$key)."'>Click here</a> to confirm your account.</p>";
                $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
                $this->email->message($message);

                if($this->email->send()) {
                    $this->load->view('email_success_view');
                }
                else {
                    echo "<br />Email failed";
                }
            }
            elseif($status == 200 && $errorcode != 1) {
                $this->modelousuarios->deleteTempUser($key);
                $datos = array(
                    'mensaje' => "The transaction was unsuccessful, please try again.",
                );
                $this->load->view('unsuccessful_transaction_view', $datos);
            }
            $http->Disconnect();
        }
        else {
            $data["key"] = $this->input->post("key");
            $this->load->view('payment_view', $data);
        }

    }

    public  function test() {
        $from   = 'USD';
        $to     = 'MXN';
        $url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $from . $to .'=X';
        $handle = @fopen($url, 'r');

        if ($handle) {
            $result = fgets($handle, 4096);
            fclose($handle);
        }
        $allData = explode(',',$result);
        $dollarValue = $allData[1];

        $total = number_format($dollarValue * 1998,2,'.', '');

        echo '$1,998 dolares a pesos mexicanos es: '.$total;
    }
}