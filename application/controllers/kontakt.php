<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontakt extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('tipo_usuario') == 2 && $this->session->userdata('logueado') == 1) {
            $this->load->view('kontaktus_supplier_view');
        }
        elseif($this->session->userdata('tipo_usuario') == 3 && $this->session->userdata('logueado') == 1){
            $this->load->view('kontaktus_buyer_view');
        }
        else {
            $this->load->view('kontaktus_main_view');
        }
    }

}