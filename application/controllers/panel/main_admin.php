<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/main_admin_model', 'main_admin_model');
    }

    public function index() {
        $this->login();
    }

    public function login() {
        $this->load->view('panel/admin_login_view');
    }

    public function validar_login() {

        $this->form_validation->set_rules('user_txt','User','trim|required|xss_clean|callback_verificarCuenta');
        $this->form_validation->set_rules('pass_txt','Password','trim|required|xss_clean');

        if ($this->form_validation->run()) {
            $usuario = $this->main_admin_model->getUsuario();
            $datos = array(
                'tipo_usuario' => $usuario->tipo,
                'usuario' => $usuario->usuario,
                'logueado' => 1
            );
            $this->session->set_userdata($datos);

            redirect('panel/main_admin/panelHome');
        }
        else {
            $this->load->view('panel/admin_login_view');
        }

    }

    public function verificarCuenta() {
        if ($this->main_admin_model->loguear()) {
            return TRUE;
        }
        else {

            return FALSE;
        }
    }

    public function panelHome() {
        $this->revisarSesion();

        $this->load->view('panel/panelHome_view');
    }

    public function suppliersOutput($output = null) {
        $this->load->view('panel/panelSuppliers_view.php',$output);
    }

    public function buyersOutput($output = null) {
        $this->load->view('panel/panelBuyers_view.php',$output);
    }

    public function productsOutput($output = null) {
        $this->load->view('panel/panelProducts_view.php',$output);
    }

    public function tempOutput($output = null) {
        $this->load->view('panel/panelTemp_view.php',$output);
    }

    public function productsCompanyOutput($output = null) {
        $this->load->view('panel/productsCompany_view.php',$output);
    }

    public function productsCompanyNo($output = null) {
        $this->load->view('panel/productsCompanyNoProducts_view.php',$output);
    }

    public function productsTypeOutput($output = null) {
        $this->load->view('panel/productsType_view.php',$output);
    }

    public function panelBuyers() {

        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme("flexigrid-original");
        $crud->set_model('admin/buyers_model');
        $crud->set_table('datosusuario');
        $crud->columns('company_name','country','user_name','prefix','first_name','last_name');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $crud->set_subject('Buyers');

        $output = $crud->render();

        $this->buyersOutput($output);

    }

    public function panelSuppliers() {

        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme("flexigrid-original");
        $crud->set_model('admin/suppliers_model');
        $crud->set_table('datosusuario');
        $crud->columns('company_name','country','user_name','prefix','first_name','last_name');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $crud->set_subject('Suppliers');

        $output = $crud->render();

        $this->suppliersOutput($output);

    }

    public function panelProducts() {

        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme("flexigrid-original");
        $crud->set_model('admin/products_model');
        $crud->set_table('productos');
        $crud->columns('company_name','product_name','specification','gmp');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $crud->set_subject('Products');

        $output = $crud->render();

        $this->productsOutput($output);

    }

    public function panelTemp() {
        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme('admin-check-flexigrid');
        $crud->set_table('tempdatosusuario')
            ->set_subject('Unregistered Users')
            ->columns('company_name','first_name', 'country_code', 'email', 'id_tipo_usuario')
            ->display_as('company_name','Company Name')
            ->display_as('first_name','Name')
            ->display_as('email','Email')
            ->display_as('id_tipo_usuario','User Type');

        $crud->callback_column('first_name',array($this,'concatenarNombre'));
        $crud->callback_column('country_code',array($this,'concatenarNumero'));
        $crud->callback_column('id_tipo_usuario',array($this,'reemplazarTipoUsuario'));

        $crud->add_action('Send Email', base_url('img/Mail-icon.png'), 'panel/main_admin/send_email');

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $output = $crud->render();

        $this->tempOutput($output);
    }

    public function concatenarNumero($value, $row) {
        return "+".$row->country_code." (".$row->area_code.") ".$row->phone;
    }

    public function concatenarNombre($value, $row) {
        return $row->first_name." ".$row->last_name;
    }

    public function reemplazarTipoUsuario($value, $row) {
        if($value == "2") {
            return "Supplier";
        }
        elseif($value == "3") {
            return "Buyer";
        }
    }

    public function products_company_name() {
        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme("flexigrid-original");
        $crud->set_model('admin/products_company_model');
        $crud->set_table('productos')
            ->set_subject('Products')
            ->columns('conteo','company_name')
            ->display_as('conteo','Total')
            ->display_as('company_name','Company Name');

        /* $crud->callback_column('first_name',array($this,'concatenarNombre'));
        $crud->callback_column('country_code',array($this,'concatenarNumero'));
        $crud->callback_column('id_tipo_usuario',array($this,'reemplazarTipoUsuario'));

        $crud->add_action('Send Email', base_url('img/Mail-icon.png'), 'panel/main_admin/send_email'); */

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $output = $crud->render();

        $this->productsCompanyOutput($output);
    }

    public function products_company_no_products() {
        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme("flexigrid-original");
        $crud->set_model('admin/No_products_company_model');
        $crud->set_table('productos')
            ->set_subject('Companies without products')
            ->columns('company_name','email')
            ->display_as('company_name','Company Name')
            ->display_as('email','Email');

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $output = $crud->render();

        $this->productsCompanyNo($output);
    }

    public function productsType() {
        $this->revisarSesion();

        $crud = new grocery_CRUD();
        $crud->set_theme("flexigrid-original");
        $crud->set_model('admin/type_products_model');
        $crud->set_table('productos')
            ->set_subject('Type of products')
            ->columns('conteo','type')
            ->display_as('conteo','Total')
            ->display_as('type','Type');

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $output = $crud->render();

        $this->productsTypeOutput($output);
    }

    public function send_email($id) {
        $resultado = array();
        $data = $this->main_admin_model->bulkEmail($id);

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'support@apikontakt.com',
            'smtp_pass' => 'ApiKtkt8',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('support@apikontakt.com','API Kontakt');
        $this->email->to($data->email);
        $this->email->subject('Confirm your account');

        $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
        $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
        $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

        $message .= '<p>Dear <strong>'.$data->first_name.' '.$data->last_name.'</strong>,';
        $message .= "<p>You haven't confirmed your email address.</p>";
        $message .= "<p>Without this step, you can't access the most effective global pharma network.</p>";
        $message .= "Please <a href='".base_url('main/register_user/'.$data->key)."'>click here</a> to complete your registration.</p>";
        $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
        $this->email->message($message);


        if($this->email->send()) {
            $resultado['resultado'] .= '<div class="alert alert-success alert-block"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">Email Sent!</h4>The email to '.$data->email.' has been sent successfully!</div>';
        }
        else {
            $resultado['resultado'] .= '<div class="alert alert-error alert-block"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">Error!</h4>There was an error, sending the email to '.$data->email.' please contact the administrator or developer.</div>';
        }

        $this->load->view('panel/emailSent_view', $resultado);
    }

    public function sendBulkEmail($data) {
        $resultado = array();
        $id_array = explode("%7C", $data);
        foreach($id_array as $item){
            if($item != 'on' && $item != '') {
                $data = $this->main_admin_model->bulkEmail($item);

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'support@apikontakt.com',
                    'smtp_pass' => 'ApiKtkt8',
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8'
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('support@apikontakt.com','API Kontakt');
                $this->email->to($data->email);
                $this->email->subject('Confirm your account');

                $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
                $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
                $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

                $message .= '<p>Dear <strong>'.$data->first_name.' '.$data->last_name.'</strong>,';
                $message .= "<p>You haven't confirmed your email address.</p>";
                $message .= "<p>Without this step, you can't access the most effective global pharma network.</p>";
                $message .= "Please <a href='".base_url('main/register_user/'.$data->key)."'>click here</a> to complete your registration.</p>";
                $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
                $this->email->message($message);


                if($this->email->send()) {
                    $resultado['resultado'] .= '<div class="alert alert-success alert-block"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">Email Sent!</h4>The email to '.$data->email.' has been sent successfully!</div>';
                }
                else {
                    $resultado['resultado'] .= '<div class="alert alert-error alert-block"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">Error!</h4>There was an error, sending the email to '.$data->email.' please contact the administrator or developer.</div>';
                }
            }
        }
        $this->load->view('panel/emailSent_view', $resultado);
    }

    public function logout() {
        redirect("main/logout");
    }

    public function revisarSesion() {
        if (!$this->session->userdata('logueado')) {
            redirect('main/restringido');
        }
        elseif($this->session->userdata('tipo_usuario') == 2 && $this->session->userdata('logueado') == 1){
            redirect('main/restringido');
        }
        elseif($this->session->userdata('tipo_usuario') == 3 && $this->session->userdata('logueado') == 1){
            redirect('main/restringido');
        }
    }

    public function add_Buyer() {
        $key = md5(uniqid());

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'support@apikontakt.com',
            'smtp_pass' => 'ApiKtkt8',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('support@apikontakt.com','API Kontakt');
        $this->email->to("support@apikontakt.com");
        $this->email->subject('Confirm your account');
        $message = '<html><head><style type="text/css">body { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; }</style><meta charset="utf8"></head><body><div style="display: block;"><div style="display: block; width: 60%;text-align: center;margin:0 auto;">';
        $message .= '<img src="'.base_url('img/Apikontakt_inicio.png').'" style="text-align:center;width: 100%;">';
        $message .= '<div style="display: block;text-align: justify;margin:0 auto;">';

        $message .= '<p>Dear <strong> Test User </strong>,';
        $message .= "<p>Please <a href='".base_url('main/register_user/'.$key)."'>Click here</a> to confirm your account.</p>";
        $message .= "<p>Username: userTest  Password: test</p>";
        $message .= '<p>Best regards</p><br /><p>API Kontakt Team</p></div></div></div></body></html>';
        $this->email->message($message);

        if($this->main_admin_model->admin_add_temp_buyer($key)){
            if($this->email->send()) {
                echo("<br />Email sent!");
            }
            else {
                echo "<br />Email failed";
            }
        }
        else {
            echo "<br />Problem adding user to temp database";
        }
    }

    public function delete_user() {
        /* Este es id del usuario que va a ser borrado */
        $user = "1051";

        $this->db->delete('productos',  array('id_supplier' => $user));
        $this->db->delete('quotesbuyer',  array('id_supplier' => $user));
        $this->db->delete('quotessupplier',  array('id_supplier' => $user));
        $this->db->delete('quotesbuyer', array('id_buyer' => $user));
        $this->db->delete('quotessupplier', array('id_buyer' => $user));
        $this->db->delete('datosusuario',  array('id_datos_usuario' => $user));
        $this->db->delete('usuario',  array('id_datos_usuario' => $user));
        echo("User deleted");
    }
}