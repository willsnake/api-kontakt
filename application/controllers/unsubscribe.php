<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unsubscribe extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('tipo_usuario') == 2 && $this->session->userdata('logueado') == 1) {
            $this->load->view('unsubscribe_supplier_view');
        }
        elseif($this->session->userdata('tipo_usuario') == 3 && $this->session->userdata('logueado') == 1){
            $this->load->view('unsubscribe_buyer_view');
        }
    }

    public function confirmation() {
        if ($this->session->userdata('tipo_usuario') == 2 && $this->session->userdata('logueado') == 1) {
            $this->load->view('unsubscribe_confirmation_supplier_view');
        }
        elseif($this->session->userdata('tipo_usuario') == 3 && $this->session->userdata('logueado') == 1){
            $this->load->view('unsubscribe_confirmation_buyer_view');
        }
    }

    public function good_bye() {
        $this->load->model('modelousuarios');
        $this->modelousuarios->delete_user();
        $this->session->sess_destroy();
        $this->load->view('unsubscribe_view');
    }
}