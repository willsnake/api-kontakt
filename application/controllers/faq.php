<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('tipo_usuario') == 2 && $this->session->userdata('logueado')  == 1) {
            $this->load->view('faq_supplier_view');
        }
        elseif($this->session->userdata('tipo_usuario') == 3 && $this->session->userdata('logueado') == 1){
            $this->load->view('faq_buyer_view');
        }
        else {
            $this->load->view('faq_view');
        }
    }

}