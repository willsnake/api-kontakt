<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?= $company_name ?> Info</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
	<link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
				<div class="nav-collapse collapse">
					<li class="brand" href="#"></li>
					<ul class="nav">
						<li class="home"><a href=<?= base_url('supplier') ?>>Home</a></li>
					</ul>
				</div>
				
				<form class="navbar-form pull-right">
					<ul class="nav">
						<li><a onclick="history.go(-1);">Go Back</a></li>
						<li><a href=<?= base_url('supplier/profile/'.$row->company_name) ?>>Profile</a></li>
						<li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
					</ul>
				</form>
			</div>
		</div>
    </div>
	
	<div class="container">
		<div class="logo-register">
			<a href=<?= base_url('supplier') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
		</div>
		
		<div class="row">
			<h2 class="titulo"><?= $company_name ?>'s Info</h2>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td style="text-align:center;">Company Name:</td>
						<td style="text-align:center;"><?php echo $company_name ?></td>
					</tr>
					<tr>
						<td style="text-align:center;">Country:</td>
						<td style="text-align:center;"><?php echo $country ?></td>
					</tr>
					<tr>
						<td style="text-align:center;">Web page:</td>
						<td style="text-align:center;"><?php echo $web_page ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<br />
	<br />
	<br />
	
	<hr>
	
	<footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>
	
</body>
</html>