<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Follow Up</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>

    <div class="container">

      <div class="masthead">
        <ul class="nav nav-pills pull-right">
          <li><a href=<?= base_url('supplier') ?>>My Account</a></li>
          <li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
        </ul>
        <img src=<?= base_url('img/ApiLogo.jpg') ?> width="200">
      </div>

      <hr>

      <div class="row" style="text-align:center;">
		<div class="span4">
			<h1>Follow up</h1>
		</div>
      </div>

      <hr>

      <footer>
        <p>&copy; Company 2013</p>
      </footer>

    </div>
    <script src=<?= base_url('js/jquery.min.js') ?>></script>

  </body>
</html>