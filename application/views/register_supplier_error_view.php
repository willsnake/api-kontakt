<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Register Supplier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/flexigrid.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url() ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a href=<?= base_url('main/login') ?>>Login</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url() ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>

    <div class="row">
        <?= form_open("main/validar_registro_supplier",$atributos = array('class' => 'form-horizontal' ), $atributos = array('tipo_usuario_txt' => 2 ))?>
        <h1 class="titulo">Register Supplier</h1>
        <?php
        $companyname = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'companyname_txt',
            'placeholder' => 'Company Name',
            'title' => 'Company Name',
            'value' => $companyn,
        );

        $companytype = array(
            "Manufacturer" => "Manufacturer",
            "Trader" => "Trader",
        );

        $street = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'street_txt',
            'placeholder' => 'Address',
            'title' => 'Address',
            'value' => $street,
        );

        $city = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'city_txt',
            'placeholder' => 'City',
            'title' => 'City',
            'value' => $city,
        );

        $zip = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'zip_txt',
            'placeholder' => 'ZIP Code',
            'title' => 'ZIP Code',
            'value' => $zip,
        );

        $options = array(
            "Afghanistan"	=>	"Afghanistan",
            "Albania"	=>	"Albania",
            "Algeria"	=>	"Algeria",
            "American Samoa"	=>	"American Samoa",
            "Andorra"	=>	"Andorra",
            "Angola"	=>	"Angola",
            "Anguilla"	=>	"Anguilla",
            "Antarctica"	=>	"Antarctica",
            "Antigua and Barbuda"	=>	"Antigua and Barbuda",
            "Argentina"	=>	"Argentina",
            "Armenia"	=>	"Armenia",
            "Aruba"	=>	"Aruba",
            "Australia"	=>	"Australia",
            "Austria"	=>	"Austria",
            "Azerbaijan"	=>	"Azerbaijan",
            "Bahamas"	=>	"Bahamas",
            "Bahrain"	=>	"Bahrain",
            "Bangladesh"	=>	"Bangladesh",
            "Barbados"	=>	"Barbados",
            "Belarus"	=>	"Belarus",
            "Belgium"	=>	"Belgium",
            "Belize"	=>	"Belize",
            "Benin"	=>	"Benin",
            "Bermuda"	=>	"Bermuda",
            "Bhutan"	=>	"Bhutan",
            "Bolivia"	=>	"Bolivia",
            "Bosnia and Herzegovina"	=>	"Bosnia and Herzegovina",
            "Botswana"	=>	"Botswana",
            "Brazil"	=>	"Brazil",
            "Brunei Darussalam"	=>	"Brunei Darussalam",
            "Bulgaria"	=>	"Bulgaria",
            "Burkina Faso"	=>	"Burkina Faso",
            "Burundi"	=>	"Burundi",
            "Cambodia"	=>	"Cambodia",
            "Cameroon"	=>	"Cameroon",
            "Canada"	=>	"Canada",
            "Cape Verde"	=>	"Cape Verde",
            "Cayman Islands"	=>	"Cayman Islands",
            "Central African Republic"	=>	"Central African Republic",
            "Chad"	=>	"Chad",
            "Chile"	=>	"Chile",
            "China"	=>	"China",
            "Christmas Island"	=>	"Christmas Island",
            "Cocos (Keeling) Islands"	=>	"Cocos (Keeling) Islands",
            "Colombia"	=>	"Colombia",
            "Comoros"	=>	"Comoros",
            "Democratic Republic of the Congo (Kinshasa)"	=>	"Democratic Republic of the Congo (Kinshasa)",
            "Congo, Republic of (Brazzaville)"	=>	"Congo, Republic of (Brazzaville)",
            "Cook Islands"	=>	"Cook Islands",
            "Costa Rica"	=>	"Costa Rica",
            "Ivory Coast (Cote d'Ivoire)"	=>	"Ivory Coast (Cote d'Ivoire)",
            "Croatia"	=>	"Croatia",
            "Cuba"	=>	"Cuba",
            "Cyprus"	=>	"Cyprus",
            "Czech Republic"	=>	"Czech Republic",
            "Denmark"	=>	"Denmark",
            "Djibouti"	=>	"Djibouti",
            "Dominica"	=>	"Dominica",
            "Dominican Republic"	=>	"Dominican Republic",
            "East Timor Timor-Leste"	=>	"East Timor Timor-Leste",
            "Ecuador"	=>	"Ecuador",
            "Egypt"	=>	"Egypt",
            "El Salvador"	=>	"El Salvador",
            "Equatorial Guinea"	=>	"Equatorial Guinea",
            "Eritrea"	=>	"Eritrea",
            "Estonia"	=>	"Estonia",
            "Ethiopia"	=>	"Ethiopia",
            "Falkland Islands"	=>	"Falkland Islands",
            "Faroe Islands"	=>	"Faroe Islands",
            "Fiji"	=>	"Fiji",
            "Finland"	=>	"Finland",
            "France"	=>	"France",
            "French Guiana"	=>	"French Guiana",
            "French Polynesia"	=>	"French Polynesia",
            "French Southern Territories"	=>	"French Southern Territories",
            "Gabon"	=>	"Gabon",
            "Gambia"	=>	"Gambia",
            "Georgia"	=>	"Georgia",
            "Germany"	=>	"Germany",
            "Ghana"	=>	"Ghana",
            "Gibraltar"	=>	"Gibraltar",
            "Great Britain"	=>	"Great Britain",
            "Greece"	=>	"Greece",
            "Greenland"	=>	"Greenland",
            "Grenada"	=>	"Grenada",
            "Guadeloupe"	=>	"Guadeloupe",
            "Guam"	=>	"Guam",
            "Guatemala"	=>	"Guatemala",
            "Guinea"	=>	"Guinea",
            "Guinea-Bissau"	=>	"Guinea-Bissau",
            "Guyana"	=>	"Guyana",
            "Haiti"	=>	"Haiti",
            "Holy See"	=>	"Holy See",
            "Honduras"	=>	"Honduras",
            "Hong Kong"	=>	"Hong Kong",
            "Hungary"	=>	"Hungary",
            "Iceland"	=>	"Iceland",
            "India"	=>	"India",
            "Indonesia"	=>	"Indonesia",
            "Iran (Islamic Republic of)"	=>	"Iran (Islamic Republic of)",
            "Iraq"	=>	"Iraq",
            "Ireland"	=>	"Ireland",
            "Israel"	=>	"Israel",
            "Italy"	=>	"Italy",
            "Jamaica"	=>	"Jamaica",
            "Japan"	=>	"Japan",
            "Jordan"	=>	"Jordan",
            "Kazakhstan"	=>	"Kazakhstan",
            "Kenya"	=>	"Kenya",
            "Kiribati"	=>	"Kiribati",
            "Korea, Democratic People's Rep. (North Korea)"	=>	"Korea, Democratic People's Rep. (North Korea)",
            "Korea, Republic of (South Korea)"	=>	"Korea, Republic of (South Korea)",
            "Kosovo"	=>	"Kosovo",
            "Kuwait"	=>	"Kuwait",
            "Kyrgyzstan"	=>	"Kyrgyzstan",
            "Lao, People's Democratic Republic"	=>	"Lao, People's Democratic Republic",
            "Latvia"	=>	"Latvia",
            "Lebanon"	=>	"Lebanon",
            "Lesotho"	=>	"Lesotho",
            "Liberia"	=>	"Liberia",
            "Libya"	=>	"Libya",
            "Liechtenstein"	=>	"Liechtenstein",
            "Lithuania"	=>	"Lithuania",
            "Luxembourg"	=>	"Luxembourg",
            "Macao"	=>	"Macao",
            "Macedonia, Rep. Of"	=>	"Macedonia, Rep. Of",
            "Madagascar"	=>	"Madagascar",
            "Malawi"	=>	"Malawi",
            "Malaysia"	=>	"Malaysia",
            "Maldives"	=>	"Maldives",
            "Mali"	=>	"Mali",
            "Malta"	=>	"Malta",
            "Marshall Islands"	=>	"Marshall Islands",
            "Martinique"	=>	"Martinique",
            "Mauritania"	=>	"Mauritania",
            "Mauritius"	=>	"Mauritius",
            "Mayotte"	=>	"Mayotte",
            "Mexico"	=>	"Mexico",
            "Micronesia, Federal States of"	=>	"Micronesia, Federal States of",
            "Moldova, Republic of"	=>	"Moldova, Republic of",
            "Monaco"	=>	"Monaco",
            "Mongolia"	=>	"Mongolia",
            "Montenegro"	=>	"Montenegro",
            "Montserrat"	=>	"Montserrat",
            "Morocco"	=>	"Morocco",
            "Mozambique"	=>	"Mozambique",
            "Myanmar, Burma"	=>	"Myanmar, Burma",
            "Namibia"	=>	"Namibia",
            "Nauru"	=>	"Nauru",
            "Nepal"	=>	"Nepal",
            "Netherlands"	=>	"Netherlands",
            "Netherlands Antilles"	=>	"Netherlands Antilles",
            "New Caledonia"	=>	"New Caledonia",
            "New Zealand"	=>	"New Zealand",
            "Nicaragua"	=>	"Nicaragua",
            "Niger"	=>	"Niger",
            "Nigeria"	=>	"Nigeria",
            "Niue"	=>	"Niue",
            "Northern Mariana Islands"	=>	"Northern Mariana Islands",
            "Norway"	=>	"Norway",
            "Oman"	=>	"Oman",
            "Pakistan"	=>	"Pakistan",
            "Palau"	=>	"Palau",
            "Palestinian territories"	=>	"Palestinian territories",
            "Panama"	=>	"Panama",
            "Papua New Guinea"	=>	"Papua New Guinea",
            "Paraguay"	=>	"Paraguay",
            "Peru"	=>	"Peru",
            "Philippines"	=>	"Philippines",
            "Pitcairn Island"	=>	"Pitcairn Island",
            "Poland"	=>	"Poland",
            "Portugal"	=>	"Portugal",
            "Puerto Rico"	=>	"Puerto Rico",
            "Qatar"	=>	"Qatar",
            "Reunion Island"	=>	"Reunion Island",
            "Romania"	=>	"Romania",
            "Russian Federation"	=>	"Russian Federation",
            "Rwanda"	=>	"Rwanda",
            "Saint Kitts and Nevis"	=>	"Saint Kitts and Nevis",
            "Saint Lucia"	=>	"Saint Lucia",
            "Saint Vincent and the Grenadines"	=>	"Saint Vincent and the Grenadines",
            "Samoa"	=>	"Samoa",
            "San Marino"	=>	"San Marino",
            "Sao Tome and Príncipe"	=>	"Sao Tome and Príncipe",
            "Saudi Arabia"	=>	"Saudi Arabia",
            "Senegal"	=>	"Senegal",
            "Serbia"	=>	"Serbia",
            "Seychelles"	=>	"Seychelles",
            "Sierra Leone"	=>	"Sierra Leone",
            "Singapore"	=>	"Singapore",
            "Slovakia (Slovak Republic)"	=>	"Slovakia (Slovak Republic)",
            "Slovenia"	=>	"Slovenia",
            "Solomon Islands"	=>	"Solomon Islands",
            "Somalia"	=>	"Somalia",
            "South Africa"	=>	"South Africa",
            "South Sudan"	=>	"South Sudan",
            "Spain"	=>	"Spain",
            "Sri Lanka"	=>	"Sri Lanka",
            "Sudan"	=>	"Sudan",
            "Suriname"	=>	"Suriname",
            "Swaziland"	=>	"Swaziland",
            "Sweden"	=>	"Sweden",
            "Switzerland"	=>	"Switzerland",
            "Syria, Syrian Arab Republic"	=>	"Syria, Syrian Arab Republic",
            "Taiwan (Republic of China)"	=>	"Taiwan (Republic of China)",
            "Tajikistan"	=>	"Tajikistan",
            "Tanzania"	=>	"Tanzania",
            "Thailand"	=>	"Thailand",
            "Tibet"	=>	"Tibet",
            "Timor-Leste (East Timor)"	=>	"Timor-Leste (East Timor)",
            "Togo"	=>	"Togo",
            "Tokelau"	=>	"Tokelau",
            "Tonga"	=>	"Tonga",
            "Trinidad and Tobago"	=>	"Trinidad and Tobago",
            "Tunisia"	=>	"Tunisia",
            "Turkey"	=>	"Turkey",
            "Turkmenistan"	=>	"Turkmenistan",
            "Turks and Caicos Islands"	=>	"Turks and Caicos Islands",
            "Tuvalu"	=>	"Tuvalu",
            "Uganda"	=>	"Uganda",
            "Ukraine"	=>	"Ukraine",
            "United Arab Emirates"	=>	"United Arab Emirates",
            "United Kingdom"	=>	"United Kingdom",
            "United States"	=>	"United States",
            "Uruguay"	=>	"Uruguay",
            "Uzbekistan"	=>	"Uzbekistan",
            "Vanuatu"	=>	"Vanuatu",
            "Vatican City State (Holy See)"	=>	"Vatican City State (Holy See)",
            "Venezuela"	=>	"Venezuela",
            "Vietnam"	=>	"Vietnam",
            "Virgin Islands (British)"	=>	"Virgin Islands (British)",
            "Virgin Islands (U.S.)"	=>	"Virgin Islands (U.S.)",
            "Wallis and Futuna Islands"	=>	"Wallis and Futuna Islands",
            "Western Sahara"	=>	"Western Sahara",
            "Yemen"	=>	"Yemen",
            "Zambia"	=>	"Zambia",
            "Zimbabwe"	=>	"Zimbabwe"
        );

        $atributos = 'class = "dropdown"';

        $countrycode = array(
            'type' => 'text',
            'pattern' => '\d+',
            'class' => 'span2',
            'name' => 'countrycode_txt',
            'placeholder' => 'Country Code',
            'title' => 'Country Code',
            'value' => $countryc,
        );

        $areacode = array(
            'type' => 'text',
            'pattern' => '\d+',
            'class' => 'span2',
            'name' => 'areacode_txt',
            'placeholder' => 'Area Code',
            'title' => 'Area Code',
            'value' => $areac,
        );

        $phone = array(
            'type' => 'text',
            'pattern' => '\d+',
            'class' => 'span2',
            'name' => 'phone_txt',
            'placeholder' => 'Phone Number',
            'title' => 'Phone Number',
            'value' => $phonenumber,
        );

        $page = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'page_txt',
            'placeholder' => 'Web Page',
            'title' => 'Web Page',
            'value' => $webpage,
        );

        $prefix = array(
            "Mr" => "Mr",
            "Mrs" => "Mrs",
            "Ms" => "Ms",
            "Miss" => "Miss",
            "PhD" => "PhD"
        );

        $fname = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'fname_txt',
            'placeholder' => 'First Name',
            'title' => 'First Name',
            'value' => $firstn,
        );

        $lname = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'lname_txt',
            'placeholder' => 'Last Name',
            'title' => 'Last Name',
            'value' => $lastn,
        );

        $position = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'position_txt',
            'placeholder' => 'Job Position',
            'title' => 'Job Position',
            'value' => $pos,
        );

        $email = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'email_txt',
            'placeholder' => 'Your email',
            'title' => 'Your email',
        );

        $cemail = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'cemail_txt',
            'placeholder' => 'Confirm your email',
            'title' => 'Confirm your email',
        );

        $user = array(
            'type' => 'text',
            'class' => 'span8',
            'name' => 'user_txt',
            'placeholder' => 'User Name',
            'title' => 'User Name',
            'value' => $usr,
        );

        $password = array(
            'type' => 'password',
            'class' => 'span8',
            'name' => 'password_txt',
            'placeholder' => 'Password',
            'title' => 'Password',
        );

        $cpassword = array(
            'type' => 'password',
            'class' => 'span8',
            'name' => 'cpassword_txt',
            'placeholder' => 'Confirm Password',
            'title' => 'Confirm Password',
        );

        $captcha = array(
            'type' => 'text',
            'class' => 'span2',
            'name' => 'captcha_txt',
            'placeholder' => 'Please enter the text above',
            'title' => 'Captcha',
        );

        // etiquetas

        $attributes = array(
            'class' => 'control-label',
        );
        ?>
        <div id="report-error" class="report-div error" style="display: block;">
            <?= validation_errors() ?>
        </div>
        <div class="control-group">
            <?= form_label('Company Name:* ','companyname_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($companyname) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Company Type:* ','companytype_txt', $attributes) ?>
            <div class="controls">
                <?= form_dropdown('companytype_txt', $companytype, '', $atributos) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Country:* ','country_txt', $attributes) ?>
            <div class="controls">
                <?= form_dropdown('country_txt', $options, '', $atributos) ?>
            </div>
        </div>

        <!-- Este bloque es nuevo -->
        <div class="control-group">
            <?= form_label('City:* ','city_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($city) ?>
            </div>
        </div>

        <div class="control-group">
            <?= form_label('Street Address:* ','street_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($street) ?>
            </div>
        </div>

        <div class="control-group">
            <?= form_label('ZIP/Postal Code:* ','zip_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($zip) ?>
            </div>
        </div>
        <!-- Fin del bloque nuevo -->

        <div class="control-group">
            <?= form_label('Phone Number:*  +','countrycode_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($countrycode) ?> (
                <?= form_input($areacode) ?> )
                <?= form_input($phone) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Web Page:* ','page_txt',$attributes) ?>
            <div class="controls controls-row">
                <?= form_input($page) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Title:* ','prefix_txt', $attributes) ?>
            <div class="controls">
                <?= form_dropdown('prefix_txt', $prefix, '', $atributos) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('First Name:* ','fname_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($fname) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Last Name:* ','lname_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($lname) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Job Position:* ','position_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($position) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Email:* ','email_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($email) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Confirm your email:* ','cemail_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($cemail) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('User Name:* ','user_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($user) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Password:* ','password_txt',$attributes) ?>
            <div class="controls">
                <?= form_password($password) ?>
            </div>
        </div>
        <div class="control-group">
            <?= form_label('Confirm Password:* ','cpassword_txt',$attributes) ?>
            <div class="controls">
                <?= form_password($cpassword) ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?= $image ?>
            </div>
            <br />
            <?= form_label('','captcha_txt',$attributes) ?>
            <div class="controls">
                <?= form_input($captcha) ?>
            </div>
        </div>
    </div>
    <div class="row" style="text-align:center;">
        <p>* Required Fields</p>
        <br />
        <p>By clicking Register, you agree to API Kontakt's <a href="<?= base_url('terms')?>">Terms and Conditions</a>, <a href="<?= base_url('privacy_policy')?>">Privacy Policy</a> and <a href="<?= base_url('cookie_policy')?>">Cookie Policy</a>.</p>
        <input type="image" src=<?= base_url('img/Register.png') ?> alt="Register">
    </div>
    <?= form_close()?>
</div>

<br />
<br />
<br />

<hr>

<footer style="text-align:center;">
    <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
</footer>

</body>
</html>