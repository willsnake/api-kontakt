<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Send Quote</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/flexigrid.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/datePicker.css') ?> rel="stylesheet">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url('supplier') ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a onclick="history.go(-1);">Go Back</a></li>
                    <li><a href=<?= base_url('supplier/profile/'.$row->company_name) ?>>Profile</a></li>
                    <li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url('supplier') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>

    <div class="row">
        <div class="span12">
            <h1 class="titulo">Send Quote</h1>
        </div>
    </div>

    <br />

    <div class="row">
        <div id="report-error" class="report-div error" style="display: block;">
            <?= validation_errors() ?>
        </div>
        <?= form_open("supplier/sendQuote_form")?>
        <input type="hidden" name="id_quote" value="<?= $id_quote_request; ?>"/>
        <input type="hidden" name="name" value="<?= $name; ?>"/>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align:center;">Product</th>
                <th style="text-align:center;">Qty</th>
                <th style="text-align:center;">Units</th>
                <th style="text-align:center;">Pharmacopeia</th>
                <th style="text-align:center;">Incoterm</th>
                <th style="text-align:center;">Port</th>
                <th style="text-align:center;">Notes From Buyer</th>
            </tr>
            </thead>
            <tbody>
            <tr class="info">
                <td style="text-align:center;"><?php echo $name ?></td>
                <td style="text-align:center;"><?php echo $quantity ?></td>
                <td style="text-align:center;"><?php echo $unit ?></td>
                <td style="text-align:center;"><?php echo $pharmacopeia ?></td>
                <td style="text-align:center;"><?php echo $incoterm ?></td>
                <td style="text-align:center;"><?php echo $port ?></td>
                <td style="text-align:center;max-width: 500px;word-wrap: break-word;"><?php echo $notes_buyer ?></td>
            </tr>
            </tbody>
        </table>
        <table class="table">
            <tbody>
            <tr>
                <td style="text-align:center;">Price*</td>
                <td><input type="text" name="price"  style="height:30px;" value="<?= $price ?>"/></td>
                <td style="text-align:center;">Currency*</td>
                <td>
                    <select class="span2" name="currency">
                        <option value="USD">USD – US Dollar</option>
                        <option value="EUR">EUR – Euro</option>
                        <option value="GBP">GBP – British Pound</option>
                        <option value="OTHER">Other</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;">Payment Terms*</td>
                <td><input type="text" name="payment"  style="height:30px;" value="<?= $payment ?>"/></td>
                <td style="text-align:center;" colspan="4"></td>
            </tr>
            <tr>
                <td style="text-align:center;">Availability*</td>
                <td><input type="text" name="availability"  style="height:30px;" value="<?= $availability ?>"/></td>
                <td style="text-align:center;" colspan="4"></td>
            </tr>
            <tr>
                <td style="text-align:center;">Notes</td>
                <td colspan="3">
                    <textarea rows="3" name="notes" class="input-xxlarge" onKeyDown="limitText(this.form.notes,this.form.countdown,100);" onKeyUp="limitText(this.form.notes,this.form.countdown,100);"><?= $notes ?></textarea>
                    <p>You can write up to 100 characters.</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="row" style="text-align:center;">
        <p>* Required fields</p>
        <input type="image" src=<?= base_url('img/SendQuote.png') ?> alt="Send Quote">
    </div>
    <?= form_close()?>

</div>

<br />
<br />
<br />

<hr>

<footer style="text-align:center;">
    <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
</footer>

<script>
    $(function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker( "option", "dateFormat", "MM dth yy" );
    });
</script>

<script>
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }
</script>

</body>
</html>