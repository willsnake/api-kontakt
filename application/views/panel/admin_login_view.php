<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="<?= base_url('css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/bootstrap-responsive.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5.min.js" ></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <?php
        $user = array(
            'name'  =>  'user_txt',
            'class' =>  'input-block-level',
            'autocomplete'  =>  'off',
            'value' =>  set_value('user_txt'),
            'placeholder' =>  'Password'
        );

        $pass = array(
            'name'  =>  'pass_txt',
            'autocomplete'  =>  'off',
            'class' =>  'input-block-level',
            'placeholder' =>  'User name'
        );

        ?>

        <?= validation_errors() ?>

        <?= form_open('panel/main_admin/validar_login', array('class' => 'form-signin', 'autocomplete' => 'off')) ?>
            <h2 class="form-signin-heading">Login</h2>
            <?= form_input($user); ?>
            <?= form_password($pass); ?>
            <?= form_submit(array('class' => 'btn btn-large btn-primary', 'name' => 'login'),'Log in') ?>
        <?= form_close() ?>


    </div>

    <script src="<?= base_url('js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-transition.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-alert.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-modal.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-scrollspy.js') ?>" ></script>
    <script src="<?= base_url('js/bootstrap-tab.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-popover.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-button.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-collapse.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-carousel.js') ?>"></script>
    <script src="<?= base_url('js/bootstrap-typeahead.js') ?>"></script>

</body>
</html>