<?php
$sql = "SELECT COUNT(id_datos_usuario) as conteo FROM usuario WHERE id_tipo_usuario = 2 OR id_tipo_usuario = 3";
$query = $this->db->query($sql);
$row = $query->row();
// Se toma el total de usuarios
$totalUsuarios = $row->conteo;

$sql = "SELECT COUNT(id_datos_usuario) as suppliers FROM usuario WHERE id_tipo_usuario = 2";
$query = $this->db->query($sql);
$row = $query->row();
// Se toma el total de suppliers
$totalSuppliers = $row->suppliers;

$sql = "SELECT COUNT(id_datos_usuario) as buyers FROM usuario WHERE id_tipo_usuario = 3";
$query = $this->db->query($sql);
$row = $query->row();
// Se toma el total de suppliers
$totalBuyers = $row->buyers;

// Se hace el calculo para el porcentaje
$porcentajeSuppliers = ($totalSuppliers * 100) / $totalUsuarios;
$porcentajeBuyers = ($totalBuyers * 100) / $totalUsuarios;

?>

<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Admin Home Page</title>

    <link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" media="screen">
    <link href="<?= base_url('bootstrap/css/bootstrap-responsive.min.css') ?>" rel="stylesheet" media="screen">
    <link href="<?= base_url('vendors/easypiechart/jquery.easy-pie-chart.css') ?>" rel="stylesheet" media="screen">
    <link href="<?= base_url('assets/styles.css') ?>" rel="stylesheet" media="screen">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?= base_url('vendors/modernizr-2.6.2-respond-1.1.0.min.js') ?>"></script>
</head>

<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?= base_url('panel/main_admin/panelHome') ?>">Admin Panel</a>
            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> User <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Profile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="<?= base_url('panel/main_admin/logout') ?>">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Users <b class="caret"></b></a>
                        <ul class="dropdown-menu" id="menu1">
                            <!-- <li>
                                <a href="#">Tools <i class="icon-arrow-right"></i></a>
                                <ul class="dropdown-menu sub-menu">
                                    <li>
                                        <a href="#">Temp Users</a>
                                    </li>
                                    <li>
                                        <a href="#">Logs</a>
                                    </li>
                                    <li>
                                        <a href="#">Errors</a>
                                    </li>
                                </ul>
                            </li> -->
                            <li>
                                <a href="<?= base_url('panel/main_admin/panelTemp') ?>">Unregistered Users</a>
                            </li>
                            <!-- <li>
                                <a href="#">Other Link</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">Other Link</a>
                            </li>
                            <li>
                                <a href="#">Other Link</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Content <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Blog</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">News</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Custom Pages</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Calendar</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="#">FAQ</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Users <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">User List</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Search</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Permissions</a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">
            <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                <li class="active">
                    <a href="<?= base_url('panel/main_admin/panelHome') ?>"><i class="icon-chevron-right"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<?= base_url('panel/main_admin/panelBuyers') ?>"><i class="icon-chevron-right"></i> Buyers</a>
                </li>
                <li>
                    <a href="<?= base_url('panel/main_admin/panelSuppliers') ?>"><i class="icon-chevron-right"></i> Suppliers</a>
                </li>
                <li>
                    <a href="<?= base_url('panel/main_admin/panelProducts') ?>"><i class="icon-chevron-right"></i> Products</a>
                </li>
            </ul>
        </div>

    <!--/span-->
        <div class="span9" id="content">

            <!-- morris bar & donut charts -->
            <div class="row-fluid section">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Users</div>
                        <div class="pull-right"><span class="badge badge-warning">Total Users <?= $totalUsuarios  ?></span>

                        </div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12 chart">
                            <h5>Users</h5>
                            <div id="hero-bar" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>

        </div>
    </div>
<hr>
</div>
<link rel="stylesheet" href="<?= base_url('vendors/morris/morris.css') ?>">

<script src="<?= base_url('vendors/jquery-1.9.1.min.js') ?>"></script>
<script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('vendors/easypiechart/jquery.easy-pie-chart.js') ?>"></script>
<script src="<?= base_url('assets/scripts.js') ?>"></script>

<script src="<?= base_url('vendors/jquery.knob.js') ?>"></script>
<script src="<?= base_url('vendors/raphael-min.js') ?>"></script>
<script src="<?= base_url('vendors/morris/morris.min.js') ?>"></script>
<script src="<?= base_url('vendors/flot/jquery.flot.js') ?>"></script>
<script src="<?= base_url('vendors/flot/jquery.flot.categories.js') ?>"></script>
<script src="<?= base_url('vendors/flot/jquery.flot.pie.js') ?>"></script>
<script src="<?= base_url('vendors/flot/jquery.flot.time.js') ?>"></script>
<script src="<?= base_url('vendors/flot/jquery.flot.stack.js') ?>"></script>
<script src="<?= base_url('vendors/flot/jquery.flot.resize.js') ?>"></script>

<script>
    var buyers = "<?= $totalBuyers ?>";
    var suppliers = "<?= $totalSuppliers ?>";

    // Morris Bar Chart
    Morris.Bar({
        element: 'hero-bar',
        data: [
            {user: 'Buyers', total: buyers},
            {user: 'Suppliers', total: suppliers}
        ],
        xkey: 'user',
        ykeys: ['total'],
        labels: ['Total'],
        barRatio: 1,
        xLabelMargin: 10,
        hideHover: 'auto',
        barColors: ["#3d88ba"]
    });

</script>

</body>

</html>