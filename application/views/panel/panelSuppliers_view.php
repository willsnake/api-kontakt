<?php
$sql = "SELECT COUNT(id_datos_usuario) as conteo FROM usuario WHERE id_tipo_usuario = 2 OR id_tipo_usuario = 3";
$query = $this->db->query($sql);
$row = $query->row();
// Se toma el total de usuarios
$totalUsuarios = $row->conteo;

$sql = "SELECT COUNT(id_datos_usuario) as suppliers FROM usuario WHERE id_tipo_usuario = 2";
$query = $this->db->query($sql);
$row = $query->row();
// Se toma el total de suppliers
$totalSuppliers = $row->suppliers;

$sql = "SELECT COUNT(id_datos_usuario) as buyers FROM usuario WHERE id_tipo_usuario = 3";
$query = $this->db->query($sql);
$row = $query->row();
// Se toma el total de suppliers
$totalBuyers = $row->buyers;

// Se hace el calculo para el porcentaje
$porcentajeSuppliers = ($totalSuppliers * 100) / $totalUsuarios;
$porcentajeBuyers = ($totalBuyers * 100) / $totalUsuarios;

?>

<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Suppliers Page</title>

    <link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" media="screen">
    <link href="<?= base_url('bootstrap/css/bootstrap-responsive.min.css') ?>" rel="stylesheet" media="screen">
    <link href="<?= base_url('assets/styles.css') ?>" rel="stylesheet" media="screen">

    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?= base_url('panel/main_admin/panelHome') ?>">Admin Panel</a>
            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> User <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Profile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="<?= base_url('panel/main_admin/logout') ?>">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav">
                    <!-- <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Users <b class="caret"></b></a>
                        <ul class="dropdown-menu" id="menu1">
                            <li>
                                <a href="#">Tools <i class="icon-arrow-right"></i></a>
                                <ul class="dropdown-menu sub-menu">
                                    <li>
                                        <a href="#">Temp Users</a>
                                    </li>
                                    <li>
                                        <a href="#">Logs</a>
                                    </li>
                                    <li>
                                        <a href="#">Errors</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?= base_url('panel/main_admin/panelTemp') ?>">Unregistered Users</a>
                            </li>
                            <li>
                                <a href="#">Other Link</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">Other Link</a>
                            </li>
                            <li>
                                <a href="#">Other Link</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Content <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Blog</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">News</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Custom Pages</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Calendar</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="#">FAQ</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Users <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">User List</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Search</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Permissions</a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">
            <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                <li>
                    <a href="<?= base_url('panel/main_admin/panelHome') ?>"><i class="icon-chevron-right"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<?= base_url('panel/main_admin/panelBuyers') ?>"><i class="icon-chevron-right"></i> Buyers</a>
                </li>
                <li class="active">
                    <a href="<?= base_url('panel/main_admin/panelSuppliers') ?>"><i class="icon-chevron-right"></i> Suppliers</a>
                </li>
                <li>
                    <a href="<?= base_url('panel/main_admin/panelProducts') ?>"><i class="icon-chevron-right"></i> Products</a>
                </li>
            </ul>
        </div>

        <!--/span-->
        <div class="span9" id="content">

            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Suppliers</div>
                        <div class="pull-right"><span class="badge badge-warning">Total Suppliers <?= $totalSuppliers ?></span>

                        </div>
                    </div>
                    <div class="block-content collapse in">
                        <?php echo $output; ?>
                    </div>
                </div>
                <!-- /block -->
            </div>

        </div>
    </div>
    <hr>
</div>

<script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>

</body>

</html>