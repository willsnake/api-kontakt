<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Register Supplier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/flexigrid.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">
	
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
				<div class="nav-collapse collapse">
					<li class="brand" href="#"></li>
					<ul class="nav">
						<li class="home"><a href=<?= base_url() ?>>Home</a></li>
					</ul>
				</div>
				
				<form class="navbar-form pull-right">
					<ul class="nav">			
						<li><a href=<?= base_url('main/login') ?>>Login</a></li>
					</ul>
				</form>
			</div>
		</div>
    </div>
	
	<div class="container">
		<div class="logo-register">
			<a href=<?= base_url() ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
		</div>
		
		<div class="row">
			<?= form_open("main/validar_registro_supplier",$atributos = array('class' => 'form-horizontal' ), $atributos = array('tipo_usuario_txt' => 2 ))?>
			<h1 class="titulo">Register Supplier</h1>
			<?php
				$companyname = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'companyname_txt',
					'placeholder' => 'Company Name',
					'title' => 'Company Name',
					'value' => set_value('companyname_txt'),
				);


                $street = array(
                    'type' => 'text',
                    'class' => 'span8',
                    'name' => 'street_txt',
                    'placeholder' => 'Address',
                    'title' => 'Address',
                    'value' => set_value('street_txt'),

                );

                $city = array(
                    'type' => 'text',
                    'class' => 'span8',
                    'name' => 'city_txt',
                    'placeholder' => 'City',
                    'title' => 'City',
                    'value' => set_value('city_txt'),
                );

                $zip = array(
                    'type' => 'text',
                    'class' => 'span8',
                    'name' => 'zip_txt',
                    'placeholder' => 'ZIP Code',
                    'title' => 'ZIP Code',
                    'value' => set_value('zip_txt'),
                );
				
				$countrycode = array(
					'type' => 'text',
					'pattern' => '\d+',
					'class' => 'span2',
					'name' => 'countrycode_txt',
					'placeholder' => 'Country Code',
					'title' => 'Country Code',
                    'value' => set_value('countrycode_txt'),
				);
				
				$areacode = array(
					'type' => 'text',
					'pattern' => '\d+',
					'class' => 'span2',
					'name' => 'areacode_txt',
					'placeholder' => 'Area Code',
					'title' => 'Area Code',
                    'value' => set_value('areacode_txt'),
				);
				
				$phone = array(
					'type' => 'text',
					'pattern' => '\d+',
					'class' => 'span2',
					'name' => 'phone_txt',
					'placeholder' => 'Phone Number',
					'title' => 'Phone Number',
                    'value' => set_value('phone_txt'),
				);
				
				$page = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'page_txt',
					'placeholder' => 'Web Page',
					'title' => 'Web Page',
                    'value' => set_value('page_txt'),
				);
				
				$fname = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'fname_txt',
					'placeholder' => 'First Name',
					'title' => 'First Name',
                    'value' => set_value('fname_txt'),
				);
				
				$lname = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'lname_txt',
					'placeholder' => 'Last Name',
					'title' => 'Last Name',
                    'value' => set_value('lname_txt'),
				);
				
				$position = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'position_txt',
					'placeholder' => 'Job Position',
					'title' => 'Job Position',
                    'value' => set_value('position_txt'),
				);
				
				$email = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'email_txt',
					'placeholder' => 'Your email',
					'title' => 'Your email',
                    'value' => set_value('email_txt'),
				);
				
				$cemail = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'cemail_txt',
					'placeholder' => 'Confirm your email',
					'title' => 'Confirm your email',
				);
				
				$user = array(
					'type' => 'text',
					'class' => 'span8',
					'name' => 'user_txt',
					'placeholder' => 'User Name',
					'title' => 'User Name',
                    'value' => set_value('user_txt'),
				);
				
				$password = array(
					'type' => 'password',
					'class' => 'span8',
					'name' => 'password_txt',
					'placeholder' => 'Password',
					'title' => 'Password',
				);
				
				$cpassword = array(
					'type' => 'password',
					'class' => 'span8',
					'name' => 'cpassword_txt',
					'placeholder' => 'Confirm Password',
					'title' => 'Confirm Password',
				);

                $captcha = array(
                    'type' => 'text',
                    'class' => 'span2',
                    'name' => 'captcha_txt',
                    'placeholder' => 'Please enter the text above',
                    'title' => 'Captcha',
                );
				
				// etiquetas
				
				$attributes = array(
					'class' => 'control-label',
				);
			?>

            <?= validation_errors('<div id="report-error" class="report-div error" style="display: block;"><p>', '</p></div>'); ?>
			<div class="control-group">
				<?= form_label('Company Name:* ','companyname_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($companyname) ?>
				</div>
			</div>
			<div class="control-group">
                <label for="companytype_txt" class="control-label">Company Type:* </label>
                <div class="controls">
                    <select name="companytype_txt" class="dropdown">
                        <option value="Manufacturer" <?= set_select('companytype_txt','Manufacturer') ?>>Manufacturer</option>
                        <option value="Trader" <?= set_select('companytype_txt','Trader') ?>>Trader</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="country_txt" class="control-label">Country:* </label>
                <div class="controls">
                    <select name="country_txt" class="dropdown">
                        <option value="Afghanistan" <?= set_select('country_txt','Afghanistan') ?>>Afghanistan</option>
                        <option value="Albania" <?= set_select('country_txt','Albania') ?>>Albania</option>
                        <option value="Algeria" <?= set_select('country_txt','Algeria') ?>>Algeria</option>
                        <option value="American Samoa" <?= set_select('country_txt','American Samoa') ?>>American Samoa</option>
                        <option value="Andorra" <?= set_select('country_txt','Andorra') ?>>Andorra</option>
                        <option value="Angola" <?= set_select('country_txt','Angola') ?>>Angola</option>
                        <option value="Anguilla" <?= set_select('country_txt','Anguilla') ?>>Anguilla</option>
                        <option value="Antarctica" <?= set_select('country_txt','Antarctica') ?>>Antarctica</option>
                        <option value="Antigua and Barbuda" <?= set_select('country_txt','Antigua and Barbuda') ?>>Antigua and Barbuda</option>
                        <option value="Argentina" <?= set_select('country_txt','Argentina') ?>>Argentina</option>
                        <option value="Armenia" <?= set_select('country_txt','Armenia') ?>>Armenia</option>
                        <option value="Aruba" <?= set_select('country_txt','Aruba') ?>>Aruba</option>
                        <option value="Australia" <?= set_select('country_txt','Australia') ?>>Australia</option>
                        <option value="Austria" <?= set_select('country_txt','Austria') ?>>Austria</option>
                        <option value="Azerbaijan" <?= set_select('country_txt','Azerbaijan') ?>>Azerbaijan</option>
                        <option value="Bahamas" <?= set_select('country_txt','Bahamas') ?>>Bahamas</option>
                        <option value="Bahrain" <?= set_select('country_txt','Bahrain') ?>>Bahrain</option>
                        <option value="Bangladesh" <?= set_select('country_txt','Bangladesh') ?>>Bangladesh</option>
                        <option value="Barbados" <?= set_select('country_txt','Barbados') ?>>Barbados</option>
                        <option value="Belarus" <?= set_select('country_txt','Belarus') ?>>Belarus</option>
                        <option value="Belgium" <?= set_select('country_txt','Belgium') ?>>Belgium</option>
                        <option value="Belize" <?= set_select('country_txt','Belize') ?>>Belize</option>
                        <option value="Benin" <?= set_select('country_txt','Benin') ?>>Benin</option>
                        <option value="Bermuda" <?= set_select('country_txt','Bermuda') ?>>Bermuda</option>
                        <option value="Bhutan" <?= set_select('country_txt','Bhutan') ?>>Bhutan</option>
                        <option value="Bolivia" <?= set_select('country_txt','Bolivia') ?>>Bolivia</option>
                        <option value="Bosnia and Herzegovina" <?= set_select('country_txt','Bosnia and Herzegovina') ?>>Bosnia and Herzegovina</option>
                        <option value="Botswana" <?= set_select('country_txt','Botswana') ?>>Botswana</option>
                        <option value="Brazil" <?= set_select('country_txt','Brazil') ?>>Brazil</option>
                        <option value="Brunei Darussalam" <?= set_select('country_txt','Brunei Darussalam') ?>>Brunei Darussalam</option>
                        <option value="Bulgaria" <?= set_select('country_txt','Bulgaria') ?>>Bulgaria</option>
                        <option value="Burkina Faso" <?= set_select('country_txt','Burkina Faso') ?>>Burkina Faso</option>
                        <option value="Burundi" <?= set_select('country_txt','Burundi') ?>>Burundi</option>
                        <option value="Cambodia" <?= set_select('country_txt','Cambodia') ?>>Cambodia</option>
                        <option value="Cameroon" <?= set_select('country_txt','Cameroon') ?>>Cameroon</option>
                        <option value="Canada" <?= set_select('country_txt','Canada') ?>>Canada</option>
                        <option value="Cape Verde" <?= set_select('country_txt','Cape Verde') ?>>Cape Verde</option>
                        <option value="Cayman Islands" <?= set_select('country_txt','Cayman Islands') ?>>Cayman Islands</option>
                        <option value="Central African Republic" <?= set_select('country_txt','entral African Republic') ?>>Central African Republic</option>
                        <option value="Chad" <?= set_select('country_txt','Chad') ?>>Chad</option>
                        <option value="Chile" <?= set_select('country_txt','Chile') ?>>Chile</option>
                        <option value="China" <?= set_select('country_txt','China') ?>>China</option>
                        <option value="Christmas Island" <?= set_select('country_txt','Christmas Island') ?>>Christmas Island</option>
                        <option value="Cocos (Keeling) Islands" <?= set_select('country_txt','Cocos (Keeling) Islands') ?>>Cocos (Keeling) Islands</option>
                        <option value="Colombia" <?= set_select('country_txt','Colombia') ?>>Colombia</option>
                        <option value="Comoros" <?= set_select('country_txt','Comoros') ?>>Comoros</option>
                        <option value="Democratic Republic of the Congo (Kinshasa)" <?= set_select('country_txt','Democratic Republic of the Congo (Kinshasa)') ?>>Democratic Republic of the Congo (Kinshasa)</option>
                        <option value="Congo, Republic of (Brazzaville)" <?= set_select('country_txt','Congo, Republic of (Brazzaville)') ?>>Congo, Republic of (Brazzaville)</option>
                        <option value="Cook Islands" <?= set_select('country_txt','Cook Islands') ?>>Cook Islands</option>
                        <option value="Costa Rica" <?= set_select('country_txt','Costa Rica') ?>>Costa Rica</option>
                        <option value="Ivory Coast (Cote d'Ivoire)" <?= set_select('country_txt',"Ivory Coast (Cote d'Ivoire)") ?>>Ivory Coast (Cote d'Ivoire)</option>
                        <option value="Croatia" <?= set_select('country_txt','Croatia') ?>>Croatia</option>
                        <option value="Cuba" <?= set_select('country_txt','Cuba') ?>>Cuba</option>
                        <option value="Cyprus" <?= set_select('country_txt','Cyprus') ?>>Cyprus</option>
                        <option value="Czech Republic" <?= set_select('country_txt','Czech Republic') ?>>Czech Republic</option>
                        <option value="Denmark" <?= set_select('country_txt','Denmark') ?>>Denmark</option>
                        <option value="Djibouti" <?= set_select('country_txt','Djibouti') ?>>Djibouti</option>
                        <option value="Dominica" <?= set_select('country_txt','Dominica') ?>>Dominica</option>
                        <option value="Dominican Republic" <?= set_select('country_txt','Dominican Republic') ?>>Dominican Republic</option>
                        <option value="East Timor Timor-Leste" <?= set_select('country_txt','East Timor Timor-Leste') ?>>East Timor Timor-Leste</option>
                        <option value="Ecuador" <?= set_select('country_txt','Ecuador') ?>>Ecuador</option>
                        <option value="Egypt" <?= set_select('country_txt','Egypt') ?>>Egypt</option>
                        <option value="El Salvador" <?= set_select('country_txt','El Salvador') ?>>El Salvador</option>
                        <option value="Equatorial Guinea" <?= set_select('country_txt','Equatorial Guinea') ?>>Equatorial Guinea</option>
                        <option value="Eritrea" <?= set_select('country_txt','Eritrea') ?>>Eritrea</option>
                        <option value="Estonia" <?= set_select('country_txt','Estonia') ?>>Estonia</option>
                        <option value="Ethiopia" <?= set_select('country_txt','Ethiopia') ?>>Ethiopia</option>
                        <option value="Falkland Islands" <?= set_select('country_txt','Falkland Islands') ?>>Falkland Islands</option>
                        <option value="Faroe Islands" <?= set_select('country_txt','Faroe Islands') ?>>Faroe Islands</option>
                        <option value="Fiji" <?= set_select('country_txt','Fiji') ?>>Fiji</option>
                        <option value="Finland" <?= set_select('country_txt','Finland') ?>>Finland</option>
                        <option value="France" <?= set_select('country_txt','France') ?>>France</option>
                        <option value="French Guiana" <?= set_select('country_txt','French Guiana') ?>>French Guiana</option>
                        <option value="French Polynesia" <?= set_select('country_txt','French Polynesia') ?>>French Polynesia</option>
                        <option value="French Southern Territories" <?= set_select('country_txt','French Southern Territories') ?>>French Southern Territories</option>
                        <option value="Gabon" <?= set_select('country_txt','Gabon') ?>>Gabon</option>
                        <option value="Gambia" <?= set_select('country_txt','Gambia') ?>>Gambia</option>
                        <option value="Georgia" <?= set_select('country_txt','Georgia') ?>>Georgia</option>
                        <option value="Germany" <?= set_select('country_txt','Germany') ?>>Germany</option>
                        <option value="Ghana" <?= set_select('country_txt','Ghana') ?>>Ghana</option>
                        <option value="Gibraltar" <?= set_select('country_txt','Gibraltar') ?>>Gibraltar</option>
                        <option value="Great Britain" <?= set_select('country_txt','Great Britain') ?>>Great Britain</option>
                        <option value="Greece" <?= set_select('country_txt','Greece') ?>>Greece</option>
                        <option value="Greenland" <?= set_select('country_txt','Greenland') ?>>Greenland</option>
                        <option value="Grenada" <?= set_select('country_txt','Grenada') ?>>Grenada</option>
                        <option value="Guadeloupe" <?= set_select('country_txt','Guadeloupe') ?>>Guadeloupe</option>
                        <option value="Guam" <?= set_select('country_txt','Guam') ?>>Guam</option>
                        <option value="Guatemala" <?= set_select('country_txt','Guatemala') ?>>Guatemala</option>
                        <option value="Guinea" <?= set_select('country_txt','Guinea') ?>>Guinea</option>
                        <option value="Guinea-Bissau" <?= set_select('country_txt','Guinea-Bissau') ?>>Guinea-Bissau</option>
                        <option value="Guyana" <?= set_select('country_txt','Guyana') ?>>Guyana</option>
                        <option value="Haiti" <?= set_select('country_txt','Haiti') ?>>Haiti</option>
                        <option value="Holy See" <?= set_select('country_txt','Holy See') ?>>Holy See</option>
                        <option value="Honduras" <?= set_select('country_txt','Honduras') ?>>Honduras</option>
                        <option value="Hong Kong" <?= set_select('country_txt','Hong Kong') ?>>Hong Kong</option>
                        <option value="Hungary" <?= set_select('country_txt','Hungary') ?>>Hungary</option>
                        <option value="Iceland" <?= set_select('country_txt','Iceland') ?>>Iceland</option>
                        <option value="India" <?= set_select('country_txt','India') ?>>India</option>
                        <option value="Indonesia" <?= set_select('country_txt','Indonesia') ?>>Indonesia</option>
                        <option value="Iran (Islamic Republic of)" <?= set_select('country_txt','Iran (Islamic Republic of)') ?>>Iran (Islamic Republic of)</option>
                        <option value="Iraq" <?= set_select('country_txt','Iraq') ?>>Iraq</option>
                        <option value="Ireland" <?= set_select('country_txt','Ireland') ?>>Ireland</option>
                        <option value="Israel" <?= set_select('country_txt','Israel') ?>>Israel</option>
                        <option value="Italy" <?= set_select('country_txt','Italy') ?>>Italy</option>
                        <option value="Jamaica" <?= set_select('country_txt','Jamaica') ?>>Jamaica</option>
                        <option value="Japan" <?= set_select('country_txt','Japan') ?>>Japan</option>
                        <option value="Jordan" <?= set_select('country_txt','Jordan') ?>>Jordan</option>
                        <option value="Kazakhstan" <?= set_select('country_txt','Kazakhstan') ?>>Kazakhstan</option>
                        <option value="Kenya" <?= set_select('country_txt','Kenya') ?>>Kenya</option>
                        <option value="Kiribati" <?= set_select('country_txt','Kiribati') ?>>Kiribati</option>
                        <option value="Korea, Democratic People's Rep. (North Korea)" <?= set_select('country_txt',"Korea, Democratic People's Rep. (North Korea)") ?>>Korea, Democratic People's Rep. (North Korea)</option>
                        <option value="Korea, Republic of (South Korea)" <?= set_select('country_txt','Korea, Republic of (South Korea)') ?>>Korea, Republic of (South Korea)</option>
                        <option value="Kosovo" <?= set_select('country_txt','Kosovo') ?>>Kosovo</option>
                        <option value="Kuwait" <?= set_select('country_txt','Kuwait') ?>>Kuwait</option>
                        <option value="Kyrgyzstan" <?= set_select('country_txt','Kyrgyzstan') ?>>Kyrgyzstan</option>
                        <option value="Lao, People's Democratic Republic" <?= set_select('country_txt',"Lao, People's Democratic Republic") ?>>Lao, People's Democratic Republic</option>
                        <option value="Latvia" <?= set_select('country_txt','Latvia') ?>>Latvia</option>
                        <option value="Lebanon" <?= set_select('country_txt','Lebanon') ?>>Lebanon</option>
                        <option value="Lesotho" <?= set_select('country_txt','Lesotho') ?>>Lesotho</option>
                        <option value="Liberia" <?= set_select('country_txt','Liberia') ?>>Liberia</option>
                        <option value="Libya" <?= set_select('country_txt','Libya') ?>>Libya</option>
                        <option value="Liechtenstein" <?= set_select('country_txt','Liechtenstein') ?>>Liechtenstein</option>
                        <option value="Lithuania" <?= set_select('country_txt','Lithuania') ?>>Lithuania</option>
                        <option value="Luxembourg" <?= set_select('country_txt','Luxembourg') ?>>Luxembourg</option>
                        <option value="Macao" <?= set_select('country_txt','Macao') ?>>Macao</option>
                        <option value="Macedonia, Rep. Of" <?= set_select('country_txt','Macedonia, Rep. Of') ?>>Macedonia, Rep. Of</option>
                        <option value="Madagascar" <?= set_select('country_txt','Madagascar') ?>>Madagascar</option>
                        <option value="Malawi" <?= set_select('country_txt','Malawi') ?>>Malawi</option>
                        <option value="Malaysia" <?= set_select('country_txt','Malaysia') ?>>Malaysia</option>
                        <option value="Maldives" <?= set_select('country_txt','Maldives') ?>>Maldives</option>
                        <option value="Mali" <?= set_select('country_txt','Mali') ?>>Mali</option>
                        <option value="Malta" <?= set_select('country_txt','Malta') ?>>Malta</option>
                        <option value="Marshall Islands" <?= set_select('country_txt','Marshall Islands') ?>>Marshall Islands</option>
                        <option value="Martinique" <?= set_select('country_txt','Martinique') ?>>Martinique</option>
                        <option value="Mauritania" <?= set_select('country_txt','Mauritania') ?>>Mauritania</option>
                        <option value="Mauritius" <?= set_select('country_txt','Mauritius') ?>>Mauritius</option>
                        <option value="Mayotte" <?= set_select('country_txt','Mayotte') ?>>Mayotte</option>
                        <option value="Mexico" <?= set_select('country_txt','Mexico') ?>>Mexico</option>
                        <option value="Micronesia, Federal States of" <?= set_select('country_txt','Micronesia, Federal States of') ?>>Micronesia, Federal States of</option>
                        <option value="Moldova, Republic of" <?= set_select('country_txt','Moldova, Republic of') ?>>Moldova, Republic of</option>
                        <option value="Monaco" <?= set_select('country_txt','Monaco') ?>>Monaco</option>
                        <option value="Mongolia" <?= set_select('country_txt','Mongolia') ?>>Mongolia</option>
                        <option value="Montenegro" <?= set_select('country_txt','Montenegro') ?>>Montenegro</option>
                        <option value="Montserrat" <?= set_select('country_txt','Montserrat') ?>>Montserrat</option>
                        <option value="Morocco" <?= set_select('country_txt','Morocco') ?>>Morocco</option>
                        <option value="Mozambique" <?= set_select('country_txt','Mozambique') ?>>Mozambique</option>
                        <option value="Myanmar, Burma" <?= set_select('country_txt','Myanmar, Burma') ?>>Myanmar, Burma</option>
                        <option value="Namibia" <?= set_select('country_txt','Namibia') ?>>Namibia</option>
                        <option value="Nauru" <?= set_select('country_txt','Nauru') ?>>Nauru</option>
                        <option value="Nepal" <?= set_select('country_txt','Nepal') ?>>Nepal</option>
                        <option value="Netherlands" <?= set_select('country_txt','Netherlands') ?>>Netherlands</option>
                        <option value="Netherlands Antilles" <?= set_select('country_txt','Netherlands Antilles') ?>>Netherlands Antilles</option>
                        <option value="New Caledonia" <?= set_select('country_txt','New Caledonia') ?>>New Caledonia</option>
                        <option value="New Zealand" <?= set_select('country_txt','New Zealand') ?>>New Zealand</option>
                        <option value="Nicaragua" <?= set_select('country_txt','Nicaragua') ?>>Nicaragua</option>
                        <option value="Niger" <?= set_select('country_txt','Niger') ?>>Niger</option>
                        <option value="Nigeria" <?= set_select('country_txt','Nigeria') ?>>Nigeria</option>
                        <option value="Niue" <?= set_select('country_txt','Niue') ?>>Niue</option>
                        <option value="Northern Mariana Islands" <?= set_select('country_txt','Northern Mariana Islands') ?>>Northern Mariana Islands</option>
                        <option value="Norway" <?= set_select('country_txt','Norway') ?>>Norway</option>
                        <option value="Oman" <?= set_select('country_txt','Oman') ?>>Oman</option>
                        <option value="Pakistan" <?= set_select('country_txt','Pakistan') ?>>Pakistan</option>
                        <option value="Palau" <?= set_select('country_txt','Palau') ?>>Palau</option>
                        <option value="Palestinian territories" <?= set_select('country_txt','Palestinian territories') ?>>Palestinian territories</option>
                        <option value="Panama" <?= set_select('country_txt','Panama') ?>>Panama</option>
                        <option value="Papua New Guinea" <?= set_select('country_txt','Papua New Guinea') ?>>Papua New Guinea</option>
                        <option value="Paraguay" <?= set_select('country_txt','Paraguay') ?>>Paraguay</option>
                        <option value="Peru" <?= set_select('country_txt','Peru') ?>>Peru</option>
                        <option value="Philippines" <?= set_select('country_txt','Philippines') ?>>Philippines</option>
                        <option value="Pitcairn Island" <?= set_select('country_txt','Pitcairn Island') ?>>Pitcairn Island</option>
                        <option value="Poland" <?= set_select('country_txt','Poland') ?>>Poland</option>
                        <option value="Portugal" <?= set_select('country_txt','Portugal') ?>>Portugal</option>
                        <option value="Puerto Rico" <?= set_select('country_txt','Puerto Rico') ?>>Puerto Rico</option>
                        <option value="Qatar" <?= set_select('country_txt','Qatar') ?>>Qatar</option>
                        <option value="Reunion Island" <?= set_select('country_txt','Reunion Island') ?>>Reunion Island</option>
                        <option value="Romania" <?= set_select('country_txt','Romania') ?>>Romania</option>
                        <option value="Russian Federation" <?= set_select('country_txt','Russian Federation') ?>>Russian Federation</option>
                        <option value="Rwanda" <?= set_select('country_txt','Rwanda') ?>>Rwanda</option>
                        <option value="Saint Kitts and Nevis" <?= set_select('country_txt','Saint Kitts and Nevis') ?>>Saint Kitts and Nevis</option>
                        <option value="Saint Lucia" <?= set_select('country_txt','Saint Lucia') ?>>Saint Lucia</option>
                        <option value="Saint Vincent and the Grenadines" <?= set_select('country_txt','Saint Vincent and the Grenadines') ?>>Saint Vincent and the Grenadines</option>
                        <option value="Samoa" <?= set_select('country_txt','Samoa') ?>>Samoa</option>
                        <option value="San Marino" <?= set_select('country_txt','San Marino') ?>>San Marino</option>
                        <option value="Sao Tome and Príncipe" <?= set_select('country_txt','Sao Tome and Príncipe') ?>>Sao Tome and Príncipe</option>
                        <option value="Saudi Arabia" <?= set_select('country_txt','Saudi Arabia') ?>>Saudi Arabia</option>
                        <option value="Senegal" <?= set_select('country_txt','Senegal') ?>>Senegal</option>
                        <option value="Serbia" <?= set_select('country_txt','Serbia') ?>>Serbia</option>
                        <option value="Seychelles" <?= set_select('country_txt','Seychelles') ?>>Seychelles</option>
                        <option value="Sierra Leone" <?= set_select('country_txt','Sierra Leone') ?>>Sierra Leone</option>
                        <option value="Singapore" <?= set_select('country_txt','Singapore') ?>>Singapore</option>
                        <option value="Slovakia (Slovak Republic)" <?= set_select('country_txt','Slovakia (Slovak Republic)') ?>>Slovakia (Slovak Republic)</option>
                        <option value="Slovenia" <?= set_select('country_txt','Slovenia') ?>>Slovenia</option>
                        <option value="Solomon Islands" <?= set_select('country_txt','Solomon Islands') ?>>Solomon Islands</option>
                        <option value="Somalia" <?= set_select('country_txt','Somalia') ?>>Somalia</option>
                        <option value="South Africa" <?= set_select('country_txt','South Africa') ?>>South Africa</option>
                        <option value="South Sudan" <?= set_select('country_txt','South Sudan') ?>>South Sudan</option>
                        <option value="Spain" <?= set_select('country_txt','Spain') ?>>Spain</option>
                        <option value="Sri Lanka" <?= set_select('country_txt','Sri Lanka') ?>>Sri Lanka</option>
                        <option value="Sudan" <?= set_select('country_txt','Sudan') ?>>Sudan</option>
                        <option value="Suriname" <?= set_select('country_txt','Suriname') ?>>Suriname</option>
                        <option value="Swaziland" <?= set_select('country_txt','Swaziland') ?>>Swaziland</option>
                        <option value="Sweden" <?= set_select('country_txt','Sweden') ?>>Sweden</option>
                        <option value="Switzerland" <?= set_select('country_txt','Switzerland') ?>>Switzerland</option>
                        <option value="Syria, Syrian Arab Republic" <?= set_select('country_txt','Syria, Syrian Arab Republic') ?>>Syria, Syrian Arab Republic</option>
                        <option value="Taiwan (Republic of China)" <?= set_select('country_txt','Taiwan (Republic of China)') ?>>Taiwan (Republic of China)</option>
                        <option value="Tajikistan" <?= set_select('country_txt','Tajikistan') ?>>Tajikistan</option>
                        <option value="Tanzania" <?= set_select('country_txt','Tanzania') ?>>Tanzania</option>
                        <option value="Thailand" <?= set_select('country_txt','Thailand') ?>>Thailand</option>
                        <option value="Tibet" <?= set_select('country_txt','Tibet') ?>>Tibet</option>
                        <option value="Timor-Leste (East Timor)" <?= set_select('country_txt','Timor-Leste (East Timor)') ?>>Timor-Leste (East Timor)</option>
                        <option value="Togo" <?= set_select('country_txt','Togo') ?>>Togo</option>
                        <option value="Tokelau" <?= set_select('country_txt','Tokelau') ?>>Tokelau</option>
                        <option value="Tonga" <?= set_select('country_txt','Tonga') ?>>Tonga</option>
                        <option value="Trinidad and Tobago" <?= set_select('country_txt','Trinidad and Tobago') ?>>Trinidad and Tobago</option>
                        <option value="Tunisia" <?= set_select('country_txt','Tunisia') ?>>Tunisia</option>
                        <option value="Turkey" <?= set_select('country_txt','Turkey') ?>>Turkey</option>
                        <option value="Turkmenistan" <?= set_select('country_txt','Turkmenistan') ?>>Turkmenistan</option>
                        <option value="Turks and Caicos Islands" <?= set_select('country_txt','Turks and Caicos Islands') ?>>Turks and Caicos Islands</option>
                        <option value="Tuvalu" <?= set_select('country_txt','Tuvalu') ?>>Tuvalu</option>
                        <option value="Uganda" <?= set_select('country_txt','Uganda') ?>>Uganda</option>
                        <option value="Ukraine" <?= set_select('country_txt','Ukraine') ?>>Ukraine</option>
                        <option value="United Arab Emirates" <?= set_select('country_txt','United Arab Emirates') ?>>United Arab Emirates</option>
                        <option value="United Kingdom" <?= set_select('country_txt','United Kingdom') ?>>United Kingdom</option>
                        <option value="United States" <?= set_select('country_txt','United States') ?>>United States</option>
                        <option value="Uruguay" <?= set_select('country_txt','Uruguay') ?>>Uruguay</option>
                        <option value="Uzbekistan" <?= set_select('country_txt','Uzbekistan') ?>>Uzbekistan</option>
                        <option value="Vanuatu" <?= set_select('country_txt','Vanuatu') ?>>Vanuatu</option>
                        <option value="Vatican City State (Holy See)" <?= set_select('country_txt','Vatican City State (Holy See)') ?>>Vatican City State (Holy See)</option>
                        <option value="Venezuela" <?= set_select('country_txt','Venezuela') ?>>Venezuela</option>
                        <option value="Vietnam" <?= set_select('country_txt','Vietnam') ?>>Vietnam</option>
                        <option value="Virgin Islands (British)" <?= set_select('country_txt','Virgin Islands (British)') ?>>Virgin Islands (British)</option>
                        <option value="Virgin Islands (U.S.)" <?= set_select('country_txt','Virgin Islands (U.S.)') ?>>Virgin Islands (U.S.)</option>
                        <option value="Wallis and Futuna Islands" <?= set_select('country_txt','Wallis and Futuna Islands') ?>>Wallis and Futuna Islands</option>
                        <option value="Western Sahara" <?= set_select('country_txt','Western Sahara') ?>>Western Sahara</option>
                        <option value="Yemen" <?= set_select('country_txt','Yemen') ?>>Yemen</option>
                        <option value="Zambia" <?= set_select('country_txt','Zambia') ?>>Zambia</option>
                        <option value="Zimbabwe" <?= set_select('country_txt','Zimbabwe') ?>>Zimbabwe</option>
                        <option value="Zimbabwe" <?= set_select('country_txt','Zimbabwe') ?>>Zimbabwe</option>
                    </select>
                </div>
            </div>

            <!-- Este bloque es nuevo -->
            <div class="control-group">
                <?= form_label('City:* ','city_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($city) ?>
                </div>
            </div>

            <div class="control-group">
                <?= form_label('Street Address:* ','street_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($street) ?>
                </div>
            </div>

            <div class="control-group">
                <?= form_label('ZIP/Postal Code:* ','zip_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($zip) ?>
                </div>
            </div>
            <!-- Fin del bloque nuevo -->

			<div class="control-group">
				<?= form_label('Phone Number:*  +','countrycode_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($countrycode) ?> (
					<?= form_input($areacode) ?> )
					<?= form_input($phone) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Web Page:* ','page_txt',$attributes) ?>
				<div class="controls controls-row">
					<?= form_input($page) ?>
				</div>
			</div>
            <div class="control-group">
                <div class="controls">
                    <select name="prefix_txt" class="dropdown">
                        <option value="Mr" <?= set_select('prefix_txt','Mr') ?>>Mr</option>
                        <option value="Mrs" <?= set_select('prefix_txt','Mrs') ?>>Mrs</option>
                        <option value="Ms" <?= set_select('prefix_txt','Ms') ?>>Ms</option>
                        <option value="Miss" <?= set_select('prefix_txt','Miss') ?>>Miss</option>
                        <option value="PhD" <?= set_select('prefix_txt','PhD') ?>>PhD</option>
                    </select>
                </div>
            </div>
			<div class="control-group">
				<?= form_label('First Name:* ','fname_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($fname) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Last Name:* ','lname_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($lname) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Job Position:* ','position_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($position) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Email:* ','email_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($email) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Confirm your email:* ','cemail_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($cemail) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('User Name:* ','user_txt',$attributes) ?>
				<div class="controls">
					<?= form_input($user) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Password:* ','password_txt',$attributes) ?>
				<div class="controls">
					<?= form_password($password) ?>
				</div>
			</div>
			<div class="control-group">
				<?= form_label('Confirm Password:* ','cpassword_txt',$attributes) ?>
				<div class="controls">
					<?= form_password($cpassword) ?>
				</div>
			</div>
            <div class="control-group">
                <div class="controls">
                    <?= $image ?>
                </div>
                <br />
                <?= form_label('','captcha_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($captcha) ?>
                </div>
            </div>
		</div>
		<div class="row" style="text-align:center;">
            <p>* Required Fields</p>
            <br />
            <p>By clicking Register, you agree to API Kontakt's <a href="<?= base_url('terms')?>">Terms and Conditions</a>, <a href="<?= base_url('privacy_policy')?>">Privacy Policy</a> and <a href="<?= base_url('cookie_policy')?>">Cookie Policy</a>.</p>
			<input type="image" src=<?= base_url('img/Register.png') ?> alt="Register">
		</div>
		<?= form_close()?>
	</div>
	
	<br />
	<br />
	<br />
	
	<hr>
	
	<footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>

</body>
</html>

