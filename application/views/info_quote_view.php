<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>Quote Info</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	
	<?php 
	foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php endforeach; ?>
	<?php foreach($js_files as $file): ?>
		<script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>

<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
				<div class="nav-collapse collapse">
					<li class="brand" href="#"></li>
					<ul class="nav">
						<li class="home"><a href=<?= base_url('buyer') ?>>Home</a></li>
					</ul>
				</div>
				
				<form class="navbar-form pull-right">
					<ul class="nav">
						<li><a onclick="history.go(-1);">Go Back</a></li>
						<li><a href=<?= base_url('buyer/profile/'.$row->company_name) ?>>Profile</a></li>
						<li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
					</ul>
				</form>
			</div>
		</div>
    </div>
	
	<div class="container">
		<div class="logo-register">
			<a href=<?= base_url('buyer') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
		</div>
		
		<div class="row">
			<h2 class="titulo" >Check Quotes</h2>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="text-align:center;">Product</th>
						<th style="text-align:center;">Qty</th>
						<th style="text-align:center;">Units</th>
						<th style="text-align:center;">Pharmacopeia</th>
                        <th style="text-align:center;">Incoterm</th>
                        <th style="text-align:center;">Port</th>
						<th style="text-align:center;">Notes From Buyer</th>
					</tr>
				</thead>
				<tbody>
					<tr class="info">
						<td style="text-align:center;"><?php echo $name ?></td>
						<td style="text-align:center;"><?php echo $quantity ?></td>
						<td style="text-align:center;"><?php echo $unit ?></td>
						<td style="text-align:center;"><?php echo $pharmacopeia ?></td>
						<td style="text-align:center;"><?php echo $incoterm ?></td>
						<td style="text-align:center;"><?php echo $port ?></td>
						<td style="text-align:center;max-width: 500px;word-wrap: break-word;"><?php echo $notes_buyer ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="row">
			<div class="span12">
				<?php echo $output; ?>
			</div>
		</div>
		
	</div>
	
	<br />
	<br />
	<br />
	
	<hr>
	
	<footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>
	
</body>
</html>