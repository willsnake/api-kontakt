<?php
$card_number = array(
    'type' => 'text',
    'class' => 'span4',
    'name' => 'card_number_txt',
    'required' => 'required',
);

$month = array(
    "01" => "01",
    "02" => "02",
    "03" => "03",
    "04" => "04",
    "05" => "05",
    "06" => "06",
    "07" => "07",
    "08" => "08",
    "09" => "09",
    "10" => "10",
    "11" => "11",
    "12" => "12"
);

$year = array(
    "14" => "14",
    "15" => "15",
    "16" => "16",
    "17" => "17",
    "18" => "18",
    "19" => "19",
    "20" => "20",
    "21" => "21",
    "22" => "22",
    "23" => "23",
    "24" => "24",
    "25" => "25"
);

$cvv = array(
    'type' => 'text',
    'class' => 'span4',
    'name' => 'cvv_txt',
    'required' => 'required',
);

$attributes = array(
    'class' => 'control-label',
);

$atributos = 'class = "dropdown"';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url() ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a href=<?= base_url('main/login') ?>>Login</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url() ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>

    <div class="row"">
        <div class="span3">
        </div>
        <div class="span5" style="text-align: center;">
            <img  src="<?= base_url('img/Membership_BOX.png') ?>" alt="Payment">
            <br />
            <br />
            <br />

            <label class="radio" style="text-align: justify;">
                <input type="radio" id="radio_visa_mastercard" name="optionPayment" value="visa_mastercard"> <img src="<?= base_url('img/Payment_VISA_MASTERCARD.png') ?>" alt="Visa Mastercard">
            </label>

            <form id="visa_mastercard" action="<?= base_url('main/result')?>" method="post" target="_top" style="text-align: left">
                <input type="hidden" name="key" value="<?= $key ?>">

                <?= validation_errors('<div id="report-error" class="report-div error" style="display: block;"><p>', '</p></div>'); ?>
                <div>
                    <label for="fname_txt">First Name:</label>
                    <input type="text" name="fname_txt" placeholder="First Name" title="First Name" value="<?= set_value('fname_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="lname_txt">Last Name:</label>
                    <input type="text" name="lname_txt" placeholder="Last Name" title="Last Name" value="<?= set_value('lname_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="company_txt">Company:</label>
                    <input type="text" name="company_txt" placeholder="Company" title="Company" value="<?= set_value('company_txt') ?>" autofocus />
                </div>
                <div>
                    <label for="email_txt">Email address:</label>
                    <input type="email" name="email_txt" placeholder="Email address" title="Email address" value="<?= set_value('email_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="address_txt">Address:</label>
                    <input type="text" name="address_txt" placeholder="Address" title="Address" value="<?= set_value('address_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="country_txt">Country:</label>
                    <input type="text" name="country_txt" placeholder="Country" title="Country" value="<?= set_value('country_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="city_txt">City:</label>
                    <input type="text" name="city_txt" placeholder="City" title="City" value="<?= set_value('city_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="state_txt">State / Province:</label>
                    <input type="text" name="state_txt" placeholder="State / Province" title="State / Province" value="<?= set_value('state_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="zip_txt">ZIP / Postal Code:</label>
                    <input type="text" name="zip_txt" placeholder="ZIP / Postal Code" title="ZIP / Postal Code" value="<?= set_value('zip_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="telephone_txt">Telephone Number:</label>
                    <input type="text" name="telephone_txt" pattern="\d+" placeholder="Telephone" title="Telephone" value="<?= set_value('telephone_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="tax_txt">Tax / VAT Number:</label>
                    <input type="text" name="tax_txt" placeholder="Tax / VAT Number" title="Tax / VAT Number" value="<?= set_value('tax_txt') ?>" autofocus required />
                </div>

                <hr>

                <div>
                    <label for="card_number_txt">Credit Card Number:</label>
                    <input type="text" name="card_number_txt" pattern="\d+" placeholder="Credit Card Number" title="Credit Card Number" value="<?= set_value('card_number_txt') ?>" autofocus required />
                </div>
                <div>
                    <label for="month_txt">Expiration date:</label>
                    <select name="month_txt" style="width: 50px;">
                        <option value="01" <?= set_select('month_txt','01') ?>>01</option>
                        <option value="02" <?= set_select('month_txt','02') ?>>02</option>
                        <option value="03" <?= set_select('month_txt','03') ?>>03</option>
                        <option value="04" <?= set_select('month_txt','04') ?>>04</option>
                        <option value="05" <?= set_select('month_txt','05') ?>>05</option>
                        <option value="06" <?= set_select('month_txt','06') ?>>06</option>
                        <option value="07" <?= set_select('month_txt','07') ?>>07</option>
                        <option value="08" <?= set_select('month_txt','08') ?>>08</option>
                        <option value="09" <?= set_select('month_txt','09') ?>>09</option>
                        <option value="10" <?= set_select('month_txt','10') ?>>10</option>
                        <option value="11" <?= set_select('month_txt','11') ?>>11</option>
                        <option value="12" <?= set_select('month_txt','12') ?>>12</option>
                    </select>
                    /
                    <select name="year_txt" style="width: 50px;">
                        <option value="14" <?= set_select('year_txt','14') ?>>14</option>
                        <option value="15" <?= set_select('year_txt','15') ?>>15</option>
                        <option value="16" <?= set_select('year_txt','16') ?>>16</option>
                        <option value="17" <?= set_select('year_txt','17') ?>>17</option>
                        <option value="18" <?= set_select('year_txt','18') ?>>18</option>
                        <option value="19" <?= set_select('year_txt','19') ?>>19</option>
                        <option value="20" <?= set_select('year_txt','20') ?>>20</option>
                        <option value="21" <?= set_select('year_txt','21') ?>>21</option>
                        <option value="22" <?= set_select('year_txt','22') ?>>22</option>
                        <option value="23" <?= set_select('year_txt','23') ?>>23</option>
                        <option value="24" <?= set_select('year_txt','24') ?>>24</option>
                        <option value="25" <?= set_select('year_txt','25') ?>>25</option>
                    </select>
                </div>
                <div>
                    <label for="cvv_txt">CVV (Security Code)</label>
                    <input type="text" name="cvv_txt" max="5" pattern="\d+" placeholder="CVV" title="CVV" style="width: 90px;" value="<?= set_value('cvv_txt') ?>" autofocus required />
                </div>
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="Visa - Mastercard">
            </form>

            <label class="radio" style="text-align: justify;">
                <input type="radio" id="radio_paypal" name="optionPayment" value="visa_mastercard"> <img  src="<?= base_url('img/Payment_PAYPAL.png') ?>" alt="Paypal">
            </label>

            <form id="boton_paypal" action="<?= base_url('main/payment_paypal')?>" method="post" target="_top">
                <input type="hidden" name="key" value="<?= $key ?>">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            </form>

        </div>
        <div class="span4">
        </div>
    </div>

    </div>
</div>

<br />
<br />
<br />

<hr>

<footer style="text-align:center;">
    <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
</footer>

<script src=<?= base_url('js/jquery.min.js') ?>></script>
<script src=<?= base_url('js/efectos.js') ?>></script>

</body>
</html>