<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>API KONTAKT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="API,Excipient,Intermediate,Pharma,Pharmaceutical,Laboratory,GMP,EDQM,CEP,COS,Written Confirmation,DMF,Pharmacopeia,CAS Number,FDA,COFEPRIS,TGA,ANVISA,CPHI,Chemical Info">
    <meta name="description" content="Pharmaceutical network and database connecting suppliers and buyers all around the world">
    <meta name="author" content="API Kontakt">

    <link href="<?= base_url('css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/bootstrap-responsive.css') ?>" rel="stylesheet">
	<link href="<?= base_url('css/estilos.css') ?>" rel="stylesheet">

	<script src="http://api.html5media.info/1.1.6/html5media.min.js"></script>
	
</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
	<div class="container-fluid" style="text-align:center;">
		<img src="<?= base_url('img/Apikontakt_inicio.png') ?>" id="logo_inicial" />
		
		<br />
		<br />
		
		<div class="row-fluid">
			<div class="span12">
				<video width="384" height="288" controls>
					<source src="<?= base_url('video/APIKONTAKT-2.mp4') ?>" type="video/mp4">
					<source src="<?= base_url('video/APIKONTAKT-2.ogv') ?>" type="video/ogg">
					<source src="<?= base_url('video/APIKONTAKT-2.webm') ?>" type="video/webm">
					Your browser does not support the video tag.
				</video>
				<br />
				<br />
				<img src=<?= base_url('img/Login_or_register.png') ?> usemap="#loginmap">
			</div>
		
			<div class="span12">
				<br />
				<br />
				
			</div>
		</div>
	</div>
	
	<hr>
	
	<footer>
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>
	
	<map name="loginmap">
		<area shape="rect" coords="0,0,189,60" alt="Login" href=<?= base_url('main/login')?>>
		<area shape="rect" coords="190,0,375,60" alt="Register" href=<?= base_url('main/register')?>>
	</map>

    <script src=<?= base_url('js/jquery.min.js') ?>></script>

</body>
</html>