<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>About us</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url('supplier') ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a onclick="history.go(-1);">Go Back</a></li>
                    <li><a href=<?= base_url('supplier/profile/'.$row->company_name) ?>>Profile</a></li>
                    <li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url('supplier') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li class="active"><a href="<?= base_url('about') ?>">About us</a></li>
                    <li><a href="<?= base_url('kontakt') ?>">Kontakt us</a></li>
                    <li><a href="<?= base_url('browser') ?>">Browser Requirements</a></li>
                    <li><a href="<?= base_url('terms') ?>">Terms and Conditions</a></li>
                    <li><a href="<?= base_url('privacy_policy') ?>">Privacy Policy</a></li>
                    <li><a href="<?= base_url('cookie_policy') ?>">Cookie Policy</a></li>
                    <li><a href="<?= base_url('faq') ?>">FAQ</a></li>
                </ul>
            </div>
        </div>
        <div class="span9">
            <div class="row-fluid">
                <h1>About us</h1>
                <h3>Our mission:</h3>
                <p>Serve as a link between worldwide API, intermediates and excipients manufacturers and pharmaceutical companies. Offering relevant data in an innovative way allowing our customers to take quick and effective decisions.</p>
                <h3>Company:</h3>
                <p>API Kontakt started out in Mexico City in 2013, and it officially launched on 2014.</p>
                <p>The management team is made up of skilled executives from the pharmaceutical industry.</p>
                <h3>Products:</h3>
                <p>Tools for buyer</p>
                <ul>
                    <li>Finder – search products or suppliers / kontakt them / request for quotes.</li>
                    <li>QuoteManager – track requested quotes.</li>
                </ul>
                <p>Tools for supplier</p>
                <ul>
                    <li>MyProducts – manage your product list with its specifications.</li>
                    <li>QuoteManager – Receive and respond to quote requests.</li>
                </ul>
            </div>
        </div>
    </div>

    <hr>

    <footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
    </footer>

    <script src=<?= base_url('js/jquery.min.js') ?>></script>

</body>
</html>