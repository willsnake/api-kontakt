<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Send Quote</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/flexigrid.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/datePicker.css') ?> rel="stylesheet">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href="<?= base_url('buyer') ?>">Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a onclick="history.go(-1);">Go Back</a></li>
                    <li><a href="<?= base_url('buyer/profile/'.$row->company_name) ?>">Profile</a></li>
                    <li><a href="<?= base_url('main/logout') ?>">Logout</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
<div class="logo-register">
    <a href="<?= base_url('buyer') ?>"><img  src="<?= base_url('img/Apikontakt_iza.png') ?>"></a>
</div>

<div class="row">
<?= form_open("buyer/send_supplier_quote",$atributos = array('class' => 'form-horizontal'))?>
<h1 class="titulo" style="text-align:center;"><?= $c_name ?></h1>
<input type="hidden" value=<?= "\"$c_name\" \ " ?> name="c_name_txt" />
<input type="hidden" value=<?= "\"$supplier\" \ " ?> name="usuario" />
<?php
$name = array(
    'type' => 'text',
    'class' => 'span8',
    'name' => 'name',
    'value' => $p_name,
    'placeholder' => 'Product Name',
    'title' => 'Product Name',
    'readonly' => 'readonly'
);

$pharmacopeia = array(
    'type' => 'text',
    'class' => 'span8',
    'name' => 'pharmacopeia',
    'placeholder' => 'Pharmacopeia',
    'title' => 'Pharmacopeia',
    'value' => $pharmacopeia
);

$quantity = array(
    'type' => 'text',
    'class' => 'span3',
    'name' => 'quantity',
    'placeholder' => 'Quantity',
    'title' => 'Quantity',
    'value' => $quantity,
);

$incoterm = array(
    'type' => 'text',
    'class' => 'span8',
    'name' => 'incoterm',
    'placeholder' => 'Incoterm',
    'title' => 'Incoterm',
    'value' => $incoterm,
);

$port = array(
    'type' => 'text',
    'class' => 'span8',
    'name' => 'port',
    'placeholder' => 'Port',
    'title' => 'Port',
    'value' => $port,
);

$limit = array(
    'type' => 'text',
    'class' => 'span8',
    'id' => 'datepicker',
    'name' => 'limit',
    'placeholder' => 'Limit to Quote',
    'title' => 'Limit to Quote',
    'value' => $due_date,
);

$notes = array(
    'type' => 'textarea',
    'rows' => '3',
    'name' => 'notes',
    'class' => 'input-xxlarge',
    'title' => 'Notes',
    'onKeyDown' => 'limitText(this.form.notes,this.form.countdown,247);',
    'onKeyUp' => 'limitText(this.form.notes,this.form.countdown,247);',
    'value' => $notes_buyer
);

// etiquetas

$attributes = array(
    'class' => 'control-label',
);
?>
<div id="report-error" class="report-div error" style="display: block;">
    <?= validation_errors() ?>
</div>
<div class="control-group">
    <?= form_label('Product Name:* ','name',$attributes) ?>
    <div class="controls">
        <?= form_input($name) ?>
    </div>
</div>
<div class="control-group">
    <?= form_label('Pharmacopeia:* ','pharmacopeia',$attributes) ?>
    <div class="controls">
        <?= form_input($pharmacopeia) ?>
    </div>
</div>
<div class="control-group">
    <?= form_label('Quantity:* ','quantity',$attributes) ?>
    <div class="controls controls-row">
        <?= form_input($quantity) ?>
        <?= form_label('Unit:* ','unit',$attributes) ?>
        <select class="span2" name="unit">
            <option value="mg">mg - milligram</option>
            <option value="g">g - gram</option>
            <option value="kg">kg – kilogram</option>
            <option value="t">t  - ton</option>
        </select>
    </div>
</div>
<div class="control-group">
    <?= form_label('Incoterm:* ','incoterm',$attributes) ?>
    <div class="controls">
        <?= form_input($incoterm) ?>
    </div>
</div>
<div class="control-group">
    <?= form_label('Port:* ','port',$attributes) ?>
    <div class="controls">
        <?= form_input($port) ?>
    </div>
</div>
<div class="control-group">
    <?= form_label('Limit to quote:* ','limit',$attributes) ?>
    <div class="controls">
        <?= form_input($limit) ?>
    </div>
</div>
<div class="control-group">
    <?= form_label('Notes: ','notes',$attributes) ?>
    <div class="controls">
        <?= form_textarea($notes) ?>
        <p>You can write up to 247 characters.</p>
    </div>
</div>
</div>
<div class="row" style="text-align:center;">
    <p>* Mandatory fields</p>
    <input type="image" src=<?= base_url('img/SendQuote.png') ?> alt="Send Quote">
</div>
<?= form_close()?>

</div>

<br />
<br />
<br />

<hr>

<footer style="text-align:center;">
    <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
</footer>

<script>
    $(function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker( "option", "dateFormat", "MM dth yy" );
        $( "#datepicker" ).datepicker( "option", "minDate", 0 );
    });
</script>

<script>
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }
</script>
</body>
</html>