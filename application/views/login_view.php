<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login Registro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css')?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css')?> rel="stylesheet">
      <link href=<?= base_url('css/flexigrid.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>
  <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-48042101-1', 'apikontakt.com');
      ga('send', 'pageview');
  </script>
  <body>
    <div class="container">
		<?= form_open("main/validar_login",$atributos = array('class' => 'form-signin' ))?>
		<h2 class="form-signin-heading">Login</h2>
		<?php
			$usuario = array(
				'type' => 'text',
				'class' => 'input-block-level',
				'name' => 'user',
				'placeholder' => 'User',
				'title' => 'Your User',
			);

			$password = array(
				'type' => 'password',
				'class' => 'input-block-level',
				'name' => 'password',
				'placeholder' => 'Password',
				'title' => 'Your password',
                "autocomplete" => "off"
			);
		?>
        <?= validation_errors() ?>
		<?= form_label('User / Email address: ','user') ?>
		<?= form_input($usuario) ?>
		<?= form_label('Password: ','password') ?>
		<?= form_input($password) ?>
		<button class="btn btn-large btn-primary" type="submit">Log in</button>
		<h5><a href=<?= base_url('main/forgotPassword')?>>Forgot password?</a></h5>
		<?= form_close()?>
	</div>
  </body>
</html>