<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>Forgot your password?</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>

<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
				<div class="nav-collapse collapse">
					<li class="brand" href="#"></li>
					<ul class="nav">
						<li class="home"><a href=<?= base_url() ?>>Home</a></li>
					</ul>
				</div>
				
				<form class="navbar-form pull-right">
					<ul class="nav">			
						<li><a href=<?= base_url('main/login') ?>>Login</a></li>
					</ul>
				</form>
			</div>
		</div>
    </div>
	<div class="container">
		<div class="logo-register">
			<a href=<?= base_url() ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
		</div>
		
		<div class="row" style="text-align:center;">
			<h3>Email address:</h3>
			<p>An email will be sent to your account, so you can reset your password</p>
			<?= form_open("main/sendPassword")?>
			<?php
				$emailpass = array(
					'type' => 'email',
					'class' => 'span4',
					'name' => 'emailpass',
					'placeholder' => 'Email address',
					'title' => 'Your email address',
					'required' => 'required',
					'autofocus' => 'autofocus'
				);
			?>
			<?= validation_errors() ?>
			<?= form_label('Email address: ','emailpass') ?>
			<?= form_input($emailpass) ?>
			
			<br />
			
			<button class="btn btn-large btn-primary" type="submit">Send email</button>
			<?= form_close()?>
		</div>

    </div>
	
	<br />
	<br />
	<br />
	
	<hr>
	
	<footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>
	
    <script src=<?= base_url('js/jquery.min.js') ?>></script>

</body>
</html>