<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Privacy Policy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url('supplier') ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a onclick="history.go(-1);">Go Back</a></li>
                    <li><a href=<?= base_url('supplier/profile/'.$row->company_name) ?>>Profile</a></li>
                    <li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url('supplier') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li><a href="<?= base_url('about') ?>">About us</a></li>
                    <li><a href="<?= base_url('kontakt') ?>">Kontakt us</a></li>
                    <li><a href="<?= base_url('browser') ?>">Browser Requirements</a></li>
                    <li><a href="<?= base_url('terms') ?>">Terms and Conditions</a></li>
                    <li class="active"><a href="<?= base_url('privacy_policy') ?>">Privacy Policy</a></li>
                    <li><a href="<?= base_url('cookie_policy') ?>">Cookie Policy</a></li>
                    <li><a href="<?= base_url('faq') ?>">FAQ</a></li>
                </ul>
            </div>
        </div>
        <div class="span9">
            <div class="row-fluid" style="text-align: left;">
                <h1 style="text-align: center">PRIVACY POLICY</h1>
                <p>API KONTAKT, S. de R.L. de C.V. (hereinafter “APIKONTAKT”), with an address at Lago Alberto 320, Edificio A5, interior A-203, Colonia Anáhuac, Delegación Miguel Hidalgo, México, Distrito Federal, C.P.11320, informs you that the personal data requested from you will be used exclusively for the execution of activities in connection to the rendering of our services or the sale of our products; also, such services may be used for the following purposes:</p>
                <ol type="a">
                    <li>Attending your inquiries, doubts, comments and suggestions;</li>
                    <li>E-mailing news, sales or information in connection to the services or products requested from APIKONTAKT; and</li>
                    <li>Drafting statistics of visits and preferences.</li>
                </ol>
                <p>In addition to the information provided to us, APIKONTAKT may also collect information such as the IP address, type of navigator used by the User, the domain name and, specific pages within the Site through which you have had access, as well as the duration of the interaction in the APIKONTAKT webpage, cookies and web beacons; the foregoing in order to obtain behavior statistics in the navigation of the visitors of our internet site. The personal information provided to us is stored in controlled databases with limited access.</p>
                <p>APIKONTAKT treats your personal data under the principles of lawfulness, consent, information, quality, purpose, loyalty, proportionally and responsibility. None of the data provided by you or collected by APIKONTAKT are sensitive personal data.</p>
                <p>Please be informed that, in order to impede unauthorized access and disclosure, as well as maintain the accuracy of the data provided to us and guarantee the proper use of your information, we use appropriate physical, technological and administrative procedures to protect the information retrieved by us.</p>
                <p>Also, please be informed that you may exercise your rights of Access, Rectification, Cancellation and/or Opposition by means of a form and procedure which may be obtained by filing a request to the following e-mail address: <?= safe_mailto('support@apikontakt.com', 'support@apikontakt.com') ?>, or consult it directly on the following website: <a href="<?= base_url('arcorights') ?>">http://www.apikontakt.com/arcorights</a></p>
                <p>APIKONTAKT may transfer your personal data only to subsidiaries, affiliates, controlled or controlling entities of APIKONTAKT, as well as to third parties with whom it is necessary for the operation of the APIKONTAKT Service. APIKONTAKT will not sell, lease or rent your personal data to any person or entity different from the foregoing.</p>
                <p>In the event of a security breach on the treatment of your personal data, APIKONTAKT, in its capacity of Responsible, will inform you about such situation immediately via e-mail, in order for you to take the necessary measures for the defense of your rights.</p>
                <p>APIKONTAKT reserves its right to modify or update from time to time this privacy statement, which can be consulted in our webpage at: <a href="<?= base_url() ?>">www.apikontakt.com</a></p>
            </div>
        </div>
    </div>

    <hr>

    <footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
    </footer>

    <script src=<?= base_url('js/jquery.min.js') ?>></script>

</body>
</html>