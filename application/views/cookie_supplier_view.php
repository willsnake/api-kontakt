<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Cookie Policy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url('supplier') ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a onclick="history.go(-1);">Go Back</a></li>
                    <li><a href=<?= base_url('supplier/profile/'.$row->company_name) ?>>Profile</a></li>
                    <li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url('supplier') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li><a href="<?= base_url('about') ?>">About us</a></li>
                    <li><a href="<?= base_url('kontakt') ?>">Kontakt us</a></li>
                    <li><a href="<?= base_url('browser') ?>">Browser Requirements</a></li>
                    <li><a href="<?= base_url('terms') ?>">Terms and Conditions</a></li>
                    <li><a href="<?= base_url('privacy_policy') ?>">Privacy Policy</a></li>
                    <li class="active"><a href="<?= base_url('cookie_policy') ?>">Cookie Policy</a></li>
                </ul>
            </div>
        </div>
        <div class="span9">
            <div class="row-fluid" style="text-align: left;">
                <h1 style="text-align: center">COOKIES POLICY</h1>
                <p>Cookies may be used in some of the pages in our site. “Cookies” are small text files stored in your hard disk which help us to offer a more personalized experience in our website. For example, a cookie may be used to store registration information in an area of the site in order for the user not to include it a second time during subsequent visits to such area. It is a policy of API KONTAKT, S. de R.L. de C.V. to use cookies in order to simplify the navigation within our website and the registration procedures.</p>
                <p>If you are worried about cookies, most of the search engines allow people to reject cookies. In most cases, a visitor may reject a cookie and continue navigating throughout our website.</p>
                <p>In order to administrate our website properly, we may include, anonymously, information on our operative systems and identify the categories of our visitors by aspects such as the type of domains and search engines. These statistics are entirely reported to the web administrators. This is executed in order to guarantee that our website offers the best experience to our visitors, and for it to be an effective source of information.</p>
            </div>
        </div>
    </div>

    <hr>

    <footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
    </footer>

    <script src=<?= base_url('js/jquery.min.js') ?>></script>

</body>
</html>