<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Privacy Policy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
    <link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <li class="brand" href="#"></li>
                <ul class="nav">
                    <li class="home"><a href=<?= base_url('buyer') ?>>Home</a></li>
                </ul>
            </div>

            <form class="navbar-form pull-right">
                <ul class="nav">
                    <li><a onclick="history.go(-1);">Go Back</a></li>
                    <li><a href=<?= base_url('buyer/profile/'.$row->company_name) ?>>Profile</a></li>
                    <li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
                </ul>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="logo-register">
        <a href=<?= base_url('buyer') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
    </div>
    <div class="row-fluid">
        <!--<div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li><a href="<?/*= base_url('about') */?>">About us</a></li>
                    <li><a href="<?/*= base_url('kontakt') */?>">Kontakt us</a></li>
                    <li><a href="<?/*= base_url('browser') */?>">Browser Requirements</a></li>
                    <li><a href="<?/*= base_url('terms') */?>">Terms and Conditions</a></li>
                    <li class="active"><a href="<?/*= base_url('privacy_policy') */?>">Privacy Policy</a></li>
                    <li><a href="<?/*= base_url('cookie_policy') */?>">Cookie Policy</a></li>
                </ul>
            </div>
        </div>-->
        <div class="span12">
            <div class="row-fluid" style="text-align: justify;">
                <h1 style="text-align: center">“ARCO” RIGHTS FORM</h1>
                <p>In order to comply with the provisions of the Federal Act for the Protection of Personal Data in Possession of Particulars in force in the Mexican United States, as well as the relevant Regulations thereof (hereinafter collectively the “Act”), API KONTAKT, S. DE R.L. DE C.V. having its offices in Lago Alberto 320, Edificio A5, interior A-203, Colonia Anáhuac, Delegación Miguel Hidalgo, Mexico, Federal District, C.P.11320, issues and makes available for all legal purposes, the form contained herein for the exercising of your rights to Access, Rectification, Cancellation and/or Opposition (“ARCO” rights).</p>
                <p>This form must be exclusively filled by the holder of the personal data or, as the case may be, by the person who evidences its capacity as legal representative.</p>
                <p>It is of the essence to provide all of the information requested herein, in order to verify your identity and give prompt assistance to your request.</p>
                <p>
                    HOLDER’S INFORMATION:
                    <br />
                    Name
                    <br />
                    Father’s Last Name: _________________________________________________
                    <br />
                    Mother’s Last Name: _________________________________________________
                    <br />
                    Name (s): _________________________________________________________
                    <br />
                    Address
                    <br />
                    Street: ____________________________________________________________
                    <br />
                    Number: __________________________ Suite: ___________________________
                    <br />
                    Locality or Municipality: _______________________________________________
                    <br />
                    State: _____________________________ Postal code: ____________________
                    <br />
                    Mobile phone (10 digits) ______________________________________________
                    <br />
                    Telephone (area code):_______________________________________________
                    <br />
                    Date of birth (DD/MM/YYYY): ______________ RFC (tax ID): ________________
                </p>
                <br />
                <p>
                    INFORMATION OF THE LEGAL REPRESENTATIVE
                    <br />
                    Name
                    <br />
                    Father’s Last Name: _________________________________________________
                    <br />
                    Mother’s Last Name: _________________________________________________
                    <br />
                    Name (s): _________________________________________________________
                    <br />
                    Address
                    <br />
                    Street: ____________________________________________________________
                    <br />
                    Number: __________________________ Suite: ___________________________
                    <br />
                    Locality or Municipality: _______________________________________________
                    <br />
                    State: _____________________________ Postal code: ____________________
                    <br />
                    Mobile phone (10 digits) ______________________________________________
                    <br />
                    Telephone (area code):_______________________________________________
                    <br />
                    <i>Note: It is compulsory to attach the official ID of the holder of the right(s), as well as the documents evidencing its capacity as legal representative of the holder of the right(s).</i>
                </p>
                <br />
                <p>
                    TYPE OF RIGHT TO BE EXERCISED (mark the accurate option with an “X”).
                    <br />
                    Access: _______		Cancellation: _______		Rectification: _______		Opposition: _______		Revoking: _______
                    <br />
                    Please provide us an e-mail in order to respond your request in connection to your rights to Access, Rectification, Cancellation and/or Opposition: ________________________.
                    <br />
                    Additionally, we would appreciate if you clearly and accurately described the personal data in regards with which you are requesting the exercising of your right: ___________________________________________________________________________________________________________________________.
                    <br />
                    <br />
                    Documents attached hereto by means of an electronic copy (mark with an “X”)
                    <br />
                    Official ID in force: __________		Evidence of the address: __________		Notarized proxy (in case of a legal representative): __________
                    <br />
                    The personal data granted hereby will only serve the purpose of verifying the identity of the holder or, as the case may be, the legal representative, to safeguard the security and protection thereof.
                </p>
            </div>
        </div>
    </div>
</div>

    <hr>

    <footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> apikontakt &copy; 2013</p>
    </footer>

    <script src=<?= base_url('js/jquery.min.js') ?>></script>

</body>
</html>