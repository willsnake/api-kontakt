<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
$sql2 = "SELECT user_name FROM Usuario WHERE id_datos_usuario = ?";
$result2 = $this->db->query($sql2, array($cookies['usuario']));
$row2 = $result2->row();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title><?= $row->company_name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">
	
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>	

</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<script>
    $(document).ready(function() {
        var country = "<?php echo($row->country) ?>";
        var prefix = "<?php echo($row->prefix) ?>";

        $("#countrySelector").val(country);
        $("#prefixSelector").val(prefix);
    });
</script>
<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
				<div class="nav-collapse collapse">
					<li class="brand" href="#"></li>
					<ul class="nav">
						<li class="home"><a href=<?= base_url('buyer') ?>>Home</a></li>
					</ul>
				</div>
				
				<form class="navbar-form pull-right">
					<ul class="nav">
						<li><a onclick="history.go(-1);">Go Back</a></li>
						<li><a href=<?= base_url('buyer/profile/'.$row->company_name) ?>>Profile</a></li>
						<li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
					</ul>
				</form>
			</div>
		</div>
    </div>
	
	<div class="container">
		<div class="logo-register">
			<a href=<?= base_url('buyer') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
		</div>
		
		<div class="row">
			<div class="span12">
				<h1 class="titulo">Update Profile Data</h1>
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<?= form_open("buyer/validate_update",$atributos = array('class' => 'form-horizontal' ))?>
            <?php
            $companyname = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'companyname_txt',
                'placeholder' => 'Company Name',
                'title' => 'Company Name',
                'value' => $row->company_name,
            );


            $street = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'street_txt',
                'placeholder' => 'Address',
                'title' => 'Address',
                'value' => $row->street_address,
            );

            $city = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'city_txt',
                'placeholder' => 'City',
                'title' => 'City',
                'value' => $row->city,
            );

            $zip = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'zip_txt',
                'placeholder' => 'ZIP Code',
                'title' => 'ZIP Code',
                'value' => $row->zip_postal,
            );

            $countrycode = array(
                'type' => 'text',
                'pattern' => '\d+',
                'class' => 'span2',
                'name' => 'countrycode_txt',
                'placeholder' => 'Country Code',
                'title' => 'Country Code',
                'value' => $row->country_code,
            );

            $areacode = array(
                'type' => 'text',
                'pattern' => '\d+',
                'class' => 'span2',
                'name' => 'areacode_txt',
                'placeholder' => 'Area Code',
                'title' => 'Area Code',
                'value' => $row->area_code,
            );

            $phone = array(
                'type' => 'text',
                'pattern' => '\d+',
                'class' => 'span2',
                'name' => 'phone_txt',
                'placeholder' => 'Phone Number',
                'title' => 'Phone Number',
                'value' => $row->phone,
            );

            $page = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'page_txt',
                'placeholder' => 'Web Page',
                'title' => 'Web Page',
                'value' => $row->web_page,
            );

            $fname = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'fname_txt',
                'placeholder' => 'First Name',
                'title' => 'First Name',
                'value' => $row->first_name,
            );

            $lname = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'lname_txt',
                'placeholder' => 'Last Name',
                'title' => 'Last Name',
                'value' => $row->last_name,
            );

            $position = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'position_txt',
                'placeholder' => 'Job Position',
                'title' => 'Job Position',
                'value' => $row->job_position,
            );

            $email = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'email_txt',
                'placeholder' => 'Your email',
                'title' => 'Your email',
                'value' => $row->email,
            );

            $cemail = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'cemail_txt',
                'placeholder' => 'Confirm your email',
                'title' => 'Confirm your email',
            );

            $user = array(
                'type' => 'text',
                'class' => 'span8',
                'name' => 'user_txt',
                'placeholder' => 'User Name',
                'title' => 'User Name',
                'value' => $row2->user_name,
            );

            $password = array(
                'type' => 'password',
                'class' => 'span8',
                'name' => 'password_txt',
                'placeholder' => 'Password',
                'title' => 'Password',
            );

            $cpassword = array(
                'type' => 'password',
                'class' => 'span8',
                'name' => 'cpassword_txt',
                'placeholder' => 'Confirm Password',
                'title' => 'Confirm Password',
            );

            // etiquetas

            $attributes = array(
                'class' => 'control-label',
            );

			?>

            <?= validation_errors('<div id="report-error" class="report-div error" style="display: block;"><p>', '</p></div>'); ?>
            <div class="control-group">
                <?= form_label('Company Name:* ','companyname_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($companyname) ?>
                </div>
            </div>
            <div class="control-group">
                <label for="country_txt" class="control-label">Country:* </label>
                <div class="controls">
                    <select name="country_txt" id="countrySelector" class="dropdown">
                        <option value="Afghanistan" <?= set_select($row->country,'Afghanistan') ?>>Afghanistan</option>
                        <option value="Albania" <?= set_select($row->country,'Albania') ?>>Albania</option>
                        <option value="Algeria" <?= set_select($row->country,'Algeria') ?>>Algeria</option>
                        <option value="American Samoa" <?= set_select($row->country,'American Samoa') ?>>American Samoa</option>
                        <option value="Andorra" <?= set_select($row->country,'Andorra') ?>>Andorra</option>
                        <option value="Angola" <?= set_select($row->country,'Angola') ?>>Angola</option>
                        <option value="Anguilla" <?= set_select($row->country,'Anguilla') ?>>Anguilla</option>
                        <option value="Antarctica" <?= set_select($row->country,'Antarctica') ?>>Antarctica</option>
                        <option value="Antigua and Barbuda" <?= set_select($row->country,'Antigua and Barbuda') ?>>Antigua and Barbuda</option>
                        <option value="Argentina" <?= set_select($row->country,'Argentina') ?>>Argentina</option>
                        <option value="Armenia" <?= set_select($row->country,'Armenia') ?>>Armenia</option>
                        <option value="Aruba" <?= set_select($row->country,'Aruba') ?>>Aruba</option>
                        <option value="Australia" <?= set_select($row->country,'Australia') ?>>Australia</option>
                        <option value="Austria" <?= set_select($row->country,'Austria') ?>>Austria</option>
                        <option value="Azerbaijan" <?= set_select($row->country,'Azerbaijan') ?>>Azerbaijan</option>
                        <option value="Bahamas" <?= set_select($row->country,'Bahamas') ?>>Bahamas</option>
                        <option value="Bahrain" <?= set_select($row->country,'Bahrain') ?>>Bahrain</option>
                        <option value="Bangladesh" <?= set_select($row->country,'Bangladesh') ?>>Bangladesh</option>
                        <option value="Barbados" <?= set_select($row->country,'Barbados') ?>>Barbados</option>
                        <option value="Belarus" <?= set_select($row->country,'Belarus') ?>>Belarus</option>
                        <option value="Belgium" <?= set_select($row->country,'Belgium') ?>>Belgium</option>
                        <option value="Belize" <?= set_select($row->country,'Belize') ?>>Belize</option>
                        <option value="Benin" <?= set_select($row->country,'Benin') ?>>Benin</option>
                        <option value="Bermuda" <?= set_select($row->country,'Bermuda') ?>>Bermuda</option>
                        <option value="Bhutan" <?= set_select($row->country,'Bhutan') ?>>Bhutan</option>
                        <option value="Bolivia" <?= set_select($row->country,'Bolivia') ?>>Bolivia</option>
                        <option value="Bosnia and Herzegovina" <?= set_select($row->country,'Bosnia and Herzegovina') ?>>Bosnia and Herzegovina</option>
                        <option value="Botswana" <?= set_select($row->country,'Botswana') ?>>Botswana</option>
                        <option value="Brazil" <?= set_select($row->country,'Brazil') ?>>Brazil</option>
                        <option value="Brunei Darussalam" <?= set_select($row->country,'Brunei Darussalam') ?>>Brunei Darussalam</option>
                        <option value="Bulgaria" <?= set_select($row->country,'Bulgaria') ?>>Bulgaria</option>
                        <option value="Burkina Faso" <?= set_select($row->country,'Burkina Faso') ?>>Burkina Faso</option>
                        <option value="Burundi" <?= set_select($row->country,'Burundi') ?>>Burundi</option>
                        <option value="Cambodia" <?= set_select($row->country,'Cambodia') ?>>Cambodia</option>
                        <option value="Cameroon" <?= set_select($row->country,'Cameroon') ?>>Cameroon</option>
                        <option value="Canada" <?= set_select($row->country,'Canada') ?>>Canada</option>
                        <option value="Cape Verde" <?= set_select($row->country,'Cape Verde') ?>>Cape Verde</option>
                        <option value="Cayman Islands" <?= set_select($row->country,'Cayman Islands') ?>>Cayman Islands</option>
                        <option value="Central African Republic" <?= set_select($row->country,'entral African Republic') ?>>Central African Republic</option>
                        <option value="Chad" <?= set_select($row->country,'Chad') ?>>Chad</option>
                        <option value="Chile" <?= set_select($row->country,'Chile') ?>>Chile</option>
                        <option value="China" <?= set_select($row->country,'China') ?>>China</option>
                        <option value="Christmas Island" <?= set_select($row->country,'Christmas Island') ?>>Christmas Island</option>
                        <option value="Cocos (Keeling) Islands" <?= set_select($row->country,'Cocos (Keeling) Islands') ?>>Cocos (Keeling) Islands</option>
                        <option value="Colombia" <?= set_select($row->country,'Colombia') ?>>Colombia</option>
                        <option value="Comoros" <?= set_select($row->country,'Comoros') ?>>Comoros</option>
                        <option value="Democratic Republic of the Congo (Kinshasa)" <?= set_select($row->country,'Democratic Republic of the Congo (Kinshasa)') ?>>Democratic Republic of the Congo (Kinshasa)</option>
                        <option value="Congo, Republic of (Brazzaville)" <?= set_select($row->country,'Congo, Republic of (Brazzaville)') ?>>Congo, Republic of (Brazzaville)</option>
                        <option value="Cook Islands" <?= set_select($row->country,'Cook Islands') ?>>Cook Islands</option>
                        <option value="Costa Rica" <?= set_select($row->country,'Costa Rica') ?>>Costa Rica</option>
                        <option value="Ivory Coast (Cote d'Ivoire)" <?= set_select($row->country,"Ivory Coast (Cote d'Ivoire)") ?>>Ivory Coast (Cote d'Ivoire)</option>
                        <option value="Croatia" <?= set_select($row->country,'Croatia') ?>>Croatia</option>
                        <option value="Cuba" <?= set_select($row->country,'Cuba') ?>>Cuba</option>
                        <option value="Cyprus" <?= set_select($row->country,'Cyprus') ?>>Cyprus</option>
                        <option value="Czech Republic" <?= set_select($row->country,'Czech Republic') ?>>Czech Republic</option>
                        <option value="Denmark" <?= set_select($row->country,'Denmark') ?>>Denmark</option>
                        <option value="Djibouti" <?= set_select($row->country,'Djibouti') ?>>Djibouti</option>
                        <option value="Dominica" <?= set_select($row->country,'Dominica') ?>>Dominica</option>
                        <option value="Dominican Republic" <?= set_select($row->country,'Dominican Republic') ?>>Dominican Republic</option>
                        <option value="East Timor Timor-Leste" <?= set_select($row->country,'East Timor Timor-Leste') ?>>East Timor Timor-Leste</option>
                        <option value="Ecuador" <?= set_select($row->country,'Ecuador') ?>>Ecuador</option>
                        <option value="Egypt" <?= set_select($row->country,'Egypt') ?>>Egypt</option>
                        <option value="El Salvador" <?= set_select($row->country,'El Salvador') ?>>El Salvador</option>
                        <option value="Equatorial Guinea" <?= set_select($row->country,'Equatorial Guinea') ?>>Equatorial Guinea</option>
                        <option value="Eritrea" <?= set_select($row->country,'Eritrea') ?>>Eritrea</option>
                        <option value="Estonia" <?= set_select($row->country,'Estonia') ?>>Estonia</option>
                        <option value="Ethiopia" <?= set_select($row->country,'Ethiopia') ?>>Ethiopia</option>
                        <option value="Falkland Islands" <?= set_select($row->country,'Falkland Islands') ?>>Falkland Islands</option>
                        <option value="Faroe Islands" <?= set_select($row->country,'Faroe Islands') ?>>Faroe Islands</option>
                        <option value="Fiji" <?= set_select($row->country,'Fiji') ?>>Fiji</option>
                        <option value="Finland" <?= set_select($row->country,'Finland') ?>>Finland</option>
                        <option value="France" <?= set_select($row->country,'France') ?>>France</option>
                        <option value="French Guiana" <?= set_select($row->country,'French Guiana') ?>>French Guiana</option>
                        <option value="French Polynesia" <?= set_select($row->country,'French Polynesia') ?>>French Polynesia</option>
                        <option value="French Southern Territories" <?= set_select($row->country,'French Southern Territories') ?>>French Southern Territories</option>
                        <option value="Gabon" <?= set_select($row->country,'Gabon') ?>>Gabon</option>
                        <option value="Gambia" <?= set_select($row->country,'Gambia') ?>>Gambia</option>
                        <option value="Georgia" <?= set_select($row->country,'Georgia') ?>>Georgia</option>
                        <option value="Germany" <?= set_select($row->country,'Germany') ?>>Germany</option>
                        <option value="Ghana" <?= set_select($row->country,'Ghana') ?>>Ghana</option>
                        <option value="Gibraltar" <?= set_select($row->country,'Gibraltar') ?>>Gibraltar</option>
                        <option value="Great Britain" <?= set_select($row->country,'Great Britain') ?>>Great Britain</option>
                        <option value="Greece" <?= set_select($row->country,'Greece') ?>>Greece</option>
                        <option value="Greenland" <?= set_select($row->country,'Greenland') ?>>Greenland</option>
                        <option value="Grenada" <?= set_select($row->country,'Grenada') ?>>Grenada</option>
                        <option value="Guadeloupe" <?= set_select($row->country,'Guadeloupe') ?>>Guadeloupe</option>
                        <option value="Guam" <?= set_select($row->country,'Guam') ?>>Guam</option>
                        <option value="Guatemala" <?= set_select($row->country,'Guatemala') ?>>Guatemala</option>
                        <option value="Guinea" <?= set_select($row->country,'Guinea') ?>>Guinea</option>
                        <option value="Guinea-Bissau" <?= set_select($row->country,'Guinea-Bissau') ?>>Guinea-Bissau</option>
                        <option value="Guyana" <?= set_select($row->country,'Guyana') ?>>Guyana</option>
                        <option value="Haiti" <?= set_select($row->country,'Haiti') ?>>Haiti</option>
                        <option value="Holy See" <?= set_select($row->country,'Holy See') ?>>Holy See</option>
                        <option value="Honduras" <?= set_select($row->country,'Honduras') ?>>Honduras</option>
                        <option value="Hong Kong" <?= set_select($row->country,'Hong Kong') ?>>Hong Kong</option>
                        <option value="Hungary" <?= set_select($row->country,'Hungary') ?>>Hungary</option>
                        <option value="Iceland" <?= set_select($row->country,'Iceland') ?>>Iceland</option>
                        <option value="India" <?= set_select($row->country,'India') ?>>India</option>
                        <option value="Indonesia" <?= set_select($row->country,'Indonesia') ?>>Indonesia</option>
                        <option value="Iran (Islamic Republic of)" <?= set_select($row->country,'Iran (Islamic Republic of)') ?>>Iran (Islamic Republic of)</option>
                        <option value="Iraq" <?= set_select($row->country,'Iraq') ?>>Iraq</option>
                        <option value="Ireland" <?= set_select($row->country,'Ireland') ?>>Ireland</option>
                        <option value="Israel" <?= set_select($row->country,'Israel') ?>>Israel</option>
                        <option value="Italy" <?= set_select($row->country,'Italy') ?>>Italy</option>
                        <option value="Jamaica" <?= set_select($row->country,'Jamaica') ?>>Jamaica</option>
                        <option value="Japan" <?= set_select($row->country,'Japan') ?>>Japan</option>
                        <option value="Jordan" <?= set_select($row->country,'Jordan') ?>>Jordan</option>
                        <option value="Kazakhstan" <?= set_select($row->country,'Kazakhstan') ?>>Kazakhstan</option>
                        <option value="Kenya" <?= set_select($row->country,'Kenya') ?>>Kenya</option>
                        <option value="Kiribati" <?= set_select($row->country,'Kiribati') ?>>Kiribati</option>
                        <option value="Korea, Democratic People's Rep. (North Korea)" <?= set_select($row->country,"Korea, Democratic People's Rep. (North Korea)") ?>>Korea, Democratic People's Rep. (North Korea)</option>
                        <option value="Korea, Republic of (South Korea)" <?= set_select($row->country,'Korea, Republic of (South Korea)') ?>>Korea, Republic of (South Korea)</option>
                        <option value="Kosovo" <?= set_select($row->country,'Kosovo') ?>>Kosovo</option>
                        <option value="Kuwait" <?= set_select($row->country,'Kuwait') ?>>Kuwait</option>
                        <option value="Kyrgyzstan" <?= set_select($row->country,'Kyrgyzstan') ?>>Kyrgyzstan</option>
                        <option value="Lao, People's Democratic Republic" <?= set_select($row->country,"Lao, People's Democratic Republic") ?>>Lao, People's Democratic Republic</option>
                        <option value="Latvia" <?= set_select($row->country,'Latvia') ?>>Latvia</option>
                        <option value="Lebanon" <?= set_select($row->country,'Lebanon') ?>>Lebanon</option>
                        <option value="Lesotho" <?= set_select($row->country,'Lesotho') ?>>Lesotho</option>
                        <option value="Liberia" <?= set_select($row->country,'Liberia') ?>>Liberia</option>
                        <option value="Libya" <?= set_select($row->country,'Libya') ?>>Libya</option>
                        <option value="Liechtenstein" <?= set_select($row->country,'Liechtenstein') ?>>Liechtenstein</option>
                        <option value="Lithuania" <?= set_select($row->country,'Lithuania') ?>>Lithuania</option>
                        <option value="Luxembourg" <?= set_select($row->country,'Luxembourg') ?>>Luxembourg</option>
                        <option value="Macao" <?= set_select($row->country,'Macao') ?>>Macao</option>
                        <option value="Macedonia, Rep. Of" <?= set_select($row->country,'Macedonia, Rep. Of') ?>>Macedonia, Rep. Of</option>
                        <option value="Madagascar" <?= set_select($row->country,'Madagascar') ?>>Madagascar</option>
                        <option value="Malawi" <?= set_select($row->country,'Malawi') ?>>Malawi</option>
                        <option value="Malaysia" <?= set_select($row->country,'Malaysia') ?>>Malaysia</option>
                        <option value="Maldives" <?= set_select($row->country,'Maldives') ?>>Maldives</option>
                        <option value="Mali" <?= set_select($row->country,'Mali') ?>>Mali</option>
                        <option value="Malta" <?= set_select($row->country,'Malta') ?>>Malta</option>
                        <option value="Marshall Islands" <?= set_select($row->country,'Marshall Islands') ?>>Marshall Islands</option>
                        <option value="Martinique" <?= set_select($row->country,'Martinique') ?>>Martinique</option>
                        <option value="Mauritania" <?= set_select($row->country,'Mauritania') ?>>Mauritania</option>
                        <option value="Mauritius" <?= set_select($row->country,'Mauritius') ?>>Mauritius</option>
                        <option value="Mayotte" <?= set_select($row->country,'Mayotte') ?>>Mayotte</option>
                        <option value="Mexico" <?= set_select($row->country,'Mexico') ?>>Mexico</option>
                        <option value="Micronesia, Federal States of" <?= set_select($row->country,'Micronesia, Federal States of') ?>>Micronesia, Federal States of</option>
                        <option value="Moldova, Republic of" <?= set_select($row->country,'Moldova, Republic of') ?>>Moldova, Republic of</option>
                        <option value="Monaco" <?= set_select($row->country,'Monaco') ?>>Monaco</option>
                        <option value="Mongolia" <?= set_select($row->country,'Mongolia') ?>>Mongolia</option>
                        <option value="Montenegro" <?= set_select($row->country,'Montenegro') ?>>Montenegro</option>
                        <option value="Montserrat" <?= set_select($row->country,'Montserrat') ?>>Montserrat</option>
                        <option value="Morocco" <?= set_select($row->country,'Morocco') ?>>Morocco</option>
                        <option value="Mozambique" <?= set_select($row->country,'Mozambique') ?>>Mozambique</option>
                        <option value="Myanmar, Burma" <?= set_select($row->country,'Myanmar, Burma') ?>>Myanmar, Burma</option>
                        <option value="Namibia" <?= set_select($row->country,'Namibia') ?>>Namibia</option>
                        <option value="Nauru" <?= set_select($row->country,'Nauru') ?>>Nauru</option>
                        <option value="Nepal" <?= set_select($row->country,'Nepal') ?>>Nepal</option>
                        <option value="Netherlands" <?= set_select($row->country,'Netherlands') ?>>Netherlands</option>
                        <option value="Netherlands Antilles" <?= set_select($row->country,'Netherlands Antilles') ?>>Netherlands Antilles</option>
                        <option value="New Caledonia" <?= set_select($row->country,'New Caledonia') ?>>New Caledonia</option>
                        <option value="New Zealand" <?= set_select($row->country,'New Zealand') ?>>New Zealand</option>
                        <option value="Nicaragua" <?= set_select($row->country,'Nicaragua') ?>>Nicaragua</option>
                        <option value="Niger" <?= set_select($row->country,'Niger') ?>>Niger</option>
                        <option value="Nigeria" <?= set_select($row->country,'Nigeria') ?>>Nigeria</option>
                        <option value="Niue" <?= set_select($row->country,'Niue') ?>>Niue</option>
                        <option value="Northern Mariana Islands" <?= set_select($row->country,'Northern Mariana Islands') ?>>Northern Mariana Islands</option>
                        <option value="Norway" <?= set_select($row->country,'Norway') ?>>Norway</option>
                        <option value="Oman" <?= set_select($row->country,'Oman') ?>>Oman</option>
                        <option value="Pakistan" <?= set_select($row->country,'Pakistan') ?>>Pakistan</option>
                        <option value="Palau" <?= set_select($row->country,'Palau') ?>>Palau</option>
                        <option value="Palestinian territories" <?= set_select($row->country,'Palestinian territories') ?>>Palestinian territories</option>
                        <option value="Panama" <?= set_select($row->country,'Panama') ?>>Panama</option>
                        <option value="Papua New Guinea" <?= set_select($row->country,'Papua New Guinea') ?>>Papua New Guinea</option>
                        <option value="Paraguay" <?= set_select($row->country,'Paraguay') ?>>Paraguay</option>
                        <option value="Peru" <?= set_select($row->country,'Peru') ?>>Peru</option>
                        <option value="Philippines" <?= set_select($row->country,'Philippines') ?>>Philippines</option>
                        <option value="Pitcairn Island" <?= set_select($row->country,'Pitcairn Island') ?>>Pitcairn Island</option>
                        <option value="Poland" <?= set_select($row->country,'Poland') ?>>Poland</option>
                        <option value="Portugal" <?= set_select($row->country,'Portugal') ?>>Portugal</option>
                        <option value="Puerto Rico" <?= set_select($row->country,'Puerto Rico') ?>>Puerto Rico</option>
                        <option value="Qatar" <?= set_select($row->country,'Qatar') ?>>Qatar</option>
                        <option value="Reunion Island" <?= set_select($row->country,'Reunion Island') ?>>Reunion Island</option>
                        <option value="Romania" <?= set_select($row->country,'Romania') ?>>Romania</option>
                        <option value="Russian Federation" <?= set_select($row->country,'Russian Federation') ?>>Russian Federation</option>
                        <option value="Rwanda" <?= set_select($row->country,'Rwanda') ?>>Rwanda</option>
                        <option value="Saint Kitts and Nevis" <?= set_select($row->country,'Saint Kitts and Nevis') ?>>Saint Kitts and Nevis</option>
                        <option value="Saint Lucia" <?= set_select($row->country,'Saint Lucia') ?>>Saint Lucia</option>
                        <option value="Saint Vincent and the Grenadines" <?= set_select($row->country,'Saint Vincent and the Grenadines') ?>>Saint Vincent and the Grenadines</option>
                        <option value="Samoa" <?= set_select($row->country,'Samoa') ?>>Samoa</option>
                        <option value="San Marino" <?= set_select($row->country,'San Marino') ?>>San Marino</option>
                        <option value="Sao Tome and Príncipe" <?= set_select($row->country,'Sao Tome and Príncipe') ?>>Sao Tome and Príncipe</option>
                        <option value="Saudi Arabia" <?= set_select($row->country,'Saudi Arabia') ?>>Saudi Arabia</option>
                        <option value="Senegal" <?= set_select($row->country,'Senegal') ?>>Senegal</option>
                        <option value="Serbia" <?= set_select($row->country,'Serbia') ?>>Serbia</option>
                        <option value="Seychelles" <?= set_select($row->country,'Seychelles') ?>>Seychelles</option>
                        <option value="Sierra Leone" <?= set_select($row->country,'Sierra Leone') ?>>Sierra Leone</option>
                        <option value="Singapore" <?= set_select($row->country,'Singapore') ?>>Singapore</option>
                        <option value="Slovakia (Slovak Republic)" <?= set_select($row->country,'Slovakia (Slovak Republic)') ?>>Slovakia (Slovak Republic)</option>
                        <option value="Slovenia" <?= set_select($row->country,'Slovenia') ?>>Slovenia</option>
                        <option value="Solomon Islands" <?= set_select($row->country,'Solomon Islands') ?>>Solomon Islands</option>
                        <option value="Somalia" <?= set_select($row->country,'Somalia') ?>>Somalia</option>
                        <option value="South Africa" <?= set_select($row->country,'South Africa') ?>>South Africa</option>
                        <option value="South Sudan" <?= set_select($row->country,'South Sudan') ?>>South Sudan</option>
                        <option value="Spain" <?= set_select($row->country,'Spain') ?>>Spain</option>
                        <option value="Sri Lanka" <?= set_select($row->country,'Sri Lanka') ?>>Sri Lanka</option>
                        <option value="Sudan" <?= set_select($row->country,'Sudan') ?>>Sudan</option>
                        <option value="Suriname" <?= set_select($row->country,'Suriname') ?>>Suriname</option>
                        <option value="Swaziland" <?= set_select($row->country,'Swaziland') ?>>Swaziland</option>
                        <option value="Sweden" <?= set_select($row->country,'Sweden') ?>>Sweden</option>
                        <option value="Switzerland" <?= set_select($row->country,'Switzerland') ?>>Switzerland</option>
                        <option value="Syria, Syrian Arab Republic" <?= set_select($row->country,'Syria, Syrian Arab Republic') ?>>Syria, Syrian Arab Republic</option>
                        <option value="Taiwan (Republic of China)" <?= set_select($row->country,'Taiwan (Republic of China)') ?>>Taiwan (Republic of China)</option>
                        <option value="Tajikistan" <?= set_select($row->country,'Tajikistan') ?>>Tajikistan</option>
                        <option value="Tanzania" <?= set_select($row->country,'Tanzania') ?>>Tanzania</option>
                        <option value="Thailand" <?= set_select($row->country,'Thailand') ?>>Thailand</option>
                        <option value="Tibet" <?= set_select($row->country,'Tibet') ?>>Tibet</option>
                        <option value="Timor-Leste (East Timor)" <?= set_select($row->country,'Timor-Leste (East Timor)') ?>>Timor-Leste (East Timor)</option>
                        <option value="Togo" <?= set_select($row->country,'Togo') ?>>Togo</option>
                        <option value="Tokelau" <?= set_select($row->country,'Tokelau') ?>>Tokelau</option>
                        <option value="Tonga" <?= set_select($row->country,'Tonga') ?>>Tonga</option>
                        <option value="Trinidad and Tobago" <?= set_select($row->country,'Trinidad and Tobago') ?>>Trinidad and Tobago</option>
                        <option value="Tunisia" <?= set_select($row->country,'Tunisia') ?>>Tunisia</option>
                        <option value="Turkey" <?= set_select($row->country,'Turkey') ?>>Turkey</option>
                        <option value="Turkmenistan" <?= set_select($row->country,'Turkmenistan') ?>>Turkmenistan</option>
                        <option value="Turks and Caicos Islands" <?= set_select($row->country,'Turks and Caicos Islands') ?>>Turks and Caicos Islands</option>
                        <option value="Tuvalu" <?= set_select($row->country,'Tuvalu') ?>>Tuvalu</option>
                        <option value="Uganda" <?= set_select($row->country,'Uganda') ?>>Uganda</option>
                        <option value="Ukraine" <?= set_select($row->country,'Ukraine') ?>>Ukraine</option>
                        <option value="United Arab Emirates" <?= set_select($row->country,'United Arab Emirates') ?>>United Arab Emirates</option>
                        <option value="United Kingdom" <?= set_select($row->country,'United Kingdom') ?>>United Kingdom</option>
                        <option value="United States" <?= set_select($row->country,'United States') ?>>United States</option>
                        <option value="Uruguay" <?= set_select($row->country,'Uruguay') ?>>Uruguay</option>
                        <option value="Uzbekistan" <?= set_select($row->country,'Uzbekistan') ?>>Uzbekistan</option>
                        <option value="Vanuatu" <?= set_select($row->country,'Vanuatu') ?>>Vanuatu</option>
                        <option value="Vatican City State (Holy See)" <?= set_select($row->country,'Vatican City State (Holy See)') ?>>Vatican City State (Holy See)</option>
                        <option value="Venezuela" <?= set_select($row->country,'Venezuela') ?>>Venezuela</option>
                        <option value="Vietnam" <?= set_select($row->country,'Vietnam') ?>>Vietnam</option>
                        <option value="Virgin Islands (British)" <?= set_select($row->country,'Virgin Islands (British)') ?>>Virgin Islands (British)</option>
                        <option value="Virgin Islands (U.S.)" <?= set_select($row->country,'Virgin Islands (U.S.)') ?>>Virgin Islands (U.S.)</option>
                        <option value="Wallis and Futuna Islands" <?= set_select($row->country,'Wallis and Futuna Islands') ?>>Wallis and Futuna Islands</option>
                        <option value="Western Sahara" <?= set_select($row->country,'Western Sahara') ?>>Western Sahara</option>
                        <option value="Yemen" <?= set_select($row->country,'Yemen') ?>>Yemen</option>
                        <option value="Zambia" <?= set_select($row->country,'Zambia') ?>>Zambia</option>
                        <option value="Zimbabwe" <?= set_select($row->country,'Zimbabwe') ?>>Zimbabwe</option>
                        <option value="Zimbabwe" <?= set_select($row->country,'Zimbabwe') ?>>Zimbabwe</option>
                    </select>
                </div>
            </div>

            <!-- Este bloque es nuevo -->
            <div class="control-group">
                <?= form_label('City:* ','city_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($city) ?>
                </div>
            </div>

            <div class="control-group">
                <?= form_label('Street Address:* ','street_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($street) ?>
                </div>
            </div>

            <div class="control-group">
                <?= form_label('ZIP/Postal Code:* ','zip_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($zip) ?>
                </div>
            </div>
            <!-- Fin del bloque nuevo -->

            <div class="control-group">
                <?= form_label('Phone Number:*  +','countrycode_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($countrycode) ?> (
                    <?= form_input($areacode) ?> )
                    <?= form_input($phone) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Web Page:* ','page_txt',$attributes) ?>
                <div class="controls controls-row">
                    <?= form_input($page) ?>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <select name="prefix_txt" id="prefixSelector" class="dropdown">
                        <option value="Mr" <?= set_select($row->prefix,'Mr') ?>>Mr</option>
                        <option value="Mrs" <?= set_select($row->prefix,'Mrs') ?>>Mrs</option>
                        <option value="Ms" <?= set_select($row->prefix,'Ms') ?>>Ms</option>
                        <option value="Miss" <?= set_select($row->prefix,'Miss') ?>>Miss</option>
                        <option value="PhD" <?= set_select($row->prefix,'PhD') ?>>PhD</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('First Name:* ','fname_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($fname) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Last Name:* ','lname_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($lname) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Job Position:* ','position_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($position) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Email:* ','email_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($email) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Confirm your email:* ','cemail_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($cemail) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('User Name:* ','user_txt',$attributes) ?>
                <div class="controls">
                    <?= form_input($user) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Password:* ','password_txt',$attributes) ?>
                <div class="controls">
                    <?= form_password($password) ?>
                </div>
            </div>
            <div class="control-group">
                <?= form_label('Confirm Password:* ','cpassword_txt',$attributes) ?>
                <div class="controls">
                    <?= form_password($cpassword) ?>
                </div>
            </div>
        </div>
		<div class="row" style="text-align:center;">
			<button class="btn btn-success" type="submit">Update</button>
		</div>
		<?= form_close()?>
		
	</div>
	
	<br />
	<br />
	<br />
	
	<hr>
	
	<footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>

    <script type="text/javascript">
        $(document).ready(function(){
            var pais = "<?php echo $row->country ?>";
            $("#countrySelector").val(pais);
        });
    </script>

</body>
</html>