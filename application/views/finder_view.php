<?php
$cookies = $this->session->all_userdata();
$sql = "SELECT * FROM DatosUsuario WHERE id_datos_usuario = ?";
$result = $this->db->query($sql, array($cookies['usuario']));
$row = $result->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
	
	<script src="<?= base_url('js/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script src=<?= base_url('js/efectos.js') ?>></script>	
	
</head>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48042101-1', 'apikontakt.com');
    ga('send', 'pageview');
</script>
<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
				<div class="nav-collapse collapse">
					<li class="brand" href="#"></li>
					<ul class="nav">
						<li class="home"><a href=<?= base_url('buyer') ?>>Home</a></li>
					</ul>
				</div>
				
				<form class="navbar-form pull-right">
					<ul class="nav">
						<li><a onclick="history.go(-1);">Go Back</a></li>
						<li><a href=<?= base_url('buyer/profile/'.$row->company_name) ?>>Profile</a></li>
						<li><a href=<?= base_url('main/logout') ?>>Logout</a></li>
					</ul>
				</form>
			</div>
		</div>
    </div>
	
	<div class="container">
		<div class="logo-register">
			<a href=<?= base_url('buyer') ?>><img  src=<?= base_url('img/Apikontakt_iza.png') ?>></a>
		</div>
		
		<div class="row">
			<div class="span12" style="text-align:center;">
				<img id="imagen-finder" src=<?= base_url('img/finder-ibuprofen.png') ?>>
			</div>
		</div>

		<br />
		
		<div class="row">
			<div class="span12" style="text-align:center;">
				<form style="text-align:center;">
					<div class="control-group">
						<div class="controls">
							<label class="radio inline" id="radio_product">
								<input type="radio" name="finder" value="1"/> Product
							</label>
							<label class="radio inline" id="radio_supplier" >
								<input type="radio" name="finder" value="2"/> Supplier
							</label>
						</div>
					</div>
				</form>
			</div>
		</div>

        <?= form_open("buyer/selectSupplier",$atributos = array('id' => 'supplier' ))?>
        <?php
        $supplier = array(
            'type' => 'text',
            'name' => 'supplier_name_txt',
            'id' => 'autocomplete2'
        );
        ?>
        <fieldset style="text-align:center;">
            <div class="control-group">
                <div class="controls">
                    <label id="supplier-text-finder">Supplier Name</label>
                    <?= form_input($supplier) ?>
                    <br />
                    <br />
                    <input type="image" src=<?= base_url('img/Search.png') ?> alt="Search">
                </div>
            </div>
        </fieldset>
        <?= form_close()?>
		
		<?= form_open("buyer/findProduct",$atributos = array('id' => 'product' ))?>
		<?php			
			$product = array(
				'type' => 'text',
				'name' => 'product_name_txt',
				'id' => 'autocomplete',
			);
		?>
		
		<fieldset style="text-align:center;">
			<div class="control-group">
				<div class="controls">
                    <br />
					<label class="text-finder">Product name</label>
					<?= form_input($product) ?>
					<label class="text-finder" id="or">or</label>
					<label class="text-finder">CAS Number</label>
					<input type="text" maxlength="7" name="cas1" style="width:10%"> --- <input type="text" maxlength="2" name="cas2" style="width:5%"> --- <input type="text" maxlength="1" name="cas3" style="width:3%">
					<br />
                    <br />
					<input type="image" src=<?= base_url('img/Search.png') ?> alt="Search">
				</div>
			</div>
		</fieldset>
		<?= form_close()?>
		
	</div>
	
	<br />
	<br />
	<br />
	
	<hr>
	
	<footer style="text-align:center;">
        <p><a class="link-footer" href="<?= base_url('about')?>">About us </a><a class="link-footer" href="<?= base_url('kontakt')?>"> Kontakt us</a> <a class="link-footer" href="<?= base_url('browser')?>">Browser Requirements</a> <a class="link-footer" href="<?= base_url('terms')?>">Terms and Conditions</a> <a class="link-footer" href="<?= base_url('privacy_policy')?>">Privacy Policy</a> <a class="link-footer" href="<?= base_url('cookie_policy')?>">Cookie Policy</a> <a class="link-footer" style="color: #0000FF;" href="<?= base_url('faq')?>"><strong>FAQ</strong></a> apikontakt &copy; 2013</p>
	</footer>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$(function() {
				$( "#autocomplete" ).autocomplete({
					source: function(request, response) {
						$.ajax({ url: "<?php echo site_url('buyer/suggestions'); ?>",
						data: { term: $("#autocomplete").val()},
						dataType: "json",
						type: "POST",
						success: function(data){
							response(data);
						}
					});
				},
				minLength: 2
				});
			});
			
			$(function() {
				$( "#autocomplete2" ).autocomplete({
					source: function(request, response) {
						$.ajax({ url: "<?php echo site_url('buyer/suggestionsSupplier'); ?>",
						data: { palabra: $("#autocomplete2").val()},
						dataType: "json",
						type: "POST",
						success: function(data){
							response(data);
						}
					});
				},
				minLength: 2
				});
			});
		});
	</script>
	
</body>
</html>