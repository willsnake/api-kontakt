<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Select Supplier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?= base_url('css/bootstrap.css') ?> rel="stylesheet">	
    <link href=<?= base_url('css/bootstrap-responsive.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/estilos.css') ?> rel="stylesheet">
	<link href=<?= base_url('css/datePicker.css') ?> rel="stylesheet">
	
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<script>
	  $(function() {
		$( "#datepicker" ).datepicker();
		$( "#datepicker" ).datepicker( "option", "dateFormat", "MM dth yy" );
	  });
	</script>
	<script>
		function checkAll(bx) {
			var cbs = document.getElementsByTagName('input');
			for(var i=0; i < cbs.length; i++) {
				if(cbs[i].type == 'checkbox') {
					cbs[i].checked = bx.checked;
				}
			}
		}
	</script>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body class="contenido">

    <div class="container">

      <div class="masthead">
        <ul class="nav nav-pills pull-right">
			<button class="btn btn-inverse back" onclick="history.go(-1);">Go Back</button>
			<a class="home" href=<?= base_url('buyer') ?>></a>
			<a class="profile" href="#"></a>
			<a class="logout" href=<?= base_url('main/logout') ?>></a>
        </ul>
      </div>

       <div class="logo">
		<img  src=<?= base_url('img/Apikontakt_iza.png') ?>>
	  </div>
	  
	  <div class="row">
		<div class="span12">
			
		</div>
	  </div>
	  
      <div class="row">
		<?= form_open('buyer/readyQuote',$atributos = array('class' => 'form-horizontal' ))?>
			<h1 class="titulo">Select supplier</h1>
			<label class="checkbox casillas">
				<input type="checkbox" value="all" onclick="checkAll(this)">Select All
			</label>
			<br />
			<?php 
				foreach ($suppliers as $row) { ?>
					<label class="checkbox casillas">
						<input type="checkbox" name="check[]" value=<?php echo "\"$row->usuario\" \ "; ?> > <?php echo $row->usuario; ?>
					</label>
				<?php }
			?>
		</div>
		<input type="hidden" name="name" value=<?php echo "\"$name\" \ "; ?>>
		<input type="hidden" name="pharmacopeia" value=<?php echo "\"$pharmacopeia\" \ "; ?>>
		<input type="hidden" name="special" value=<?php echo "\"$special_requirement\" \ "; ?>>
		<input type="hidden" name="quantity" value=<?php echo "\"$quantity\" \ "; ?>>
		<input type="hidden" name="unit" value=<?php echo "\"$unit\" \ "; ?>>
		<input type="hidden" name="incoterm" value=<?php echo "\"$incoterm\" \ "; ?>>
		<input type="hidden" name="port" value=<?php echo "\"$port\" \ "; ?>>
		<input type="hidden" name="limit" value=<?php echo "\"$due_date\" \ "; ?>>
		<input type="hidden" name="buyer" value=<?php echo "\"$buyer\" \ "; ?>>
		<textarea name="notes" id="hide" value=<?php echo "\"$notes\" \ "; ?>><?php echo $notes; ?></textarea>
		<div class="row" style="text-align:center;">
			<input type="image" src=<?= base_url('img/SendQuote.png') ?> alt="Send Quote">
		</div>
		<?= form_close()?>

      <br />

      <footer style="text-align:center;">
		<p>apikontakt &copy; 2013</p>
	</footer>

    </div>

  </body>
</html>